<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Quotation</title>
        <link rel="stylesheet" type="text/css" href="css/quotation.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    </head>

    <body>
        <%String uid;
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("type");
        if(user.compareTo("supplier")!=0){
        response.sendRedirect("/project/index.jsp");
        }else{
        uid =(String)session.getAttribute("user");
            ResultSet rs5;
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	        /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
            psm5.setString(1,uid);
            rs5 = psm5.executeQuery();
            if(!rs5.next()){
                response.sendRedirect("/project/index.jsp");
            }*/
        %>

        <input type="checkbox" id="check">  

        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../logout.jsp?user=<%=uid%>" class="logout">Logout</a>
            </div>
        </header>

        <!--sidebar-->
        <div class="sidebar">
            <center>
                <%
                    ResultSet rs6;
                    PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                    psm6.setString(1,uid);
                    rs6 = psm6.executeQuery();
                    if(rs6.next()){
                %>

                <img src="avatar.svg" class="profileimg" alt="">
                <h4><%=rs6.getString("name") %></h4>
                <% } %>
            </center>
            <ul>
                <li><a href="supplier_orders.jsp?user=<%=uid%>"><i class="fas fa-tasks"></i>Orders</a></li>
                 <li><a href="suppliersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>     
        </div>

        <div class="content">
            <div class="box">
                <h3>Quotation Form</h3>
                <%
                    String order = request.getParameter("id");
                %>

                <%
                    LocalDate date = LocalDate.now();
                    String strDate = date.toString();
                %>

                <form action="application/quotationsend.jsp" onsubmit="return validateform()" method="POST" name="quotation">
                    <table class="table">
                        <tr>
                            <td class="td1"><p>Date</p></td>
                            <td><input type="date" id="date" name="date" value="<%=strDate%>" readonly></td>
                            <!-- <td><p id="date1" style="font-size:smaller;" class = "error hidden">Required!</p></td> -->
                            <td class="td1"><p>Order ID</p></td>
                            <td><input type="text" id="orderid" value="<%=order%>" name="orderid"></td>
                        </tr>
                        
                        <!-- <tr>
                            <td class="td1"><p>Quotation ID *</p></td>
                            <td><input type="number" id="id" name="qid" placeholder="Enter Quotation ID"></td>   
                            <td><p id="qid" style="font-size:smaller;" class = "error hidden">Required!</p></td>
                            <td></td>
                        </tr> -->

                        <tr>
                            <!-- <td class="td1"><p>Quotaion</p></td>-->
                            <td colspan=4><input type="text" name="body" value="0" hidden></td>
                        </tr> 
                            
                        <%
                            ResultSet rs;
                            PreparedStatement psm = conn.prepareStatement("select supordermaterials.orderID,supordermaterials.materialID,supordermaterials.quantity,materials.name from supordermaterials inner join materials on supordermaterials.materialID = materials.materialID where orderID=?");
                            psm.setString(1,order);
                            rs =  psm.executeQuery();
                            while(rs.next()){
                        %>
                        <tr> 
                            <td class="td1"><p><%=rs.getString("name") %> x<%=rs.getString("quantity") %>*</p></td>
                            <td><input prevalue=0 type="number" id="num" placeholder="Enter Price" min=0 step=".01" style="text-align: right;" required></td>
                            <!-- <td><p id="numb" style="font-size:smaller;" class = "error hidden">Required!</p></td> -->
                        </tr>
                        <%
                            }     
                        %>
        
                        <tr>
                            <td class="td1"><p>Sub Total</p></td>
                            <td><input type="number" id="subtotal" name="subtotal" step=".01" onkeyup="sub()" style="text-align: right;" readonly></td>  
                            <td><p id="sub" style="font-size:smaller;"></p></td>
                        </tr>
                        
                        <script>
                            var total = 0;
                            $("#subtotal").val(total);
                            
                            $("[id^=num]").change(function(){
                                var preVal = parseFloat($(this).attr("prevalue"));
                                total = total + parseFloat($(this).val()) - preVal;
                                $(this).attr("prevalue",parseFloat($(this).val()));
                                var result = total.toFixed(2);
                                $("#subtotal").val(result);
                            })
                        </script>

                        <tr>
                            <td class="td1"><p>Discount *</p></td>
                            <td><input type="number" id="discount" name="discount" placeholder="Enter Discount" step=".01" min="0" max="20000" onkeyup="sub()" style="text-align: right;" required></td>  
                            <!-- <td><p id="dis" style="font-size:smaller;" class = "error hidden">Required!</p></td> -->
                        </tr>
                        
                        <tr>
                            <td class="td1"><p>Total</p></td>
                            <td><input type="number" id="total" name="total" value="0.00" step=".01" style="text-align: right;" readonly></td>  
                            <td><p id="tot" style="font-size:smaller;"></p></td>
                        </tr>
                    </table>
 
                    <script type="text/javascript">
                        function sub(){
                            var num1, num2, num3;
                            num1 = parseFloat(document.getElementById("subtotal").value);
                            num2 = parseFloat(document.getElementById("discount").value);
                            if(num1=="")
                                num1 = 0;
                            if(num2=="")
                                num2 = 0;
                            num3 = num1 - num2;
                           
                            if(!isNaN(num3)){
                                document.getElementById("total").value = (num3).toFixed(2);
                            }   
                        }
                    </script>
                    
                    <!-- <script>
                        function validateform(){
                            var b = document.forms["quotation"]["id"].value;
                            var e = document.forms["quotation"]["discount"].value;

                            if(b == "" && e == ""){
                                document.getElementById("qid").classList.remove('hidden');
                                document.getElementById("dis").classList.remove('hidden');
                                return false;
                                // document.getElementById("qid").innerHTML = "Quotation ID is Compulsory!";
                                // alert("Quotation ID is required!");
                            }
                            
                            else if(b != "" && e == ""){
                                document.getElementById("qid").classList.add('hidden');
                                document.getElementById("dis").classList.remove('hidden');
                                return false;
                            }

                            else if(b == "" && e != ""){
                                document.getElementById("qid").classList.remove('hidden');
                                document.getElementById("dis").classList.add('hidden');
                                return false;
                            }

                            else if(b != "" && e != ""){
                                return true;
                            }
                        }
                    </script> -->

                    <br>
                    <button type="submit" name="user" value="<%=uid%>">Send</button>       
                </form>
                
                <form action="#">
                    <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                    <button type="submit" name="id" value="<%=order%>" onclick="form.action='orderdetails.jsp'">Back</button>
                </form> 
            </div> 
        </div>
        <%}}%>
    </body>
</html>