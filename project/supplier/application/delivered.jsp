<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>

<%
    String user = request.getParameter("user");
    String id = request.getParameter("btn");
    LocalDate date = LocalDate.now();
    String strDate = date.toString();

    try{
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
        PreparedStatement ps = conn.prepareStatement("update allorders set status=? where orderID=?");
        ps.setString(1, "Delivered");
        ps.setString(2, id);
        ps.executeUpdate();

        PreparedStatement ps1 = conn.prepareStatement("insert into notification(oip,from_user,to_user,type,description,link,status,date) values(?,?,?,?,?,?,?,?)");
        ps1.setString(1, id);
        ps1.setString(2, user);
        ps1.setString(3, "1");
        ps1.setString(4, "sup_order");
        ps1.setString(5, "Order Delivered");
        ps1.setString(6, "/project/admin/supply/orderlist.jsp");
        ps1.setString(7, "not viewed");
        ps1.setString(8, strDate);
        ps1.executeUpdate();

        response.sendRedirect("/project/supplier/orderdetails.jsp?user="+user+"&id="+id);
    }

catch(Exception e){
    out.println(e);
}
%>