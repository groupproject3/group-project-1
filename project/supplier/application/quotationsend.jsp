<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>

<%
    

LocalDate sdate = LocalDate.now();
String strDate = sdate.toString();
String uid = request.getParameter("user");
String date = request.getParameter("date");
String qid = request.getParameter("qid");
String orderid = request.getParameter("orderid");
String body = request.getParameter("body");
String subtotal = request.getParameter("subtotal");
String discount = request.getParameter("discount");
String total = request.getParameter("total");

try{
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    PreparedStatement ps = conn.prepareStatement("insert into quotation(date, qid, orderid, body, subtotal, discount, total) values(?,?,?,?,?,?,?)");
    ps.setString(1,date);
    ps.setString(2,qid);
    ps.setString(3,orderid);
    ps.setString(4,body);
    ps.setString(5,subtotal);
    ps.setString(6,discount);
    ps.setString(7,total);
    
    int x = ps.executeUpdate();

    PreparedStatement ps2 = conn.prepareStatement("update allorders set status=?, amount=? where orderID = ?");
    ps2.setString(1,"Quotation Sent");
    ps2.setString(2,total);
    ps2.setString(3,orderid);
    ps2.executeUpdate();

    PreparedStatement ps3 = conn.prepareStatement("insert into notification(oip,from_user,to_user,type,description,link,status,date) values(?,?,?,?,?,?,?,?)");
    ps3.setString(1,orderid);
    ps3.setString(2,uid);
    ps3.setString(3,"1");
    ps3.setString(4,"sup_order");
    ps3.setString(5,"Quotation Sent");
    ps3.setString(6,"/project/admin/supply/orderlist.jsp");
    ps3.setString(7,"not viewed");
    ps3.setString(8,strDate);
    ps3.executeUpdate();

    response.sendRedirect("/project/supplier/orderdetails.jsp?user="+uid+"&id="+orderid);

    
}catch(Exception e){
    out.println(e);
}
%>