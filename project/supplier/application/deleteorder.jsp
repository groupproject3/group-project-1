<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>

<%
    String uid = request.getParameter("user");
    String oid = request.getParameter("btn");
    LocalDate date = LocalDate.now();
    String strDate = date.toString();

    try{
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
        PreparedStatement ps = conn.prepareStatement("update allorders set status=? where orderID=?");
        ps.setString(1,"Order Rejected");
        ps.setString(2,oid);
        ps.executeUpdate();

        PreparedStatement ps3 = conn.prepareStatement("insert into notification(oip,from_user,to_user,type,description,link,status,date) values(?,?,?,?,?,?,?,?)");
        ps3.setString(1,oid);
        ps3.setString(2,uid);
        ps3.setString(3,"1");
        ps3.setString(4,"sup_order");
        ps3.setString(5,"Order Rejected");
        ps3.setString(6,"/project/admin/supply/orderlist.jsp");
        ps3.setString(7,"not viewed");
        ps3.setString(8,strDate);
        ps3.executeUpdate();
%>

<script>
    alert("Order Deleted!");
</script>

<%
        response.sendRedirect("/project/supplier/orderdetails.jsp?user="+uid+"&id="+oid);
    }
    catch(Exception e){
        out.println(e);
    }
%>