<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/ordermaterials.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Order Details</title>
    </head>
    <body>
    <%String uid;
    String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("type");
        if(user.compareTo("supplier")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>

        <input type="checkbox" id="check">

        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../logout.jsp?user=<%=uid%>" class="logout">Logout</a>
            </div>
        </header>

        <!--sidebar-->
        <div class="sidebar">
            <center>
                <%
                    ResultSet rs6;
                    PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                    psm6.setString(1,uid);
                    rs6 = psm6.executeQuery();
                    if(rs6.next()){
                %>
                
                <img src="avatar.svg" class="profileimg" alt="">
                <h4><%=rs6.getString("name") %></h4>
                <% } %>
            </center>
            <ul>
                <li><a href="supplier_orders.jsp?user=<%=uid%>" id="a1"><i class="fas fa-tasks"></i>Orders</a>
                <li><a href="suppliersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
        </div>

        <div class="content">
            <div class="box">
                <h3>Ordered Materials</h3>
                <div style="overflow-x:auto;">      <!--responsive table-->
                <table cellpadding ="8" width="100%">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Item Name</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <%
                        String ordrid = request.getParameter("btn");
                        ResultSet rs;
                        PreparedStatement psm = conn.prepareStatement("select supordermaterials.orderID,supordermaterials.materialID,supordermaterials.quantity,materials.name from supordermaterials inner join materials on supordermaterials.materialID = materials.materialID where orderID=?");
                        psm.setString(1,ordrid);
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            %>
                                <tr>
                                    <td align="center"><%=rs.getString("orderID") %></td>
                                    <td align="center"><%=rs.getString("name") %></td>
                                    <td align="center"><%=rs.getString("quantity") %></td>
                                </tr>
                            <%
                        }  
                        
                    %>
                    
                </table>
                </div>
                            <form action="#">
                                <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                                <button type="submit" id="btn1"  name="id" value="<%=ordrid%>" onclick="form.action='orderdetails.jsp'" style="float: right; margin-top: 50px;">Back</button>
                            </form> 
                        
                
                   
            </div>
            
        </div>
        <%}}%>
    </body>
</html>