<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Quotation</title>
        <link rel="stylesheet" type="text/css" href="css/viewquotation.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    </head>

    <body>
        <%String uid;
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("type");
        if(user.compareTo("supplier")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
            uid =(String)session.getAttribute("user");
            ResultSet rs5;
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	        /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
            psm5.setString(1,uid);
            rs5 = psm5.executeQuery();
            if(!rs5.next()){
                response.sendRedirect("/project/index.jsp");
            }*/
        %>

        <input type="checkbox" id="check">

        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../logout.jsp?user=<%=uid%>" class="logout">Logout</a>
            </div>
        </header>

        <!--sidebar-->
        <div class="sidebar">
            <center>
                <%
                    ResultSet rs6;
                    PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                    psm6.setString(1,uid);
                    rs6 = psm6.executeQuery();
                    if(rs6.next()){
                %>
                
                <img src="avatar.svg" class="profileimg" alt="">
                <h4><%=rs6.getString("name") %></h4>
                <% } %>
            </center>
            <ul>
                <li><a href="supplier_orders.jsp?user=<%=uid%>" id="a1"><i class="fas fa-tasks"></i>Orders</a></li>
                 <li><a href="suppliersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>  
        </div>

        <div class="content">
            <div class="box">
                <h3>Quotation Form</h3>
                <%   
                    ResultSet rs;
                    String ordrid = request.getParameter("id");
                    PreparedStatement psm = conn.prepareStatement("select * from quotation where orderID = ?");
                    psm.setString(1, ordrid);
                    rs =  psm.executeQuery();
                       
                    while(rs.next()){        
                %>
            
                <form action="application/quotationsend.jsp" method="POST">
                    <table>
                        <tr>
                            <td class="td1"><p>Date</p></td>
                            <td><input type="date" id="date" name="date" value="<%=rs.getString("date")%>" disabled></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td class="td1"><p>Quotation ID</p></td>
                            <td><input type="text" id="id" name="qid" value="<%=rs.getString("qid")%>" disabled></td>   
                            <td class="td1"><p>Order ID</p></td>
                            <td><input type="text" id="orderid" value="<%=rs.getString("orderid")%>" disabled name="orderid"></td>
                        </tr>
                        
                        <!-- <tr>
                            <td class="td1"><p>Quotation</p></td>
                            <td colspan=4><textarea name="body" rows="10" cols="100"  disabled><%=rs.getString("body")%></textarea></td>
                        </tr> -->

                        <tr>
                            <td class="td1"><p>Sub Total</p></td>
                            <td><input type="text" id="subtotal" name="subtotal" value="<%=rs.getString("subtotal")%>" disabled style="text-align: right;"></td>  
                        </tr>

                        <tr>
                            <td class="td1"><p>Discount</p></td>
                            <td><input type="text" id="discount" name="discount" value="<%=rs.getString("discount")%>" disabled style="text-align: right;"></td>  
                        </tr>

                        <tr>
                            <td class="td1"><p>Total</p></td>
                            <td><input type="text" id="total" name="total" value="<%=rs.getString("total")%>" disabled style="text-align: right;"></td>  
                        </tr>
                    </table>
                    
                    <form action="#">
                        <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                        <button type="submit" name="id" value="<%=ordrid%>" onclick="form.action='orderdetails.jsp'">Back</button>
                    </form> 
                            
                <%}%>
                </form>
            </div> 
        </div>
        <%}}%>
    </body>
</html>