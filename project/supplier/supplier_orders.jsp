<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/supplier_orders.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Orders</title>
    </head>
    <body>
        <%String uid;
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("type");
        if(user.compareTo("supplier")!=0){
        response.sendRedirect("/project/index.jsp");
        }else{
        uid =(String)session.getAttribute("user");
        ResultSet rs5,rs6;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>

        <input type="checkbox" id="check">

        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        PreparedStatement psm9 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm9.setString(1,uid);
                        psm9.setString(2,"not viewed");
                        rs6 = psm9.executeQuery();
                        if(rs6.next()){
                        %><a href="supnotifications.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="supnotifications.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../logout.jsp?user=<%=uid%>" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>

        <!--sidebar-->
        <div class="sidebar">
            <center>
                <%
                    ResultSet rs7;
                    PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                    psm6.setString(1,uid);
                    rs7 = psm6.executeQuery();
                    if(rs7.next()){
                %>
                    
                <img src="avatar.svg" class="profileimg" alt="">
                <h4><%=rs7.getString("name") %></h4>
                <% } %>
            </center>
            <ul>
                <%-- <li><a href="#"><i class="fas fa-house-user"></i>Home</a></li> --%>
                <li><a href="#" id="a1"><i class="fas fa-tasks"></i>Orders</a>
                    <!-- <div class="submenu">
                        <ul>
                            <li id="li"><a href="inventoryitems.html">Items</a></li>
                            <li id="li"><a href="inventorystocks.html">Stocks</a></li>
                            <li id="li"><a href="#">Production</a></li>
                        </ul>
                    </div> -->
                </li>
                <!-- <li><a href="adminsaleshome.html"><i class="fas fa-dollar-sign"></i>Sales</a></li>
                <li><a href="adminsupplyhome.html"><i class="fas fa-truck"></i>Supply</a></li>
                <li><a href="adminemployee.html"><i class="fas fa-male"></i>Employee</a></li>
                <li><a href="adminreporthome.html"><i class="fas fa-file"></i>Report</a></li> -->
                <li><a href="suppliersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <h3>All Orders</h3>
                <table cellpadding ="11" width="100%" class="table1">
                    <thead>
                      <tr class="tr1">
                        <th class="th1"><p name=id value=id id=id>Order ID</p></th>
                        <th class="th1">Name</th>
                        <th class="th1">Req. Date</th>
                        <th class="th1"></th>
                      </tr>
                    </thead>
                      
                    <%
                        ResultSet rs;
                        PreparedStatement psm = conn.prepareStatement("select orderID,status,req from allorders where flag='supplier' and userID=? order by orderID desc");
                        psm.setString(1,uid);
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            String orderid = rs.getString("orderID");
                            %>
                                <tr class="tr1">
                                    <td align="center"><%=rs.getString("orderID") %></td>
                                    <td align="center"><%=rs.getString("status") %></td>
                                    <td align="center"><%=rs.getString("req") %></td>
                                    <td align="center"><a href="orderdetails.jsp?user=<%=uid%>&id=<%=rs.getString("orderID")%>">View</a></td>
                                </tr> 
                            <%
                        }
                    %>
                </table>
            </div> 
        </div>
        <!--<td><button type="submit" name="btn" value="orderitems" id="btn1" onclick="document.location='orderdetails.jsp?id=<%%>'">View</button></td>-->
    <%}}%>
    </body>
</html>