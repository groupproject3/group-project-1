<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/orderdetails.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Order Details</title>
    </head>
    <body>
    <%String uid;
    String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("type");
        if(user.compareTo("supplier")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>

        <input type="checkbox" id="check">

        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../logout.jsp?user=<%=uid%>" class="logout">Logout</a>
            </div>
        </header>

        <!--sidebar-->
        <div class="sidebar">
            <center>
                <%
                    ResultSet rs6;
                    PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                    psm6.setString(1,uid);
                    rs6 = psm6.executeQuery();
                    if(rs6.next()){
                %>

                <img src="avatar.svg" class="profileimg" alt="">
                <h4><%=rs6.getString("name") %></h4>
                <% } %>
            </center>
            <ul>
                <li><a href="supplier_orders.jsp?user=<%=uid%>" id="a1"><i class="fas fa-tasks"></i>Orders</a>
                <li><a href="suppliersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
        </div>

        <div class="content">
            <div class="box">
                <h3>Order Details</h3>
                <table cellpadding="12" width="100%">
                    <tr>
                    <%
                        ResultSet rs;
                        String ordrid = request.getParameter("id");
                        PreparedStatement psm = conn.prepareStatement("select * from allorders where orderID = ?");
                        psm.setString(1, ordrid);
                        rs =  psm.executeQuery();
                       
                        while(rs.next()){
                    %>
<!--'"+id+"'-->
                    <tr>
                        <td class="td">
                            <p>Order ID</p>
                            <input type="text" value="<%= rs.getString("orderID")%>" name="orderID" id="orderID" readonly>
                        </td>
                        <td class="td">
                            <p>Customer Name</p>
                            <input type="text" value="Asiri Industries (Pvt) Ltd." name="name" id="name" disabled>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <p>Email</p>
                            <input type="text" value="asiriindustries@gmail.com" name="email" id="email" disabled>
                        </td>
                        <td>
                            <p>Total Price</p>
                            <input type="text" value="<%= rs.getString("amount")%>" name="amount" id="amount" disabled>
                        </td>
                    </tr>
                        
                    <tr>
                        <td>
                            <p>Req. Date</p>
                            <input type="text" value="<%= rs.getString("req")%>" name="req" id="req" disabled>
                        </td>
                        <td>
                            <p>Status</p>
                            <input type="text" value="<%= rs.getString("status")%>" name="status" id="status" disabled>
                        </td>
                    </tr>
                </table>

                <form action="#">
                <table>
                    <tr>

                        <%-- <a id="btn2" href="suppliersettings.jsp?user=<%=uid%>">Materials</a> --%>
                        <td class="td">
                            <%
                                String status = rs.getString("status");
                                String s = "Quotation Accepted";
                                if(status.compareTo(s)==0){
                            %>
                            <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                            <button type="submit" id="btn1"  name="btn" value="<%=rs.getString("orderID")%>" onclick="form.action='application/delivered.jsp'">Delivered</button>
                            <%}%>
                        </td>
                        
                        <td class="td">
                            <%
                                status = rs.getString("status");
                                s = "Placed";
                                if(status.compareTo(s)==0){
                            %>
                            <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                            <button type="submit" id="btn1" name="btn" value="<%=ordrid%>" onclick="form.action='application/deleteorder.jsp'">Reject</button> <!--display1('<%=ordrid%>');-->
                            <%
                                }
                            %>
                        </td>

                        <td class="td">
                            <%
                                status = rs.getString("status");
                                s = "Placed";
                                String s1 = "Order Rejected";
                                if(status.compareTo(s)!=0){
                                    if(status.compareTo(s1)!=0){
                            %>
                            
                                <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                                <button type="submit" id="btn1" name="id" value="<%= rs.getString("orderID")%>" onclick="form.action='viewquotation.jsp'">Quotation</button>
                            
                            <%}}else{%>
                            
                                <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                                <button type="submit" id="btn1" name="id" value="<%= rs.getString("orderID")%>" onclick="form.action='quotationform.jsp'">Accept</button>
                           
                            <%}%>
                        </td>

                        <td class="td">
                            <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                            <button type="submit" id="btn1"  name="btn" value="<%=rs.getString("orderID")%>" onclick="form.action='ordermaterials.jsp'">Materials</button>
                        </td>

                        <td class="td">
                            <button type="submit" id="btn1" value="<%=uid%>" name="user" onclick="form.action='supplier_orders.jsp'">Back</button>
                        </td>
                    </tr>
                </table>
            </form>
                    <%}%>    
            </div>
        </div>
        <!-- finally{
            /*This block should be added to your code
             * You need to release the resources like connections
             */
            if(conn!=null)
             conn.close();
           } -->

           <%}}%>
    </body>
</html>