<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/notifications.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Notifications</title>
    </head>
    <body>
    <%
        String uid;
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("type");
        if(user.compareTo("supplier")!=0){
        response.sendRedirect("/project/index.jsp");
        }else{
        uid =(String)session.getAttribute("user");
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
    %>

        <input type="checkbox" id="check">

        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../logout.jsp?user=<%=uid%>" class="logout">Logout</a>
            </div>
        </header>

        <!--sidebar-->
        <div class="sidebar">
            <center>
                <%
                    ResultSet rs6;
                    PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                    psm6.setString(1,uid);
                    rs6 = psm6.executeQuery();
                    if(rs6.next()){
                %>

                <img src="avatar.svg" class="profileimg" alt="">
                <h4><%=rs6.getString("name") %></h4>
                <% } %>
            </center>
            <ul>
                <li><a href="supplier_orders.jsp?user=<%=uid%>" id="a1"><i class="fas fa-tasks"></i>Orders</a>
                <li><a href="suppliersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
        </div>

        <div class="content">
            <div class="box">
                <table cellpadding ="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                  <%
                        ResultSet rs;
                        
                        PreparedStatement psm = conn.prepareStatement("select notification.id,usertable.name,notification.description,notification.link,notification.date from notification inner join usertable on usertable.userID=notification.from_user where notification.to_user=? and notification.status=?");
                        psm.setString(1,uid);
                        psm.setString(2,"not viewed");
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            %>
                                <tr class="tr1">
                                    <td><i class="fas fa-exclamation-circle"></i> <%=rs.getString("name") %></td>
                                    <td><%=rs.getString("description") %></td>
                                    <td><%=rs.getString("date") %></td>
                                    <td><a href="application/notifyredirect.jsp?id=<%=rs.getString("id") %>">Link</a></td>
                                </tr>
                            <%
                        }

                    %>
                    <%
                        ResultSet rs2;
                        
                        PreparedStatement psm2 = conn.prepareStatement("select notification.id,usertable.name,notification.description,notification.link,notification.date from notification inner join usertable on usertable.userID=notification.from_user where notification.to_user=? and notification.status=?");
                        psm2.setString(1,uid);
                        psm2.setString(2,"viewed");
                        rs2 =  psm2.executeQuery();
                        while(rs2.next()){
                            %>
                                <tr class="tr1">
                                    <td><%=rs2.getString("name") %></td>
                                    <td><%=rs2.getString("description") %></td>
                                    <td><%=rs2.getString("date") %></td>
                                    <td><a href="application/notifyredirect.jsp?id=<%=rs2.getString("id") %>">Link</a></td>
                                </tr>
                            <%
                        }
                    %>
                </table>
            </div>
        </div>
        <%}}%>
    </body>
</html>