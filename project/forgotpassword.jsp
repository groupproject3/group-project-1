<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="forgotpassword.css">
    <script src="https://smtpjs.com/v3/smtp.js"></script>
    <head>
        <title>Sign Up</title>
    </head>
    <body>
        <div class="box">
            
            <h1>Forgot Password</h1>
            <form action="forgotpasswordconnect.jsp" method="POST">
            <%
                String result = null;
                result = request.getParameter("result");
                String str = "Invalide";
                %>
                <p class="p">User Name</p>
                <input type="text" name="uname" placeholder="Enter User Name" required>
                <p class="p">Email</p>
                <input type="email" name="email" placeholder="Enter Email" required>
                <%
                    if(result!=null){
                %><h4>Incorrect Email!</h4><% 
                    }
                    result = null;
                %>
                <button type="submit">Send Email</button>
                
                
            </form>
            <form action="login.jsp">
            <button type="submit">Back</button>
            </form>
        </div>
    </body>
   
  
</html>