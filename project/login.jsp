<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <head>
        <title>Sign Up</title>
    </head>
    <body>
     <script>
    function repwd(){
        var passwordInput = document.getElementById('password');
        var passStatus = document.getElementById('icon3');

        if(passwordInput.type == 'password'){
            passwordInput.type = 'text';
            passStatus.className = 'fa fa-eye-slash';
        }
        else{
            passwordInput.type = 'password';
            passStatus.className = 'fa fa-eye';
        }
        }
        </script>
        <div class="box">
            <img src="img/avatar.svg" class="avatar">
            <h1>Login</h1>
            <form action="indexconnect.jsp" method="POST">
                <%
                String result = null;
                result = request.getParameter("result");
                String str = "Invalide";
                %>
                <p class="p">User Name</p>
                <input type="text" name="uname" placeholder="Enter a User Name" required>
                <p class="p">Password</p>
                <input type="password" name="password" id="password" placeholder="Enter a password" required>
                <i id="icon3" class="fa fa-eye" onClick="repwd()"></i>
                <%
                    if(result!=null){
                %><h4>Login Unsuccessfull!</h4><% 
                    }
                    result = null;
                %>

                <button type="submit">Login</button>
                <a href="forgotpassword.jsp">Forgot Password</a><br>
                <a href="sign.jsp">Sign Up</a>
            </form>
        </div>
    </body>
    
  
</html>