<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/adminhome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Notifications</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../../logout.jsp?user=1" class="logout">Logout</a>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
             <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp" id="a1"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Update Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="adminsaleshome.jsp"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="adminsupplyhome.jsp"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="adminreporthome.jsp"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <table cellpadding ="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                  <%
                        ResultSet rs;
                        
                        PreparedStatement psm = conn.prepareStatement("select notification.id,usertable.name,notification.type,notification.description,notification.link,notification.date from notification inner join usertable on usertable.userID=notification.from_user where notification.to_user=? and notification.status=?");
                        psm.setString(1,"1");
                        psm.setString(2,"not viewed");
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            %>
                                <tr class="tr1">
                                    <td><i class="fas fa-exclamation-circle"></i> <%=rs.getString("name") %></td>
                                    <td><%=rs.getString("type") %></td>
                                    <td><%=rs.getString("description") %></td>
                                    <td><%=rs.getString("date") %></td>
                                    <td><a href="application/notifyredirect.jsp?id=<%=rs.getString("id") %>">Link</a></td>
                                </tr>
                            <%
                        }

                    %>
                    <%
                        ResultSet rs2;
                        
                        PreparedStatement psm2 = conn.prepareStatement("select notification.id,usertable.name,notification.type,notification.description,notification.link,notification.date from notification inner join usertable on usertable.userID=notification.from_user where notification.to_user=? and notification.status=?");
                        psm2.setString(1,"1");
                        psm2.setString(2,"viewed");
                        rs2 =  psm2.executeQuery();
                        while(rs2.next()){
                            %>
                                <tr class="tr1">
                                    <td><%=rs2.getString("name") %></td>
                                    <td><%=rs2.getString("type") %></td>
                                    <td><%=rs2.getString("description") %></td>
                                    <td><%=rs2.getString("date") %></td>
                                    <td><a href="application/notifyredirect.jsp?id=<%=rs2.getString("id") %>">Link</a></td>
                                </tr>
                            <%
                        }

                    %>
                
                </table>
                <form action="#">
                    <button type="submit" id="btn1" name="btn" value="" onclick="form.action='customer.jsp'">Back</button>
                </form>
            </div>
        </div>

    <%}}%>
    </body>
    
  
</html>S