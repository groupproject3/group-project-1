<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    String opt = request.getParameter("id");
    if(opt!=null){
        PreparedStatement ps = conn.prepareStatement("update notification set status = ? where id = ?");
        ps.setString(1,"viewed");
        ps.setString(2,opt);
        ps.executeUpdate();
        PreparedStatement psm = conn.prepareStatement("select * from notification where id=?");
        psm.setString(1,opt);
        ResultSet rs = psm.executeQuery();
        while(rs.next()){
            String link = rs.getString("link");
            response.sendRedirect(link);
        }
    }
%>