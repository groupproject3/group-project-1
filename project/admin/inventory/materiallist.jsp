<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>



<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/materiallist.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Materials</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
             <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" id="a1"><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="#" id="a1">Items</a></li>
                            <li id="li"><a href="inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <table class="table1" cellpadding ="0" width="100%">
                    <thead>
                        <tr class="tr2">
                            <th>Material ID</th>
                            <th>Name</th>
                            <th>Re-order Level</th>
                            <th>Type</th>
                            <th>Mtype</th>
                            <th>Stock</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <%
                        ResultSet rs;
                        
                        PreparedStatement psm = conn.prepareStatement("select * from materials");
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            String id = rs.getString("materialID");
                            String type = rs.getString("type");
                            %>
                                <tr class="tr1">
                                    <td><p id="<%=id %>"><%=type %><%=id %></p></td>
                                    <td><%=rs.getString("name") %></td>
                                    <td><%=rs.getString("prelvl") %></td>
                                    <%if(rs.getString("type").compareTo("RAW")==0){
                                        %><td>Raw</td><%   
                                    }else if(rs.getString("type").compareTo("INT")==0){
                                        %><td>Intermediate</td><% 
                                    }else{
                                        %><td>Outcome</td><%
                                    }%>
                                    <td><%=rs.getString("mtype") %></td>
                                    <td><%=rs.getString("stock") %></td>  
                                    <td><a href="updateitem.jsp?id=<%=id%>">Edit</a></td>
                                    <td><a href="#" onclick="display1(<%=id%> , '<%=type%>');">Delete</a></td>
                                </tr>
                            <%
                        }

                    %>
                </table>
                <form action="#">
                    <button type="submit" style="margin-bottom: 10px;" name="btn" value="inventoryitems" id="btn1" onclick="form.action='application/inventoryredirect.jsp'">Back</button>
                </form>
                   
            </div>
            
        </div>
                <div id="popup1" class="popup">

                <!-- Modal content -->
                <div class="popup-content">
                    <span class="close" onclick="hide1();">&times;</span>
                    <h3>Warning</h3>
                    <p id="mid2"></p>
                    <table style="float:right;">
                        <tr>
                            <td>
                                <form>
                                    <button type="submit" id="btn3" class="btn3" name="pid"  onclick="form.action='application/deleteitem.jsp'">Yes</button>
                                </form>
                            </td>
                            <td><button type="submit" id="btn4" class="btn4" onclick="hide1();">No</button></td>
                        </tr>
                    </table>
                    
                  </div>  
                </div>
               
                
    <script>
        // Get the modal
        var pop1 = document.getElementById("popup1");
        //var materilal = document.getElementById("mid").value;
        // Get the button that opens the modal
        var btn1 = document.getElementById("btn1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];       

        // When the user clicks the button, open the modal 
        function display1(mid , name){
        document.getElementById('btn3').value = mid;
        document.getElementById('mid2').innerHTML = "Are you sure to delete the " + name+mid;
        pop1.style.display = "block";
        }
        function hide1(){
        pop1.style.display = "none";
        }
        
        // When the user clicks on <span> (x), close the modal


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == pop1) {
            pop1.style.display = "none";
        }
        }
    </script>
    <%}}%>
    </body>
    
  
</html>