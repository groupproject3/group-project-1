<%@ page import = "java.sql.*"%>
<%
String pid = request.getParameter("pid");
try{
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    
    PreparedStatement psm3 = conn.prepareStatement("select materials.materialID,productionmaterials.amount,materials.stock from productionmaterials inner join materials on materials.materialID = productionmaterials.materialID where productionID = ?");
    psm3.setString(1,pid);
    ResultSet rs = psm3.executeQuery();
    while(rs.next()){
        String amount = rs.getString("amount");
        int stock = rs.getInt("stock");
        int newstock = stock + Integer.parseInt(amount);
        PreparedStatement ps4 = conn.prepareStatement("update materials set stock=?  where materialID=?");
        ps4.setString(1,Integer.toString(newstock));
        ps4.setString(2,rs.getString("materialID"));
        ps4.executeUpdate();
        out.println("old = "+ stock + "new = " + Integer.toString(newstock) + "material = " +rs.getString("materialID"));
        
    }
    PreparedStatement psm = conn.prepareStatement("delete from production where productionID = ?");
    psm.setString(1, pid);
    psm.executeUpdate();
    PreparedStatement psm2 = conn.prepareStatement("delete from productionmaterials where productionID = ?");
    psm2.setString(1, pid);
    psm2.executeUpdate();
    %>
        
        <script>
            
            alert("Record Deletee");
            
       </script>
    <%
    //out.println(pid);
    response.sendRedirect("/project/admin/inventory/productionlist.jsp");
}catch(Exception e){
    out.println(e);
}

%>