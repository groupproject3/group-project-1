<%@ page import = "java.sql.*"%>
<%
String ptype = request.getParameter("ptype");
String priority = request.getParameter("priority");
String sdate = request.getParameter("sdate");
String edate = request.getParameter("edate");
String amt = request.getParameter("amt");

Class.forName("com.mysql.jdbc.Driver");
Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");

try{
    ResultSet rs,rs2;
    PreparedStatement psm = conn.prepareStatement("insert into production(outcome,priority,sdate,edate,out_amt,status) values(?,?,?,?,?,?)");
    psm.setString(1,ptype);
    psm.setString(2,priority);
    psm.setString(3,sdate);
    psm.setString(4,edate);
    psm.setString(5,amt);
    psm.setString(6,"ongoing");
    psm.executeUpdate();

    PreparedStatement psm2 = conn.prepareStatement("select max(productionID) from production");
    rs2 = psm2.executeQuery();
    String proid = null;
    while(rs2.next()){
        proid = rs2.getString("max(productionID)");
    }
    PreparedStatement ps = conn.prepareStatement("select * from materials where type=?");
    ps.setString(1,"RAW");
    rs = ps.executeQuery();
    while(rs.next()){
        String amount = request.getParameter(rs.getString("materialID"));
        int stock = rs.getInt("stock");
        int newstock = stock - Integer.parseInt(amount);
        PreparedStatement ps4 = conn.prepareStatement("update materials set stock=?  where materialID=?");
        ps4.setString(1,Integer.toString(newstock));
        ps4.setString(2,rs.getString("materialID"));
        ps4.executeUpdate();

        PreparedStatement psm3 = conn.prepareStatement("insert into productionmaterials(productionID,materialID,amount) values(?,?,?)");
        psm3.setString(1,proid);
        psm3.setString(2,rs.getString("materialID")); 
        psm3.setString(3,amount);
        psm3.executeUpdate();                
    }
    response.sendRedirect("/project/admin/inventory/productionlist.jsp");
}catch(Exception e){
    out.println(e);
}
%>