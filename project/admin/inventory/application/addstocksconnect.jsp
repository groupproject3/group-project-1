<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<%
if(request.getParameter("submit")!=null){
    String itemid = request.getParameter("itemid");
    String amt = request.getParameter("amt");
    String type = request.getParameter("type");
    ResultSet rs,rs2,rs3,rs4;
    LocalDate date = LocalDate.now();
    String strDate = date.toString();
    
    try{
        
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
        PreparedStatement ps = conn.prepareStatement("select * from materials where materialID=?");
        ps.setString(1,itemid);
        rs = ps.executeQuery();
        while(rs.next()){
            int stock = rs.getInt("stock");
            int newstock = stock + Integer.parseInt(amt);
            PreparedStatement ps2 = conn.prepareStatement("update materials set stock=?  where materialID=?");
        
            ps2.setString(1,Integer.toString(newstock));
            ps2.setString(2,itemid);
            int x = ps2.executeUpdate();
            //out.println(newstock);
        }
        PreparedStatement ps3 = conn.prepareStatement("select * from materials where materialID=?");
        ps3.setString(1,itemid);
        rs2 = ps3.executeQuery();
        while(rs2.next()){
            int pre = rs2.getInt("prelvl");
            int stock = rs2.getInt("stock");
            if(stock <= pre){
                PreparedStatement ps7 = conn.prepareStatement("select * from notification where oip=? and status=?");
                ps7.setString(1,itemid);
                ps7.setString(2,"not viewed");
                rs4 = ps7.executeQuery();
                if(!rs4.next()){
                    PreparedStatement ps4 = conn.prepareStatement("insert into notification (oip,to_user,type,description,link,status,date,from_user) values(?,?,?,?,?,?,?,?)");
                    ps4.setString(1,itemid);
                    ps4.setString(2,"1");
                    ps4.setString(3,"stock");
                    ps4.setString(4,"Stock is below re-order level!");
                    ps4.setString(5,"/project/admin/inventory/inventorystocks.jsp");
                    ps4.setString(6,"not viewed");
                    ps4.setString(7,strDate);
                    ps4.setString(8,"1");
                    ps4.executeUpdate();
                }
            }else{
                PreparedStatement ps5 = conn.prepareStatement("select * from notification where oip=? and status=?");
                ps5.setString(1,itemid);
                ps5.setString(2,"not viewed");
                rs3 = ps5.executeQuery();
                while(rs3.next()){
                    PreparedStatement ps6 = conn.prepareStatement("update notification set status = ?, date=? where oip=? and status=?");
                    ps6.setString(1,"viewed");
                    ps6.setString(2,strDate);
                    ps6.setString(3,itemid);
                    ps6.setString(4,"not viewed");
                    ps6.executeUpdate();
                }

            }
        }
       if(type.compareTo("raw")==0){
           response.sendRedirect("/project/admin/inventory/inventorystocks.jsp");
       }else if(type.compareTo("out")==0){
           response.sendRedirect("/project/admin/inventory/inventorystocks2.jsp");
       }else if(type.compareTo("int")==0){
           response.sendRedirect("/project/admin/inventory/inventorystocks3.jsp");
       }
       

    
}catch(Exception e){
    out.println(e);
}
}
%>