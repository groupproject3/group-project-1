<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/inventoryproduction.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Production Details</title>
    </head>
    <body>
    <%String uid = "1";
    String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");*/
        
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
             <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" id="a1"><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="#" id="a1">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
            <%
                    
                    String pid = request.getParameter("pid");
                    ResultSet rs;
                    PreparedStatement psm = conn.prepareStatement("select * from production where productionID=?");
                    psm.setString(1,pid);
                    rs =  psm.executeQuery();
                    while(rs.next()){
                        LocalDate date = LocalDate.now();
                        date = date.plusDays(1);
                        String strDate = date.toString();
            %>
                <table>
                
                      <tr>
                          <td class="td">
                          <p>Product</p>
                          <%
                          PreparedStatement psm3 = conn.prepareStatement("select * from materials where materialID=?");
                        psm3.setString(1,rs.getString("outcome"));
                        ResultSet rs3 = psm3.executeQuery();
                         while(rs3.next()){
                          %>
                          <input type="text" id="ptype" value="<%=rs3.getString("name")%>" name="ptype">
                        <%}%>     
                         
                          </td>
                          <td class="td">
                          <p>Priority</p>
                         <select id="priority" value="<%=rs.getString("priority")%>" name="priority">
                                 <option value="high">High</option>
                                 <option value="medium">Medium</option>
                                 <option value="low">Low</option>
                          </select>
                          </td>
                          <td class="td">
                          <button type="submit" id="btn2" onclick="display2();">Cancel Production</button>
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                             <p>Start Date</p>
                          <input type="date" name="sdate" value="<%=rs.getString("sdate")%>" min="<%=strDate%>" id="sdate">
                          </td>
                          <td class="td">
                            <p>End Date</p>
                          <input type="date" value="<%=rs.getString("edate")%>" id="edate" min="<%=strDate%>" name="edate">
                          </td>
                          <td class="td">
                         <button type="submit" id="btn1" onclick="display1();">Mark as Finished</button>
                        </td>
                      </tr>
                      <tr>
                        <td><p>Expected Amount</p>
                          <input type="number" id="amt" value="<%=rs.getString("out_amt")%>" name="amt" readonly></td>
                      </tr>
                      <tr>
                        <td><hr width="110%"></td>
                         <td><hr width="110%"></td>
                          <td><hr width="110%"></td>
                      </tr>
                      <tr>
                        <%
                        ResultSet rs2;
                        int count = 0;
                        PreparedStatement psm2 = conn.prepareStatement("select materials.name,productionmaterials.amount from materials inner join productionmaterials on productionmaterials.materialID = materials.materialID where productionmaterials.productionID = ?");
                        psm2.setString(1,pid);
                        rs2 =  psm2.executeQuery();
                        while(rs2.next()){
                            count = count + 1;
                            
                            if(count%3==1){
                                %><tr><%
                            }
                            %>
                        <td>
                            <p><%=rs2.getString("name") %></p>
                          <input type="number" id="edate" value="<%=rs2.getString("amount")%>" name="edate" min="0" readonly>
                        </td>
                            <%if(count%3==0){
                                %></tr><%
                            }
                        }%>
                          <%}%>
                      </tr>
                     
                </table>
            </div>
            
            </div>
            <div id="popup1" class="popup">

                <!-- Modal content -->
                <div class="popup-content">
                    <span class="close" onclick="hide1();">&times;</span>
                    <h3>Warning</h3>
                    <p>Are you sure to finish the production!</p>
                    <table style="float:right;">
                        <tr>
                            <td>
                              <button type="submit" id="btn3" class="btn3" name="pid"  onclick="display4();">Yes</button>
                               
                            </td>
                            <td><button type="submit" id="btn4" class="btn4" onclick="hide4();">No</button></td>
                        </tr>
                    </table>
                    
                  </div>  
                </div>
                <div id="popup4" class="popup">

                <!-- Modal content -->
                <div class="popup-content">
                    <span class="close" onclick="hide4();">&times;</span>
                   <form>
                    <p>Please check the amount of finished prodicts!</p>
                    <input type="number" min="0" value="" id="popnum" name="popnum">
                    <table style="float:right;">
                        <tr>
                            <td>
                                
                                    <button type="submit" id="btn3" class="btn3" name="pid" value="<%=request.getParameter("pid")%>" onclick="form.action='application/finishproduction.jsp'">Yes</button>
                                </form>
                            </td>
                            <td><button type="submit" id="btn4" class="btn4" onclick="hide4();">No</button></td>
                        </tr>
                    </table>
                    
                  </div>  
                </div>
                <div id="popup2" class="popup">

                <!-- Modal content -->
                <div class="popup-content">
                    <span class="close" onclick="hide2();">&times;</span>
                    <h3>Warning</h3>
                    <p>Are you sure to cancel the production!</p>
                    <table style="float:right;">
                        <tr>
                            <td>
                                <button type="submit" id="btn5" class="btn3" name="pid" onclick="display3();">Yes</button>
                            </td>
                            <td><button type="submit" id="btn6" class="btn4" onclick="hide2();">No</button></td>
                        </tr>
                    </table>
                    
                   </div> 
                </div>
                <div id="popup3" class="popup">

                <!-- Modal content -->
                <div class="popup-content">
                    <span class="close" onclick="hide3();">&times;</span>
                    <h3>Warning</h3>
                    <p>Do you want to re-srote the stocks?</p>
                    <table style="float:right;">
                        <tr>
                            <td>
                                <form>
                                    <button type="submit" id="btn7" class="btn3" name="pid" value="<%=request.getParameter("pid")%>" onclick="form.action='application/restore.jsp'">Yes</button>
                                </form>
                            </td>
                            <td><form>
                                    <button type="submit" id="btn8" class="btn4" name="pid" value="<%=request.getParameter("pid")%>" onclick="form.action='application/cancelproduction.jsp'">No</button>
                                </form></td>
                        </tr>
                    </table>
                    
                  </div>  
                </div>

           
        
    <script>
        // Get the modal
        var pop1 = document.getElementById("popup1");
        var pop2 = document.getElementById("popup2");
        var pop3 = document.getElementById("popup3");
        var pop4 = document.getElementById("popup4");
        // Get the button that opens the modal
        var btn1 = document.getElementById("btn1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];       

        // When the user clicks the button, open the modal 
        function display1(){
        pop1.style.display = "block";
        }
        function hide1(){
        pop1.style.display = "none";
        }
        function display2(){
        pop2.style.display = "block";
        }
        function hide2(){
        pop2.style.display = "none";
        }
        function display3(){
        pop3.style.display = "block";
        pop2.style.display = "none";
        }
        function hide3(){
        pop3.style.display = "none";
        }
        function display4(){
        var amt = document.getElementById("amt").value;
        document.getElementById("popnum").value = amt;
        pop4.style.display = "block";
        pop1.style.display = "none";
        }
        function hide4(){
        pop4.style.display = "none";
        }
        // When the user clicks on <span> (x), close the modal


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == pop1) {
            pop1.style.display = "none";
        }
        if (event.target == pop2) {
            pop2.style.display = "none";
        }
        if (event.target == pop3) {
            pop3.style.display = "none";
        }
        if (event.target == pop4) {
            pop4.style.display = "none";
        }
        }
    </script>
    <%}}%>
    </body>
    
  
</html>