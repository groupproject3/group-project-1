<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/inventoryproduction.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Add to Production</title>
    </head>
    <body>
    <%String uid = "1";
    String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
             <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" id="a1"><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="#" id="a1">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
            <form>
            <%
                LocalDate date = LocalDate.now();
                String strDate = date.toString();
                date = date.plusDays(1);
                String endDate = date.toString();
            %>
                <table cellpadding ="0" width="100%">
                      <tr>
                          <td class="td">
                          <p>Product</p>
                          <select id="ptype" name="ptype">
                          <%
                          ResultSet rs2;
                            PreparedStatement psm2 = conn.prepareStatement("select * from materials where type = ?");
                            psm2.setString(1,"OUT");
                            rs2 =  psm2.executeQuery();
                            while(rs2.next()){
                          %>
                                 <option value="<%=rs2.getString("materialID")%>"><%=rs2.getString("name")%></option>
                            <%}%>
                          </select>
                          </td>
                          <td class="td">
                          <p>Priority</p>
                         <select id="priority" name="priority">
                                 <option value="high">High</option>
                                 <option value="medium">Medium</option>
                                 <option value="low">Low</option>
                          </select>
                          </td>
                          <td class="td">
                          <button type="submit" id="btn1" onclick="form.action='application/startproduction.jsp'">Start</button>
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                             <p>Start Date</p>
                          <input type="date" name="sdate" min="<%=strDate%>" id="sdate" >
                          </td>
                          <td class="td">
                            <p>End Date</p>
                          <input type="date" id="edate" min="<%=endDate%>" name="edate" >
                          </td>
                          <td class="td">
                         <form action="#">
                          <button type="submit" name="btn" value="productionlist" id="btn1" onclick="form.action='application/inventoryredirect.jsp'">Production List</button>
                        </form>
                        </td>
                      </tr>
                      <tr>
                        <td><p>Expected Amount</p>
                          <input type="number" id="amt" name="amt" ></td>
                      </tr>
                      <tr>
                        <td><hr width="110%"></td>
                         <td><hr width="110%"></td>
                          <td><hr width="110%"></td>
                      </tr>
                      <tr>
                        <%
                        ResultSet rs;
                        int count = 0;
                        PreparedStatement psm = conn.prepareStatement("select * from materials where type=?");
                        psm.setString(1,"RAW");
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            count = count + 1;
                            String id = rs.getString("materialID");
                            if(count%3==1){
                                %><tr><%
                            }
                            %>
                        <td>
                            <p><%=rs.getString("name") %></p>
                          <input type="number" id="<%=rs.getString("materialID")%>" name="<%=rs.getString("materialID")%>" min="0" max="<%=rs.getString("stock") %>">
                        </td>
                            <%if(count%3==0){
                                %></tr><%
                            }%>
                          <%}%>
                      </tr>
                     
                </table>
                </form>
            </div>
            
            </div>
        
    <%}}%>
    </body>
    
  
</html>