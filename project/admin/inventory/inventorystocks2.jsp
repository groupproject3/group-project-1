<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/inventorystocks.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Stocks</title>
    </head>
    <body>
    <%String uid = "1";
    String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
            
        </header>
        <!--sidebar-->
        <div class="sidebar">
             <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" id="a1"><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="#" id="a1">Stocks</a></li>
                            <li id="li"><a href="inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
            <div class="tab">
                <form><button class="tablinks" onclick="form.action='inventorystocks.jsp'">Raw</button></form>
                <button class="tablinks1">Products</button>
                <form><button class="tablinks" onclick="form.action='inventorystocks3.jsp'">Intermediate</button></form>
            </div>
            <div id="raw" class="tabcontent" style="display:block;">
            <h4 style="font-size:20px; padding:0;">Product Stocks</h4>
                <%
                    ResultSet rs;
                    String id = request.getParameter("id");
                    PreparedStatement psm = conn.prepareStatement("select * from materials where type=?");
                    psm.setString(1,"OUT");
                    rs =  psm.executeQuery();
                    while(rs.next()){
                           
                            int value = rs.getInt("stock");
                           
                %>
                <%
                if(value>=100){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=90){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=80){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=70){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=60){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=50){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=40){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td class="td"><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=30){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td class="td"><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>=20){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td class="td"><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value>10){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td class="td"><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else if(value==0){
                    %>
                    <li>
                    <table>
                    <tr>
                        <td class="td"><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }else{
                    %>
                    <li>
                    <table>
                    <tr>
                        <td class="td"><a href="addstocks.jsp?id=<%= rs.getString("materialID")%>&type=out"><i style="padding-right:5px;" class="fas fa-plus-circle"></i><%= rs.getString("name")%></a><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10"></span></span></td>
                    </tr>
                    </table>
                    </li>
                    <%
                }
                    %>
                
                <%}%>
            </div>  
            
            </div>
            
            </div>
        <%}}%>
    
    </body>
    <%-- <script>
        var tab1 = document.getElementById("raw");
        var tab2 = document.getElementById("out");
        var tab3 = document.getElementById("int");
        function openCity1() {
            tab1.style.display = "block";
            tab2.style.display = "none";
            tab3.style.display = "none";
        }
        function openCity2() {
            tab1.style.display = "none";
            tab2.style.display = "block";
            tab3.style.display = "none";
        }
        function openCity2() {
            tab1.style.display = "none";
            tab2.style.display = "none";
            tab3.style.display = "block";
        }
    </script> --%>
  
</html>