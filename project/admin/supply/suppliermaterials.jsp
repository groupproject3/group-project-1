<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>


<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/suppliermaterials.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Supplier Materials</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
           <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" ><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href=#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"id="a1"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" id="a1" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li" id="a1"><a href="supplierlist.jsp" id="a1">Suppliers</a></li>
                            <li id="li"><a href="orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Generate Report</a></li>
                            <%-- <li id="li"><a href="../report/additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bestselling.jsp">Best Selling</a></li>
                            <li id="li"><a href="../report/productinfo.jsp">Orders Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
        </div>

        <div class="content">
            <div class="box" style="top:70%;">
                <table cellpadding ="0"  width="100%" class="table1">
                    <thead>
                        <tr class="tr1">
                            <th class="th1">material ID</th>
                            <th class="th1">Name</th>
                            <th class="th1">Available</th>
                            <th class="th1">Quantity</th>
                            <th class="th1">Add</th>
                            
                        </tr>
                    </thead>
                    <%
                        ResultSet rs;
                        String id = request.getParameter("id");
                        PreparedStatement psm = conn.prepareStatement("select supplier_materials.materialID,materials.name,materials.stock from materials inner join supplier_materials on supplier_materials.materialID = materials.materialID where supplier_materials.supplierID = ?");
                        psm.setString(1, id);
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            
                            %>
                                <tr class="tr1">
                                <form>
                                    <td class="td"><input type="text" id="mid" name="mid" value="<%=rs.getString("materialID") %>"></td>
                                    <td class="td"><%=rs.getString("name") %></td>
                                    <td class="td"><%=rs.getString("stock") %></td>
                                    <td class="td"><input type="number" id="qty" name="qty" min="0" ></td>
                                    <td class="td"><button type="submit" name="btn" value="<%=id%>" id="btn2" onclick="form.action='application/addtocart.jsp'">Add</button></td>
                                </form>
                                </tr>
                            <%
                        }

                    %>
                </table>
            </div>
           
            <div class="box" style="margin-top: 120px;">    
                <p class="p">Added Items</p>
                <table cellpadding ="0" style="margin-top: 20px;" width="100%" class="table1">
                    <thead>
                        <tr class="tr1">
                            <th class="th1">material ID</th>
                            <th class="th1">Name</th>
                            <th class="th1">Quantity</th>
                            <th class="th1">Update</th>
                            <th class="th1">Delete</th>
                            
                        </tr>
                    </thead>
                    <%
                        ResultSet rs2;
                        PreparedStatement psm2 = conn.prepareStatement("select supshoppingcart.materialID,supshoppingcart.quantity,materials.name from supshoppingcart inner join materials on supshoppingcart.materialID=materials.materialID where supshoppingcart.supplierID=?");
                        psm2.setString(1, id);
                        rs2 =  psm2.executeQuery();
                        while(rs2.next()){
                            
                            %>
                                <tr class="tr1">
                                <form>
                                    <td class="td"><input type="text" id="mid" name="mid" value="<%=rs2.getString("materialID") %>"></td>

                                    <td class="td"><%=rs2.getString("name") %></td>
                                    
                                    <td class="td"><input type="number" id="qty" value="<%=rs2.getString("quantity") %>" name="qty" min="0" ></td>
                                    <td class="td"><button type="submit" name="btn" value="<%=id%>" id="btn2" onclick="form.action='application/updatecart.jsp'">Update</button></td>
                                    <td class="td"><button type="submit" name="btn2" value="<%=id%>" id="btn2" onclick="form.action='application/deletecartitem.jsp'">Delete</button></td>
                                </form>
                                </tr>
                            <%
                        }

                    %>
                </table>
            </div>
             <div class="box" style="height: 200px;margin-top: -230px;">    
               
               <%
                  
                    LocalDate date = LocalDate.now();
                    date = date.plusDays(1);
                    String strDate = date.toString();
                %>
                <form action="#">
                <h3>Place Order</h3>
                    <table cellpadding ="0" width="100%">
                        <tr>
                            <td ><p>Required Date : </p></td>
                            <td ><input type="date" id="req"  name="req" min="<%=strDate%>" required></td>
                            <td class="td"><button type="submit" name="id" value="<%=id%>" id="btn3" onclick="form.action='application/placeorder.jsp'">Place Order</button></td>
                        </tr>
                    </table>
                    </form>
               
                 
            </div>
           
        
            
        </div>
    <%}}%>
    
    </body>
    
  
</html>