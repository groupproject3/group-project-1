<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>



<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/quotation.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Order List</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" ><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"id="a1"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" id="a1" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="supplierlist.jsp">Suppliers</a></li>
                            <li id="li" id="a1"><a href="orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

         <div class="content">
            <div class="box">
                <%   
                    ResultSet rs;
                   String ordrid = request.getParameter("id");
                    PreparedStatement psm = conn.prepareStatement("select * from quotation where orderID = ?");
                    psm.setString(1, ordrid);
                    rs =  psm.executeQuery();
                       
                    while(rs.next()){        
                %>
            
                <form action="application/quotationsend.jsp" method="POST">
                    <table>
                        <tr>
                            <td class="td1"><label for="date">Date</label></td>
                            <td><input type="date" id="date" name="date" value="<%=rs.getString("date")%>" disabled></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td class="td1"><label for="id">Quotation ID</label></td>
                            <td><input type="text" id="id" name="qid" value="<%=rs.getString("qid")%>" disabled></td>   
                            <td class="td1"><label for="orderid">Order ID</label></td>
                            <td><input type="text" id="orderid" value="<%=rs.getString("orderid")%>" disabled name="orderid"></td>
                        </tr>
                        
                        <%-- <tr>
                            <td class="td1"><label for="body">Quotation</label></td>
                            <td colspan=4><textarea name="body" rows="10" cols="100"  disabled><%=rs.getString("body")%></textarea></td>
                        </tr> --%>

                        <tr>
                            <td class="td1"><label for="subtotal">Sub Total</label></td>
                            <td><input type="text" id="subtotal" name="subtotal" value="<%=rs.getString("subtotal")%>" disabled style="text-align: right;"></td>  
                        </tr>

                        <tr>
                            <td class="td1"><label for="discount">Discount</label></td>
                            <td><input type="text" id="discount" name="discount" value="<%=rs.getString("discount")%>" disabled style="text-align: right;"></td>  
                        </tr>

                        <tr>
                            <td class="td1"><label for="total">Total</label></td>
                            <td><input type="text" id="total" name="total" value="<%=rs.getString("total")%>" disabled style="text-align: right;"></td>  
                        </tr>
                    </table>
                    <%
                        ResultSet rs2;
                        PreparedStatement psm2 = conn.prepareStatement("select * from allorders where orderID = ?");
                        psm2.setString(1, ordrid);
                        rs2 =  psm2.executeQuery();
                        String ac = "Quotation Accepted";
                        String re = "Quotation Rejected";
                        String de = "Delivered";
                        String rec = "Received";
                        while(rs2.next()){
                            if(ac.compareTo(rs2.getString("status"))!=0 && re.compareTo(rs2.getString("status"))!=0 && de.compareTo(rs2.getString("status"))!=0 && rec.compareTo(rs2.getString("status"))!=0){
                    %>
                    <button type="submit" name="id" value="<%=ordrid%>" onclick="form.action='application/reject.jsp'">Reject</button>
                    
                    <button type="submit" name="id" value="<%=ordrid%>" onclick="form.action='application/acceptquotation.jsp'">Accept</button>
                    <%}%>
                    <form action="#">
                        
                        <button type="submit" id="btn1"  name="btn" value="orderlist" onclick="form.action='application/supplyredirect.jsp'">Back</button>
                    </form> 
                            
                <%}}%>
                </form>
            </div> 
        </div>
        <%}}%>
    </body>
</html>