<%@ page import = "java.sql.*"%>
<%
    String uid = request.getParameter("id");
    String company = request.getParameter("company");
    String name = request.getParameter("name");
    String contact = request.getParameter("contact");
    String email = request.getParameter("email");
    String address = request.getParameter("address");

    try{
        ResultSet rs1, rs2;
        String nextid = "";
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");

        PreparedStatement ps = conn.prepareStatement("update supplier set company=?, name=?, contact=?, email=?, address=? where supplierID=?");
        ps.setString(1,company);
        ps.setString(2,name);
        ps.setString(3,contact);
        ps.setString(4,email);
        ps.setString(5,address);
        ps.setString(6,uid);
        ps.executeUpdate();

        PreparedStatement ps1 = conn.prepareStatement("select * from materials where type='RAW'");
        rs1 =  ps1.executeQuery();
        
        while(rs1.next()){
            String id = rs1.getString("materialID");
            PreparedStatement ps2 = conn.prepareStatement("select * from supplier_materials where supplierID=? and materialID=?");
            ps2.setString(1,uid);
            ps2.setString(2,id);
            rs2 =  ps2.executeQuery();

            if(rs2.next()){  
                if(request.getParameter(id) == null){
                    PreparedStatement ps3 = conn.prepareStatement("delete from supplier_materials where supplierID=? and materialID=?");
                    ps3.setString(1,uid);
                    ps3.setString(2,id);
                    ps3.executeUpdate();
                }
            }
            else{
                if(request.getParameter(id) != null){
                    PreparedStatement ps4 = conn.prepareStatement("insert into supplier_materials(supplierID, materialID) values(?, ?)");
                    ps4.setString(1,uid);
                    ps4.setString(2,id);
                    ps4.executeUpdate();
                }
            }
        }

        response.sendRedirect("/project/admin/supply/supplierlist.jsp");
    }
     
    catch(Exception e){
        out.println(e);
    }
%>