<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<%
String oid = request.getParameter("btn");
try{
    LocalDate date = LocalDate.now();
    String strDate = date.toString();
    ResultSet rs,rs2,rs3;
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    PreparedStatement psm = conn.prepareStatement("select * from supordermaterials where orderID = ?");
    psm.setString(1, oid);
    rs = psm.executeQuery();
    while(rs.next()){
        int qty = rs.getInt("quantity");
        String  mat = rs.getString("materialID");
        PreparedStatement ps = conn.prepareStatement("select * from materials where materialID = ?");
        ps.setString(1,mat);
        rs2 = ps.executeQuery();
        while(rs2.next()){
            int stock = rs2.getInt("stock");
            int newstock = stock +qty;
            PreparedStatement ps2 = conn.prepareStatement("update materials set stock=?  where materialID=?");
            ps2.setString(1,Integer.toString(newstock));
            ps2.setString(2,mat);
            ps2.executeUpdate();
            PreparedStatement ps6 = conn.prepareStatement("delete from notification where oip = ? and from_user = ? and to_user = ?");
            ps6.setString(1,mat);
            ps6.setString(2,"1");
            ps6.setString(3,"1");
            ps6.executeUpdate();
        }
    }
    PreparedStatement ps3 = conn.prepareStatement("select * from quotation where orderid=?");
    ps3.setString(1,oid);
    rs3 = ps3.executeQuery();
    while(rs3.next()){
        PreparedStatement ps4 = conn.prepareStatement("insert into income_expence(flag,description,amount,oip,date) values(?,?,?,?,?)");
        ps4.setString(1,"expence");
        ps4.setString(2,"material purchase");
        ps4.setString(3,rs3.getString("total"));
        ps4.setString(4,oid);
        ps4.setString(5,strDate);
        ps4.executeUpdate();
        PreparedStatement ps5 = conn.prepareStatement("update allorders set status=?  where orderID=?");
        ps5.setString(1,"Received");
        ps5.setString(2,oid);
        ps5.executeUpdate();
    }
    response.sendRedirect("/project/admin/supply/orderlist.jsp");
}catch(Exception e){
    out.println(e);
}

%>