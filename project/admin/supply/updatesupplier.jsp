<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/addsupplier.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Update Supplier</title>
    </head>
    <body>
    <%String uid = "1";
    String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../../logout.jsp?user=1" class="logout">Logout</a>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" id="a1" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="supplierlist.jsp" id="a1">Suppliers</a></li>
                            <li id="li"><a href="orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <form action="#">
                 <%
                        ResultSet rs;
                        String id = request.getParameter("id");
                        PreparedStatement psm = conn.prepareStatement("select * from supplier where supplierID=?");
                        psm.setString(1, id);
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            
                    %>
                <table>
                <h3>Update Supplier</h3>
                      <tr>
                          <td class="td">
                            
                          <p>Company</p>
                          <input type="text"  name="company" value="<%= rs.getString("company")%>" id="company" readonly>
                          
                          </td>
                          <td class="td">
                           <p>Name</p>
                          <input type="text" value="<%= rs.getString("name")%>" name="name" id="name" readonly>
                          
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                            <p>Contact Number</p>
                            <input type="text" value="<%= rs.getString("contact")%>" name="contact" id="contact" required>
                          </td>
                          <td class="td">
                            <p>Email</p>
                            <input type="email" value="<%= rs.getString("email")%>" name="email" id="email" required>
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                            <p>Address</p>
                            <input type="text" value="<%= rs.getString("address")%>" name="address" id="address" required>
                          </td>
                          <td class="td">
                         </td>
                      </tr>
                      <%}%>
                        <%
                        ResultSet rs2;
                        int count = 0;
                        PreparedStatement psm2 = conn.prepareStatement("select * from materials where type='RAW'");
                        rs2 =  psm2.executeQuery();
                        while(rs2.next()){
                            count = count + 1;
                            String id2 = rs2.getString("materialID");
                            if(count%4==1){
                                %><tr><td class="td1"><table><tr>
                                <td class="td1">
                                 <p><%=rs2.getString("name") %>
                                <input type="checkbox" id="<%=rs2.getString("materialID") %>" name="<%=rs2.getString("materialID") %>" ></p>
                                </td><%
                            }
                            if(count%4==2){
                                %><td class="td1">
                                <p><%=rs2.getString("name") %>
                                <input type="checkbox" id="<%=rs2.getString("materialID") %>" name="<%=rs2.getString("materialID") %>" ></p>
                                </td><%
                            }
                            if(count%4==3){
                                %></tr></table></td><td class="td1"><table><tr><td class="td1">
                                <p><%=rs2.getString("name") %>
                                <input type="checkbox" id="<%=rs2.getString("materialID") %>" name="<%=rs2.getString("materialID") %>" ></p>
                                </td><%
                            }
                            if(count%4==0){
                                %><td class="td1">
                                <p><%=rs2.getString("name") %>
                                <input type="checkbox" id="<%=rs2.getString("materialID") %>" name="<%=rs2.getString("materialID") %>" ></p>
                                 </td>
                                </tr></table></td></tr><%
                            }
                            %>
                        
                          <%}%>
                      </table></td></tr>
                    <tr>
                        <td class="td"></td>
                      
                        <td class="td">
                        <table>
                        <tr>
                            <td class="td"><button type="submit" id="btn1" name="id" value="<%=id%>" onclick="form.action='application/updatesup.jsp'">Update</button></form></td>
                            <td class="td"><form>
                                <button type="submit" id="btn1" name="btn" value="supplierlist" onclick="form.action='application/supplyredirect.jsp'">Back</button>
                                </form>
                            </td>
                        </tr>
                        </table>
                        </td>
                    </tr>
                </table>
      
            </div>
            
        </div>

    <%}}%>
    </body>
    
  
</html>