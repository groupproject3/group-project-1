<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.lang.Math"%>
<%@ page import = "java.time.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="adminhome.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <head>
        <title>Home</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
         
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
            //String nm = session.getAttribute("user").toString();
            //session.setAttribute("user","admin");
            //session.setMaxInactiveInterval(2);
        ResultSet rs5,rs6;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
            <table style="float:right;">
            <tr>
                <td>
                <%
                PreparedStatement psm9 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm9.setString(1,uid);
                psm9.setString(2,"not viewed");
                rs6 = psm9.executeQuery();
                if(rs6.next()){
                %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp?user=1" class="logout">Logout</a></td>
                </tr>
            </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4"><%=session.getAttribute("user")%></h4>
            </div>
            <ul class="ul">
                <li><a href="#" id="a1"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="inventory/inventorystocks.jsp">Update Stocks</a></li>
                            <li id="li"><a href="inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="adminsaleshome.jsp"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="adminsupplyhome.jsp"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="adminreporthome.jsp"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                           <li id="li"><a href="report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
             
        </div>

        <div class="content">
            <div class="box">
            <table class="table1">
            <tr>
                <td class="td">
                    <div class="div1">
                        <h3 style="padding-top:25px;">Sales of Products</h3> 

           
            <%
                ResultSet rs3, rs4;
                String from = request.getParameter("from");
                String to = request.getParameter("to");
                String cat = request.getParameter("cat");
    
                String labels = "";
                String data = "";
                double sum = 0.0;
                double percent = 0.0;
    
                if(from!=null){
                    if(cat==""){
                        PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID inner join custorder cu on cu.orderID = c.orderID where cu.required_date between ? and ? group by p.name order by sum(c.quantity) desc");
                        psm6.setString(1,from);
                        psm6.setString(2,to);
                        rs3 = psm6.executeQuery();
        
                        while(rs3.next()){
                            sum = sum + Integer.parseInt(rs3.getString("sum(c.quantity)"));
                        }
                        rs4 = psm6.executeQuery();
                    }

                    else{
                        PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID inner join custorder cu on cu.orderID = c.orderID where p.category = ? and cu.required_date between ? and ? group by p.name order by sum(c.quantity) desc");
                        psm6.setString(1,cat);
                        psm6.setString(2,from);
                        psm6.setString(3,to);
                        rs3 = psm6.executeQuery();
    
                        while(rs3.next()){
                            sum = sum + Integer.parseInt(rs3.getString("sum(c.quantity)"));
                        }
                        rs4 = psm6.executeQuery();
                    }
                }
    
                else{
                    PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID group by p.name");
                    rs3 = psm6.executeQuery();
    
                    while(rs3.next()){
                        sum = sum + Integer.parseInt(rs3.getString("sum(c.quantity)"));
                    }
                    rs4 = psm6.executeQuery();
                }
    
                while(rs4.next()){
                    percent = (double)Math.round(((Integer.parseInt(rs4.getString("sum(c.quantity)")) * 100) / sum) * 100) / 100;
                    labels = labels+","+rs4.getString("p.name").toString()+": "+String.valueOf(percent)+"%";
                    data = data+","+rs4.getString("sum(c.quantity)");
                }
            %>
        
            
    

            <!--Redar Chart-->
            <div class="centerchart" style="width:700px; height:400px; margin-left:130px; margin-top:25px">
                <canvas id="myChart"></canvas>         
                <a href="report/bestselling.jsp" class="btn1" style="float:right; margin-right:5px;">More</a>       
            </div>
                
            <script>
                var temp = '<%=labels%>';
                var labelArr = temp.split(',').slice(1,);
                console.log(labelArr);
                var temp1 = '<%=data%>';
                var dataArr = temp1.split(',').slice(1,);
                console.log(dataArr);
                console.log('<%=sum%>');
    
                new Chart(document.getElementById("myChart"), {
                    type: 'radar',
                    data:{
                    labels: labelArr,
                    datasets:[{
                        label: "Sales of Products",
                        backgroundColor: "rgba(200,0,0,0.2)",
                        borderColor: "rgba(200,0,0,0.6)",
                        fill: true,
                        data: dataArr
                    }]
                    },
                    options:{
                    // title: {
                    //     display: true,
                    //     text: 'Best Selling Products'
                    // },
                        legend:{
                            display:false,
                            position:'top',
                            labels:{
                                fontColor:'#000'
                            }
                        },
                        // layout:{
                        //     padding:{
                        //         left:0,
                        //         right:0,
                        //         bottom:0,
                        //         top:0
                        //     }
                        // },
                        scale:{
                            ticks:{
                                suggestedMin: 0,
                                stepSize: 10
                                // beginAtZero: true,
                                // min: 0,
                                // max: 100,
                            },
                            pointLabels:{
                                fontSize: 13
                            }
                        }
                    }
                });

                // Top 5
                var temp2 = '<%=labels%>';
                var labelArr1 = temp2.split(',').slice(1,6);
                console.log(labelArr1);
                var temp3 = '<%=data%>';
                var dataArr1 = temp3.split(',').slice(1,6);
                console.log(dataArr1);

                var i;
                for(i=0;i<5;i++){
                    document.getElementById(i).innerHTML = "None";
                } 

                if(labelArr1.length>5){
                    for(i=0;i<5;i++){
                    document.getElementById(i).innerHTML = labelArr1[i];
                    }  
                }
                else{
                    for(i=0;i<labelArr1.length;i++){
                        document.getElementById(i).innerHTML = labelArr1[i];
                    }  
                }
            </script>
                    
                    </div>
                </td>
            </tr>
            <tr>
                <td class="td">
                    <div class="div1" style="float:right;margin-top:17.5px">
                    <h3>Mostly Used Raw Materials</h3>
                        <%
            ResultSet rs12,rs9;
            String materialName=request.getParameter("materialName");

            String labels1 = "";
            String data1 = "";        
          
                PreparedStatement psm12 = conn.prepareStatement("select sum(p.amount), m.name from materials m inner join productionmaterials p on m.materialID = p.materialID group by m.name order by sum(p.amount) desc limit 5");
                rs12 = psm12.executeQuery();
            
            while(rs12.next()){
                labels1 = labels1+","+rs12.getString("m.name").toString();
                data1 = data1+","+rs12.getString("sum(p.amount)");
            }

        %>

   

        <!--bar chart-->
        <div style="width:700px; height: 340px; margin-top:75px; margin-left:130px" class= "centerchart">
            <canvas id="bar-chart"></canvas>
            <a href="report/bar.jsp" class="btn1" style="float:right; margin-right:5px; margin-bottom:25px;">More</a>
        </div>

        <script>
            var temp = '<%=labels1%>';
            var labelArr = temp.split(',').slice(1,6);
            console.log(labelArr);
            var temp1 = '<%=data1%>';
            var dataArr = temp1.split(',').slice(1,6);
            console.log(dataArr);

            new Chart(document.getElementById("bar-chart"), {
                type: 'bar',
                data: {
                labels: labelArr,
                datasets: [
                {
                    label: "Number of Products",
                    backgroundColor: "#5779e9",
                    data: dataArr
                }
                ]
                }, 
                options: {
                    legend: { display: false },
                    title: {
                        display: false,
                        text: 'Mostly Used Raw Materials'
                    },
                    scales: {
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                }  
                            }]
                        },
                    layout:{
                        padding:{
                            left:0,
                            right:0,
                            bottom:0,
                            top:0
                        }
                    }
                    
                }
            });
           
        </script>
            
                    </div>
                </td>
            </tr>
            <tr>
                <td class="td" colspan="2">
                    <div class="div1">
                        <h3 style="padding-top:25px;">Financial Analysis</h3>
                        <div class="chart"  style="margin-left:50px">
                            <canvas id="myChart1"></canvas>
                            <a href="report/generatereport.jsp" class="btn1" style="float:right; margin-right:-5px;">More</a>
                        </div>
                        <script>
                            var i =0;
                        </Script>
                        <%
                            double[] months1;
                            double[] months2;
                            double[] profit;
                            months1 = new double[12];
                            months2 = new double[12];
                            profit = new double[12];

                            ResultSet rs33;
                            
                            PreparedStatement psm3 = conn.prepareStatement("select sum(amount), MONTH(date) from income_expence where flag = ? AND date LIKE ? GROUP by MONTH(date)");
                            psm3.setString(1,"income");
                            psm3.setString(2,"2021%");
                            
                            rs33 =  psm3.executeQuery();

                            while(rs33.next()){
                            int month1 = rs33.getInt("MONTH(date)");
                            Double sum1 = rs33.getDouble("sum(amount)");
                                
                            months1[month1-1] = sum1;
                            
                            }

                            PreparedStatement psm5 = conn.prepareStatement("select sum(amount), MONTH(date) from income_expence where flag = ? AND date LIKE ? GROUP by MONTH(date)");
                            psm5.setString(1,"expence");
                            psm5.setString(2,"2021%");
                            rs5 =  psm5.executeQuery();
                            
                            while(rs5.next()){
                                int month2 = rs5.getInt("MONTH(date)");
                                Double sum2 = rs5.getDouble("sum(amount)");

                                months2[month2-1] = sum2;
                            }

                            for (int i = 0; i < 12; i++) {
                            profit[i] = months1[i] - months2[i]; 
                            
                            }

                        %>    

                        <script>
                            Chart.defaults.global.title.display = true;
                            Chart.defaults.global.elements.point.pointStyle = "circle";

                            var ctx = document.getElementById('myChart1').getContext('2d');
                            var chart = new Chart(ctx, {
                            // The type of chart we want to create
                            type: 'line',

                            // The data for our dataset
                            data: {
                                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','Octomber','November','December'],
                                datasets: [{
                                    label: 'Profit',
                                    //backgroundColor: 'rgb(255, 99, 132)',
                                    borderColor: 'rgb(56, 138, 232)',
                                
                                    data: [<%=profit[0]%>,<%=profit[1]%>,<%=profit[2]%>,<%=profit[3]%>,<%=profit[4]%>,<%=profit[5]%>,<%=profit[6]%>,<%=profit[7]%>,<%=profit[8]%>,<%=profit[9]%>,<%=profit[10]%>,<%=profit[11]%>]
                                },
                                {
                                    label: 'Expense',
                                    fillColor: "rgb(255, 0, 0)",
                                    borderColor: 'rgb(255, 0, 0)',
                                    data: [<%=months2[0]%>,<%=months2[1]%>,<%=months2[2]%>,<%=months2[3]%>,<%=months2[4]%>,<%=months2[5]%>,<%=months2[6]%>,<%=months2[7]%>,<%=months2[8]%>,<%=months2[9]%>,<%=months2[10]%>,<%=months2[11]%>]
                                },
                                {
                                    label: 'Income',
                                    fillColor: "rgb(0, 201, 67)",
                                    borderColor: 'rgb(0, 201, 67)',
                                    data: [<%=months1[0]%>,<%=months1[1]%>,<%=months1[2]%>,<%=months1[3]%>,<%=months1[4]%>,<%=months1[5]%>,<%=months1[6]%>,<%=months1[7]%>,<%=months1[8]%>,<%=months1[9]%>,<%=months1[10]%>,<%=months1[11]%>]
                                }
                                ]
                            },

                            // Configuration options go here
                            options: {
                                elements: {
                                    line: {
                                        tension: 0
                                    }
                                },
                                scales: {
                                    yAxes: [{
                                        scaleLabel: {
                                        display: true,
                                        labelString: 'Amount (Rs.)',
                                        fontStyle: 'bold',
                                        fontColor: '#FF0000'
                                        }
                                    }],
                                    xAxes: [{
                                        scaleLabel: {
                                        display: true,
                                        labelString: 'Month',
                                        fontStyle: 'bold',
                                        fontColor: '#FF0000'
                                        }
                                    }]
                                }     
                                }
                            });
                        </script>
                        
                    </div>
                </td>
            </tr>
            <tr>
            <td class="td" colspan="2">
            <div class="div1">
                <table>
                    <tr>
                        <td class="td3"><h3 style="padding-top:15px;">Recent Orders</h3></td>
                    </tr>
                    <tr>
                        <td class="td3">
                        <table cellpadding ="0" width="100%" class="table2" align="center">
                        <thead>
                            <tr class="tr2">
                                <th>Order ID</th>
                                <th>Customer</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Placed Data</th>
                                <th>Required Date</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <%
                            LocalDate date = LocalDate.now();
                            String strDate = date.toString();
                            date = date.plusDays(7);
                            String endDate = date.toString();
                            ResultSet rs;
                        // Class.forName("com.mysql.jdbc.Driver");
                            //Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                            PreparedStatement psm = conn.prepareStatement("select allorders.orderID,allorders.amount,allorders.status,allorders.placed,allorders.req,usertable.name from ((allorders inner join custorder on allorders.orderID=custorder.OrderID) inner join usertable on custorder.customerID=usertable.userID) where allorders.req <=? and not allorders.status=?");
                            psm.setString(1,endDate);
                            psm.setString(2,"Order Sent");
                            rs =  psm.executeQuery();
                            while(rs.next()){
                                String id = rs.getString("orderID");
                                
                                %>
                                    <tr class="tr1">
                                        <td><%=rs.getString("OrderID") %></td>
                                        
                                        <td><%=rs.getString("name") %></td>
                                        
                                        <td><%=rs.getString("amount") %></td>
                                        <td><%=rs.getString("status") %></td>
                                        <td><%=rs.getString("placed") %></td>
                                        <td><%=rs.getString("req") %></td>
                                        <td><a href="sales/vieworder.jsp?id=<%=rs.getString("OrderID")%>">View</a></td>
                                    </tr>
                                <%
                            }  
                            
                        %>
                    </table>
                    </td>
                    </tr>
                </table>
                </div>
                </td>
                </tr>
                <tr>
                <td class="td" colspan="2">
                    <div  class="div1">
                        <table>
                            <tr>
                                <td class="td3"><h3 style="padding-top:15px;">Recent Productions</h3></td>
                            </tr>
                            <tr>
                                <td class="td3">
                                    <table cellpadding ="0" width="100%" class="table2" align="center">
                                        <thead>
                                            <tr class="tr2">
                                                <th></th>
                                                <th>Production ID</th>
                                                <th>Outcome</th>
                                                <th>Priority</th>
                                                <th>Start Date</th>
                                                <th>Finish Date</th>
                                                <th>Expected Amount</th>
                                                <th>View</th>
                                            </tr>
                                        </thead>
                                         <%
                                            ResultSet rs13;
                                            PreparedStatement psm6 = conn.prepareStatement("select * from production where status = ? and edate<=? order by edate asc");
                                            psm6.setString(1,"ongoing");
                                            psm6.setString(2,endDate);
                                            rs13 =  psm6.executeQuery();
                                            while(rs13.next()){
                                                String prid = rs13.getString("productionID");
                                                if(strDate.compareTo(rs13.getString("edate"))==0){
                                                %>
                                                    <tr class="tr1">
                                                        <td><i class="fas fa-exclamation-circle" style="color:red;"></i></td>
                                                        <td><%=rs13.getString("productionID") %></td>
                                                        <td><%=rs13.getString("outcome") %></td>
                                                        <td><%=rs13.getString("priority") %></td>
                                                        <td><%=rs13.getString("sdate") %></td>
                                                        <td><%=rs13.getString("edate") %></td>
                                                        <td><%=rs13.getString("out_amt") %></td>
                                                        <td><a href="inventory/productiondetails.jsp?pid=<%=prid%>">View</a></td>
                                                    </tr>
                                                <%
                                            }else{
                                                 %>
                                                    <tr class="tr1">
                                                        <td></td>
                                                        <td><%=rs13.getString("productionID") %></td>
                                                        <td><%=rs13.getString("outcome") %></td>
                                                        <td><%=rs13.getString("priority") %></td>
                                                        <td><%=rs13.getString("sdate") %></td>
                                                        <td><%=rs13.getString("edate") %></td>
                                                        <td><%=rs13.getString("out_amt") %></td>
                                                        <td><a href="inventory/productiondetails.jsp?pid=<%=prid%>">View</a></td>
                                                    </tr>
                                                <%                                            
                                            }
                                            }
                                        %>
                                    </table>
                                
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                </tr>
                <tr>
                    <td class="td" colspan="2">
                    <div  class="div1">
                        <table>
                            <tr>
                                <td class="td3"><h3 style="padding-top:15px;">Raw Material Stocks</h3></td>
                            </tr>
                            <tr>
                                <td class="td3">
                                <%
                                    ResultSet rs14;
                                    PreparedStatement psm7 = conn.prepareStatement("select * from materials where type=?");
                                    psm7.setString(1,"RAW");
                                    rs14 =  psm7.executeQuery();
                                    while(rs14.next()){
                                            int pre = rs14.getInt("prelvl");
                                            int max = pre*10;
                                            int value = rs14.getInt("stock")*100/max;
                                        
                                %>
                                <%
                                if(value>=100){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material12"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=90){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material1"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=80){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material2"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=70){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material3"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=60){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material4"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=50){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material5"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=40){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2" class="td"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material6"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=30){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2" class="td"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material7"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>=20){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2" class="td"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material8"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value>10){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2" class="td"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material9"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else if(value==0){
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2" class="td"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material11"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }else{
                                    %>
                                    <li class="li">
                                    <table>
                                    <tr>
                                        <td class="td"><label class="lbl2"><%= rs14.getString("name")%></label><td>
                                        <td rowspan="2" class="td"> <%= rs14.getString("stock")%></td>
                                    </tr>
                                    <tr>
                                        <td><span class="bar"><span class="material10"></span></span></td>
                                    </tr>
                                    </table>
                                    </li>
                                    <%
                                }
                                    %>
                                
                                <%}%>
                                </td>
                            </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                </table>
            </div>

        </div>
        <%}}%>
    
    </body>
    
  
</html>