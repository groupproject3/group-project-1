<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/adminsettings.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Settings</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" ><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href=#><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                           <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a id="a1" href="#"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

       <div class="content">
            <div class="box">
                <h3>User Details</h3>

                <!--validating email and phone-->
                <script>  
                    function validateform(phone2){  
                        var a = document.detailup.email.value;  
                        var pos1 = a.indexOf("@");  
                        var pos2 = a.lastIndexOf(".");  

                        var phone1 = "0[0-9]{9}";       ///^\d{10}$/;
                        //var phone2 = document.detailup.contact;

                        if((pos1<1 || pos2<pos1+6 || pos2+2>=a.length) && !phone2.value.match(phone1)){  
                            // alert("Please enter a valid e-mail address!");  
                            document.getElementById("mail").classList.remove('hidden');
                            document.getElementById('phone').classList.remove('hidden');
                            return false;  
                        }  

                        else if(pos1<1 || pos2<pos1+6 || pos2+2>=a.length && phone2.value.match(phone1)){
                            document.getElementById("mail").classList.remove('hidden');
                            document.getElementById('phone').classList.add('hidden');
                            return false;
                        }

                        else if(!(pos1<1 || pos2<pos1+6 || pos2+2>=a.length) && !phone2.value.match(phone1)){
                            document.getElementById("mail").classList.add('hidden');
                            document.getElementById('phone').classList.remove('hidden');
                            return false;
                        }

                        else if(!(pos1<1 || pos2<pos1+6 || pos2+2>=a.length) && phone2.value.match(phone1)){
                            return true;   
                        }
                    }
                
                    // function validatephone(){
                    //     var phone1 = "0[0-9]{9}";       ///^\d{10}$/;
                    //     var phone2 = document.detailup.contact;
                    //     if(phone2.value.match(phone1)){
                    //         // return true;
                    //         document.getElementById('phone').classList.add('hidden');
                    //     }
                    //     else{
                    //         // alert("Please enter a valid phone number!");
                    //         document.getElementById("phone").classList.remove('hidden');
                    //         return false;
                    //     }
                    // }
                </script>

                <%
                    ResultSet rs;
                    PreparedStatement psm = conn.prepareStatement("select * from usertable where userID=?");
                    psm.setString(1, "1");
                    rs =  psm.executeQuery();
                    while(rs.next()){      
                %>

                <form action="#" name="detailup" onsubmit="return validateform(document.detailup.contact)">
                    <table width="100%">
                        <tr>
                            <td class="td1">
                                <p>Title</p>
                                <select id="title" value="<%= rs.getString("title")%>" name="title">
                                    <option value="mr">Mr.</option>
                                    <option value="mrs">Mrs.</option>
                                    <option value="miss">Ms.</option>
                                </select>
                            </td>
                          
                            <td style="padding-left:30px;">
                                <p>Name</p>
                                <input type="text" value="<%= rs.getString("name")%>" name="name" id="name" required>
                            </td>
                          
                            <td>
                                <p>Company</p>
                                <input type="text" value="<%= rs.getString("company")%>" name="cname" id="cname" required>
                            </td>
                        </tr>
                      
                        <tr>
                            <td class="td1">
                                <p>Contact Number</p>
                                <input type="text" name="contact" value="<%= rs.getString("phone")%>" id="contact" required>
                                <!--pattern="0[0-9]{9}"-->
                            </td>

                            <td style="padding-left:30px;">
                                <p>Email</p>
                                <input type="email" value="<%= rs.getString("email")%>" name="email" id="email" required>
                            </td>
                            <td></td>
                        </tr>

                        <tr>
                            <td class="td3">
                                <p id="phone" style="font-size:smaller;" class = "error hidden">Please enter a valid number!</p>
                            </td>
                            <td class="td2">
                                <p id="mail" style="font-size:smaller;" class = "error hidden">Please enter a valid email!</p>
                            </td>
                        </tr>
                    </table>
                    
                    <button type="submit" id="btn1" name="id" value="1" onclick="form.action='application/settingsupdate.jsp'">Update</button>    
                </form>
                <%}%>
                <br><br><br><hr><br>

                <!--password matching script-->
                <script type = "text/javascript">  
                    function pwdmatch(){  
                        var pwd1 = document.pwdform.npass.value;  
                        var pwd2 = document.pwdform.rpass.value;  
                      
                        if(pwd1 == pwd2){  
                            // return true;  
                            document.getElementById("match").classList.add('hidden');
                        }  
                        else{  
                            // alert("Passwords must be the same!");  
                            document.getElementById("match").classList.remove('hidden');
                            return false; 
                        }  
                    }  
                </script> 

                <!--validating password-->
                <!-- <script>
                    function validatepwd(){
                        var val1 = "[a-z][A-Z][0-9]{8,15}";
                        var val2 = document.pwdform.npass;
                        if(val2.value.match(val1)){
                            // return true;
                            document.getElementById('match').classList.add('hidden');
                        }
                        else{
                            // alert("Please enter a valid phone number!");
                            document.getElementById("match").classList.remove('hidden');
                            return false;
                        }
                    }
                </script> -->

                <!--show password script-->
                <script>
                    function currentpwd(){
                        var passwordInput = document.getElementById('cpass');
                        var passStatus = document.getElementById('icon1');

                        if(passwordInput.type == 'password'){
                            passwordInput.type = 'text';
                            passStatus.className = 'fa fa-eye-slash';
                        }
                        else{
                            passwordInput.type = 'password';
                            passStatus.className = 'fa fa-eye';
                        }
                    }

                    function newpwd(){
                        var passwordInput = document.getElementById('npass');
                        var passStatus = document.getElementById('icon2');

                        if(passwordInput.type == 'password'){
                            passwordInput.type = 'text';
                            passStatus.className = 'fa fa-eye-slash';
                        }
                        else{
                            passwordInput.type = 'password';
                            passStatus.className = 'fa fa-eye';
                        }
                    }

                    function repwd(){
                        var passwordInput = document.getElementById('rpass');
                        var passStatus = document.getElementById('icon3');

                        if(passwordInput.type == 'password'){
                            passwordInput.type = 'text';
                            passStatus.className = 'fa fa-eye-slash';
                        }
                        else{
                            passwordInput.type = 'password';
                            passStatus.className = 'fa fa-eye';
                        }
                    }
                </script>

                <%
                    String result = null;
                    String result2 = null;
                    result = request.getParameter("result");
                    result2 = request.getParameter("result2");     
                %>

                <!--reset password-->
                <h3>Reset Password</h3>
                <form name="pwdform" onsubmit="return pwdmatch()">
                    <table width="100%">
                        <tr>
                            <td class="td" style="padding-left:40px;">
                                <p>Current Password</p>
                                <input type="password" name="cpass" id="cpass" required>
                                <i id="icon1" class="fa fa-eye" aria-hidden="true" onClick="currentpwd()"></i>
                            </td>
                        
                            <td class="td">
                                <p>New Password</p>
                                <input type="password" name="npass" id="npass" min="8" required>
                                <i id="icon2" class="fa fa-eye" aria-hidden="true" onClick="newpwd()"></i>
                            </td>
                        
                            <td class="td">
                                <p>Re-enter Password</p>
                                <input type="password" name="rpass" id="rpass" min="8" required>
                                <i id="icon3" class="fa fa-eye" onClick="repwd()"></i>
                            </td>
                        </tr>
                    
                        <%
                            if(result!=null){
                        %> 
                        
                        <tr>
                            <td style="padding-left:40px;"><p class="h5" style="font-size:smaller;">Current password incorrect!</p></td>
                            <td></td>
                            <td></td>   
                        </tr>
                        
                        <%
                            }               
                        %>
                    
                        <!-- <%
                            if(result2!=null){
                        %> 
                        
                        <tr>
                            <td></td>
                            <td><p class="h5" style="font-size:smaller;">Passwords must be same!</p></td>
                            <td></td>   
                        </tr>
                        
                        <%
                            }               
                        %> -->
                        <tr>
                            <td></td>
                            <td><p id="match" style="font-size:smaller;" class = "error hidden">Passwords must be same!</p></td>
                            <td></td>
                        </tr>
                    </table>
                    
                    <button type="submit" id="btn1" value="1" name="id" onclick="form.action='application/resetpassword.jsp'">Reset</button>         
                </form>
            </div>    
        </div>
        <!--<td><button type="submit" name="btn" value="orderitems" id="btn1" onclick="document.location='orderdetails.jsp?id=<%%>'">View</button></td>-->
    <%}}%>
    </body>
</html>