<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/additem.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Add Product</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
               <%-- <li><a href="#" id="a1"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" id="a1"><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="#" id="a1">Add Items</a></li>
                            <li id="li"><a href="productlist.jsp">Item List</a></li>
                            <li id="li"><a href="orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <form action="#">
                <h3>Add Product</h3>
                <table>
                      <tr>
                          <td class="td">
                            
                           <p>Product Name</p>
                            <input type="text" placeholder="Enter Item Name" name="name" id="name" required>
                           
                          </td>
                          <td class="td">
                            <p>Product Type</p>
                            <select id="cat" name="cat">
                                    <option value="Iron">Iron</option>
                                    <option value="Concrete Mixture Parts">Concrete Mixer Parts</option>
                                    <option value="Brass">Brass</option>
                                    <%-- <option value="Other">Other</option> --%>
                            </select>
                          
                          </td>
                      </tr>
                      <tr>
                          <td>
                          <p>Image</p>
                            <input type="file" name="path" id="path">
                          
                          </td>
                          <td>
                            <p>Minimum Quantity</p>
                            <input type="number"  name="min" id="min" min="0" pattern="[0-9]+" required>
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                          <p>Unit Price</p>
                          <input type="text" name="uprice" placeholder="Enter Unit Price" pattern="[0-9]+" id="uprice" required>
                          </td>
                          <td><p>Size</p>
                          <input type="text" name="size" placeholder="Enter Size" id="size" required>
                          </td>
                      </tr>
                     <tr>
                      <td class="td">
                      <p>Inventory Code</p>
                      <select id="mid"  name="mid">
                      <%
                        ResultSet rs;
                        PreparedStatement psm = conn.prepareStatement("select * from materials where type=?");
                        psm.setString(1,"OUT");
                        rs = psm.executeQuery();
                        while(rs.next()){
                             %>
                                <option value="<%=rs.getString("materialID")%>"><%=rs.getString("name")%></option>
                             <%
                        }
        
                      %>
                         </select>
                         </td>
                        <td class="td">
                          </td>
                    </tr>
                    <tr>
                        <td class="td" colspan="2">
                        <p>Description</p>
                            <input type="text" name="dsc"  id="dsc">
                        </td>
                    </tr>
                    <tr>
                        <td class="td"></td>
                        <td class="td">
                        <table>
                        <tr>
                            <td class="td"><button type="submit" id="btn1" onclick="form.action='application/itemaddcontrol.jsp'">Add</button></form></td>
                            <td class="td"><form>
                                <button type="submit" id="btn1" name="btn" value="productlist" onclick="form.action='application/salesredirect.jsp'">Item List</button>
                                </form>
                            </td>
                        </tr>
                        </table>
                        </td>
                    </tr>
                </table>
            
            </div>
            
        </div>

    <%}}%>
    </body>
    
  
</html>