<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<!DOCTYPE html>
<html>
<script src="https://smtpjs.com/v3/smtp.js"></script>
<%

String ordr = request.getParameter("btn3");
String ptype = request.getParameter("ptype");
String user;
LocalDate date = LocalDate.now();
String strDate = date.toString();
String email;
String pdate;
String rdate;
try{
    out.println(ptype);
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    PreparedStatement psm = conn.prepareStatement("select * from allorders where orderID = ?");
    psm.setString(1,ordr);
    ResultSet rs = psm.executeQuery();
    while(rs.next()){
        user = rs.getString("userID");
        String amount = rs.getString("amount");
        PreparedStatement ps = conn.prepareStatement("update allorders set status=? where orderID=?");
        ps.setString(1,"Order Sent");
        ps.setString(2,ordr);
        ps.executeUpdate();
        PreparedStatement ps11 = conn.prepareStatement("select * from quotation where orderid = ?");
        ps11.setString(1,ordr);
        ResultSet rs4 = ps11.executeQuery();
        while(rs4.next()){
    
        PreparedStatement ps2 = conn.prepareStatement("update custorder set payment=? where orderID=?");
        ps2.setString(1,"done");
        ps2.setString(2,ordr);
        ps2.executeUpdate();
        PreparedStatement ps3 = conn.prepareStatement("insert into notification(oip,from_user,to_user,type,description,link,status,date) values(?,?,?,?,?,?,?,?)");
        ps3.setString(1,ordr);
        ps3.setString(2,"1");
        ps3.setString(3,user);
        ps3.setString(4,"normal_order");
        ps3.setString(5,"Order Sent");
        ps3.setString(6,"/project/customer/conform-resipt.jsp?id="+ordr);
        ps3.setString(7,"not viewed");
        ps3.setString(8,strDate);
        ps3.executeUpdate();
        PreparedStatement ps4 = conn.prepareStatement("insert into income_expence(flag,description,amount,oip,date) values(?,?,?,?,?)");
        ps4.setString(1,"Income");
        ps4.setString(2,"Customized Order Payment Received");
        ps4.setString(3,rs4.getString("total"));
        ps4.setString(4,ordr);
        ps4.setString(5,strDate);
        ps4.executeUpdate();

        PreparedStatement ps5 = conn.prepareStatement("select * from custnormalorder where orderID = ?");
        ps5.setString(1,ordr);
        ResultSet rs2 = ps5.executeQuery();
        while(rs2.next()){
            int amt = rs2.getInt("quantity");
            PreparedStatement ps6 = conn.prepareStatement("select materials.stock,materials.materialID from products inner join materials on products.materialID = materials.materialID where products.productID = ?");
            ps6.setString(1,rs2.getString("itemID"));
            ResultSet rs3 = ps6.executeQuery();
            while(rs3.next()){
                int old = rs3.getInt("stock");
                int newstock = old - amt;
                PreparedStatement ps7 = conn.prepareStatement("update materials set stock = ? where materialID=?");
                ps7.setString(1,Integer.toString(newstock));
                ps7.setString(2,rs3.getString("materialID"));
                ps7.executeUpdate();
            }
            
        }
    
    
    PreparedStatement ps10 = conn.prepareStatement("delete from materials where materialID =?");
    ps10.setString(1,ptype);
    ps10.executeUpdate();
    PreparedStatement ps8 = conn.prepareStatement("select usertable.email from usertable inner join allorders on usertable.userID = allorders.userID where orderID = ?");
    ps8.setString(1,ordr);
    ResultSet rs5 = ps8.executeQuery();
    while(rs5.next()){
        email  = rs5.getString("email");
        PreparedStatement ps9 = conn.prepareStatement("select placed,req from allorders where orderID = ?");
        ps9.setString(1,ordr);
        ResultSet rs6 = ps9.executeQuery();
        while(rs6.next()){
            pdate  = rs6.getString("placed");
            rdate  = rs6.getString("req");
%>    
    <script>
        function send(){
            var sender = "ems35group@gmail.com";
            var pass = "ems@1234";
            
            Email.send({
            SecureToken : "cd00a9d3-9b20-425d-85c0-495fc2da1c53",
            Host : "smtp.gmail.com",
            Username : sender,
            Password : pass,
            To : '<%=email%>',
            From : sender,
            Subject : "Ready to deliver",
            Body : "Dear Sir/Madam,<br><br>Your order (order ID: <%=ordr%>) placed on <%=pdate%> is ready and will be delivered today. Please check our website and pay the advance amount to proceed. <br>For further information, feel free to contact us on +94778889999 or ems35group@gmail.com . <br><a href=\"http://localhost:8080/project\">Click here</a> to visit our website <br><br>Thank You."
        });
        alert("Email sent");
        }
           send();
           window.location.replace("/project/admin/sales/vieworder.jsp?id=<%=ordr%>");
    </script>
    <%
    }}}}
    //response.sendRedirect("/project/admin/sales/vieworder.jsp?id="+ordr);
}catch(Exception e){
    out.println(e);
}
%>
</html>
