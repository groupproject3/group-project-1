<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<!DOCTYPE html>
<html>
<script src="https://smtpjs.com/v3/smtp.js"></script>
<%

String date = request.getParameter("date");
String orderid = request.getParameter("orderid");
String body = request.getParameter("body");
String subtotal = request.getParameter("subtotal");
String discount = request.getParameter("discount");
String total = request.getParameter("total");
LocalDate sdate = LocalDate.now();
String strDate = sdate.toString();
String email;
String pdate;
String rdate;

try{
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    PreparedStatement ps = conn.prepareStatement("insert into quotation(date, orderid, body, subtotal, discount, total) values(?,?,?,?,?,?)");
    ps.setString(1,date);
    ps.setString(2,orderid);
    ps.setString(3,body);
    ps.setString(4,subtotal);
    ps.setString(5,discount);
    ps.setString(6,total);
    
    int x = ps.executeUpdate();

    PreparedStatement ps2 = conn.prepareStatement("update allorders set status=?, amount=? where orderID = ?");
    ps2.setString(1,"Quotation Sent");
    ps2.setString(2,subtotal);
    ps2.setString(3,orderid);
    ps2.executeUpdate();

    PreparedStatement ps4 = conn.prepareStatement("select * from allorders where orderID=?");
    ps4.setString(1,orderid);
    ResultSet rs2 = ps4.executeQuery();
    while(rs2.next()){
        PreparedStatement ps3 = conn.prepareStatement("insert into notification(oip,from_user,to_user,type,description,link,status,date) values(?,?,?,?,?,?,?,?)");
        ps3.setString(1,orderid);
        ps3.setString(2,"1");
        ps3.setString(3,rs2.getString("userID"));
        ps3.setString(4,"Customized Order");
        ps3.setString(5,"Quotation Sent");
        ps3.setString(6,"/project/customer/custorder-quotation.jsp?btn="+orderid+"&stat=placed");
        ps3.setString(7,"not viewed");
        ps3.setString(8,strDate);
        ps3.executeUpdate();
    }
    PreparedStatement ps5 = conn.prepareStatement("select usertable.email from usertable inner join allorders on usertable.userID = allorders.userID where orderID = ?");
    ps5.setString(1,orderid);
    ResultSet rs = ps5.executeQuery();
    while(rs.next()){
        email  = rs.getString("email");
        PreparedStatement ps6 = conn.prepareStatement("select placed,req from allorders where orderID = ?");
        ps6.setString(1,orderid);
        ResultSet rs3 = ps6.executeQuery();
        while(rs3.next()){
            pdate  = rs3.getString("placed");
            rdate  = rs3.getString("req");
%>    
    <script>
        function send(){
            var sender = "ems35group@gmail.com";
            var pass = "ems@1234";
            
            Email.send({
            SecureToken : "cd00a9d3-9b20-425d-85c0-495fc2da1c53",
            Host : "smtp.gmail.com",
            Username : sender,
            Password : pass,
            To : '<%=email%>',
            From : sender,
            Subject : "Quotation",
            Body : "Dear Sir/Madam,<br><br>The quotation for your order (order ID: <%=orderid%>) placed on <%=pdate%> is sent. Please check our website and pay the advance amount to proceed. <br>For further information, feel free to contact us on +94778889999 or ems35group@gmail.com . <br><a href=\"http://localhost:8080/project\">Click here</a> to visit our website <br><br>Thank You."
        });
        alert("Email sent");
        }
           send();
           window.location.replace("/project/admin/sales/vieworder.jsp?id=<%=orderid%>");
    </script>
    <%
    }}

    //response.sendRedirect("/project/admin/sales/vieworder.jsp?id="+orderid);
    
}catch(Exception e){
    out.println(e);
}
%>
</html>