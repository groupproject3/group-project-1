<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/quotation.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Quotation</title>
    </head>
    <body><%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head"  id="a1"><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="additem.jsp">Add Items</a></li>
                            <li id="li"><a href="productlist.jsp">Item List</a></li>
                            <li id="li"><a href="orderlist.jsp"  id="a1">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <h3>Quotation Form</h3>
                <%
                    String order = request.getParameter("btn");
                %>

                <%
                    LocalDate date = LocalDate.now();
                    String strDate = date.toString();
                %>

                <form action="application/quotationsend.jsp" onsubmit="return validateform()" method="POST" name="quotation">
                    <table class="table">
                        <tr>
                            <td class="td1"><p>Date</p></td>
                            <td><input type="date" id="date" name="date" value="<%=strDate%>" readonly></td>
                            <!-- <td><p id="date1" style="font-size:smaller;" class = "error hidden">Required!</p></td> -->
                            <td class="td1"><p>Order ID</p></td>
                            <td><input type="text" id="orderid" value="<%=order%>" name="orderid"></td>
                        </tr>
                        
                        <!-- <tr>
                            <td class="td1"><p>Quotation ID *</p></td>
                            <td><input type="number" id="id" name="qid" placeholder="Enter Quotation ID"></td>   
                            <td><p id="qid" style="font-size:smaller;" class = "error hidden">Required!</p></td>
                            <td></td>
                        </tr> -->

                        <!-- <tr>
                            <td class="td1"><p>Quotaion</p></td>
                            <td colspan=4><input type="text" name="body" value="0" hidden></td>
                        </tr> -->

                        <tr style="padding: 10px;">
                            <td class="td1">
                                <p>Quatation Body</p>
                            </td>
                            <td colspan=3>
                                <textarea id="body" name="body" rows="9" cols="84"></textarea>
                            </td>
                        </tr>
                            
                        <tr>
                            <td class="td1"><p>Total *</p></td>
                            <td><input type="number" id="total" name="subtotal" placeholder="Enter Total" step="100" onkeyup="sub()" style="text-align: right;" Required></td>  
                            <!-- <td><p id="sub" style="font-size:smaller;"></p></td> -->
                            <td><p id="dis" style="font-size:smaller;" class = "error hidden">Required!</p></td>

                            <td rowspan="3">
                                <p style="font-size:smaller;">**Please deposit the advanced payment to this account.</p>
                                <textarea id="account" name="account" rows="5" cols="39">
                                    Bank & Branch: BOC, Malabe
                                    Account No.: 12345678
                                </textarea>
                            </td>

                        </tr>
                        
                        <!-- <script>
                            var total = 0;
                            $("#subtotal").val(total);
                            
                            $("[id^=num]").change(function(){
                                var preVal = parseFloat($(this).attr("prevalue"));
                                total = total + parseFloat($(this).val()) - preVal;
                                $(this).attr("prevalue",parseFloat($(this).val()));
                                var result = total.toFixed(2);
                                $("#subtotal").val(result);
                            })
                        </script> -->

                        <tr>
                            <td class="td1"><p>Advanced *</p></td>
                            <td><input type="number" id="advanced" name="discount" placeholder="Enter Advanced" step="10" min="0" max="20000" onkeyup="sub()" style="text-align: right;"Required></td>  
                            <td><p id="dis" style="font-size:smaller;" class = "error hidden">Required!</p></td>
                        </tr>
                        
                        <tr>
                            <td class="td1"><p>Balance</p></td>
                            <td><input type="number" id="bal" name="total" value="0.00" step=".01" style="text-align: right;" readonly></td>  
                            <td><p id="tot" style="font-size:smaller;"></p></td>
                        </tr>
                    </table>
 
                    <script type="text/javascript">
                        function sub(){
                            var num1, num2, num3;
                            num1 = parseFloat(document.getElementById("total").value);
                            num2 = parseFloat(document.getElementById("advanced").value);
                            if(num1=="")
                                num1 = 0;
                            if(num2=="")
                                num2 = 0;
                            num3 = num1 - num2;
                           
                            if(!isNaN(num3)){
                                document.getElementById("bal").value = (num3).toFixed(2);
                            }   
                        }
                    </script>
                    
                    <script>
                        function validateform(){
                            var b = document.forms["quotation"]["id"].value;
                            var e = document.forms["quotation"]["discount"].value;

                            if(b == "" && e == ""){
                                document.getElementById("qid").classList.remove('hidden');
                                document.getElementById("dis").classList.remove('hidden');
                                return false;
                                // document.getElementById("qid").innerHTML = "Quotation ID is Compulsory!";
                                // alert("Quotation ID is required!");
                            }
                            
                            else if(b != "" && e == ""){
                                document.getElementById("qid").classList.add('hidden');
                                document.getElementById("dis").classList.remove('hidden');
                                return false;
                            }

                            else if(b == "" && e != ""){
                                document.getElementById("qid").classList.remove('hidden');
                                document.getElementById("dis").classList.add('hidden');
                                return false;
                            }

                            else if(b != "" && e != ""){
                                return true;
                            }
                        }
                        
                        // function validateDate(){
                        //     var a = document.forms["quotation"]["date"].value;
                            
                        //     if(a == ""){
                        //         // alert("Date is required!");
                        //         document.getElementById("date1").classList.remove('hidden');
                        //         return false;
                        //     }
                        //     else{
                        //         document.getElementById('date1').classList.add('hidden');
                        //     }
                        // }

                        // validating only discount
                        // function validateDis(){
                        //     var e = document.forms["quotation"]["discount"].value;
                        //     if(e == ""){
                        //         document.getElementById("dis").classList.remove('hidden');
                        //         return false;
                        //     }
                        //     else{
                        //         document.getElementById('dis').classList.add('hidden');
                        //     }
                        // }

                        // function validatePrice(){
                        //     var e = document.forms["quotation"]["num"].value;
                        //     if(e == ""){
                        //         document.getElementById("numb").classList.remove('hidden');
                        //         return false;
                        //     }
                        //     else{
                        //         document.getElementById('numb').classList.add('hidden');
                        //     }
                        // }
                    </script>

                    <br>
                    <button type="submit" name="user" value="<%=uid%>">Send</button>       
                </form>
                
                <form action="#">
                    <input type="text" name="user" id="user" value="<%=uid%>" hidden>
                    <button type="submit" name="id" value="<%=order%>" onclick="form.action='orderlist.jsp'">Back</button>
                </form>
            </div> 
        </div>
        <%}}%>
    </body>
</html>