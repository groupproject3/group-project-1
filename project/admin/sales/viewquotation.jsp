<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/quotation.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Quotation</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" id="a1"><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="additem.jsp" id="a1">Add Items</a></li>
                            <li id="li"><a href="productlist.jsp">Item List</a></li>
                            <li id="li"><a href="orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                           <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <h3>Quotation</h3>
                <%   
                    ResultSet rs;
                    String ordrid = request.getParameter("btn");
                    //Class.forName("com.mysql.jdbc.Driver");
                    //Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                    PreparedStatement psm = conn.prepareStatement("select * from quotation where orderID = ?");
                    psm.setString(1, ordrid);
                    rs =  psm.executeQuery();
                       
                    while(rs.next()){        
                %>
            
                <form action="application/quotationsend.jsp" method="POST">
                    <table>
                        <tr>
                            <td class="td1"><label for="date">Date</label></td>
                            <td><input type="date" id="date" name="date" value="<%=rs.getString("date")%>" disabled></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td class="td1"><label for="id">Quotation ID</label></td>
                            <td><input type="text" id="id" name="qid" value="<%=rs.getString("qid")%>" disabled></td>   
                            <td class="td1"><label for="orderid">Order ID</label></td>
                            <td><input type="text" id="orderid" value="<%=rs.getString("orderid")%>" disabled name="orderid"></td>
                        </tr>
                        
                        <tr>
                            <td class="td1"><label for="body">Quotation Body</label></td>
                            <td colspan=3><textarea name="body" rows="9" cols="84"  disabled><%=rs.getString("body")%></textarea></td>
                        </tr>

                        <tr>
                            <td class="td1"><label for="subtotal">Sub Total</label></td>
                            <td><input type="text" id="subtotal" name="subtotal" value="<%=rs.getString("subtotal")%>" disabled style="text-align: right;"></td>  
                        </tr>

                        <tr>
                            <td class="td1"><label for="discount">Advanced</label></td>
                            <td><input type="text" id="discount" name="discount" value="<%=rs.getString("discount")%>" disabled style="text-align: right;"></td>  
                        </tr>

                        <tr>
                            <td class="td1"><label for="total">Total</label></td>
                            <td><input type="text" id="total" name="total" value="<%=rs.getString("total")%>" disabled style="text-align: right;"></td>  
                        </tr>
                    </table>
                    
                    <form action="#">
                        <%
                            PreparedStatement psm1 = conn.prepareStatement("select allorders.orderID,allorders.amount,allorders.status,allorders.placed,allorders.req,custorder.flag,usertable.name,usertable.userID from ((allorders inner join custorder on allorders.orderID=custorder.OrderID) inner join usertable on custorder.customerID=usertable.userID) where allorders.orderID=?");
                            psm1.setString(1, ordrid);
                            ResultSet rs1 = psm1.executeQuery();
                            while(rs1.next()){
                                user = rs1.getString("userID");
                                String tp1 = "Advance Paid";
                                String status = rs1.getString("status");
                                if(tp1.compareTo(status)==0){
                            
                                PreparedStatement psm2 = conn.prepareStatement("select * from customizedorder where orderID=?");
                                psm2.setString(1, ordrid);
                                ResultSet rs2 =  psm2.executeQuery();
                                while(rs2.next()){
                                    String path = rs2.getString("slip");
                            %>
                            <a href="#" onclick="display1('<%=path%>',<%=ordrid%>,<%=user%>);">Payment Slip</a>
                            <%}}
                                String tp2 = "Order Accepted";
                                if(tp2.compareTo(status)==0){
                                    PreparedStatement psm2 = conn.prepareStatement("select * from customizedorder where orderID=?");
                                    psm2.setString(1, ordrid);
                                    ResultSet rs2 =  psm2.executeQuery();
                                    while(rs2.next()){
                                        String path = rs2.getString("slip");
                            %>
                                <a href="#" onclick="display2('<%=path%>');">Payment Slip</a>
                          <%}}}%>           
                        
                        <button type="submit" id="btn1"  name="id" value="<%=ordrid%>" onclick="form.action='vieworder.jsp'">Back</button>
                    </form> 
                            
                <%}%>
                </form>
            </div> 
        </div>

        <div id="popup1" class="popup">
            <!-- Modal content -->
            <div class="popup-content">
                <span class="close" onclick="hide1();">&times;</span>
                <h3>Payment Slip</h3>
            </br></br>
            <img src="" class="slip" id="mid2">
                <table style="float:right;"> 
                    <tr>
                        <td>
                            <form>
                                <input type="text" value="" id="popuser" name="userID" hidden>
                                <button type="submit" id="btn3" class="btn3" name="btn3"  onclick="form.action='application/advancepaid.jsp'">Accept</button>
                            </form>
                        </td>
                        <td><button type="submit" id="btn4" class="btn4" onclick="hide1();">Back</button></td>
                    </tr>
                </table>
            </div>  
        </div>

        <!--popup after order accepted-->
        <div id="popup2" class="popup">
            <!-- Modal content -->
            <div class="popup-content" style="width:35%;">
                <span class="close" onclick="hide2();">&times;</span>
                <h3>Payment Slip</h3>
            </br></br>
            <img src="" class="slip" id="mid3" style="margin-left:37px;">
                <!-- <table style="float:right;"> 
                    <tr>
                        <td>
                            <form>
                                <input type="text" value="" id="popuser" name="userID" hidden>
                                <button type="submit" id="btn3" class="btn3" name="btn3"  onclick="form.action='application/advancepaid.jsp'">Accept</button>
                            </form>
                        </td>
                        <td><button type="submit" id="btn4" class="btn4" onclick="hide2();">Back</button></td>
                    </tr>
                </table> -->
            </div>  
        </div>

        <script>
            // Get the modal
            var pop1 = document.getElementById("popup1");
            var pop2 = document.getElementById("popup2");
            //var materilal = document.getElementById("mid").value;
            // Get the button that opens the modal
            var btn1 = document.getElementById("btn1");
    
            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];       
    
            // When the user clicks the button, open the modal 
            function display1(img, id, uid){
            document.getElementById('btn3').value = id;
            document.getElementById('popuser').value = uid;
            document.getElementById('mid2').src = "/project/customer/"+img;
            pop1.style.display = "block";
            }
            function hide1(){
            pop1.style.display = "none";
            }

            function display2(img){
            document.getElementById('mid3').src = "/project/customer/"+img;
            pop2.style.display = "block";
            }
            function hide2(){
            pop2.style.display = "none";
            }
            
            // When the user clicks on <span> (x), close the modal
    
            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event){
                if (event.target == pop1) {
                    pop1.style.display = "none";
                }
                if (event.target == pop2) {
                    pop2.style.display = "none";
                }
            }
        </script>
        <%}}%>
    </body>
</html>