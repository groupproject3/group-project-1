<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/vieworder.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>View Order</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" id="a1"><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="additem.jsp" id="a1">Add Items</a></li>
                            <li id="li"><a href="productlist.jsp">Item List</a></li>
                            <li id="li"><a href="orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                
                <h3>View Order</h3>
                <table>
                 <%
                        ResultSet rs,rs2;
                        String id = request.getParameter("id");
                        //Class.forName("com.mysql.jdbc.Driver");
                        //Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                        PreparedStatement psm = conn.prepareStatement("select allorders.orderID,allorders.amount,allorders.status,allorders.placed,allorders.req,custorder.flag,usertable.name,usertable.userID from ((allorders inner join custorder on allorders.orderID=custorder.OrderID) inner join usertable on custorder.customerID=usertable.userID) where allorders.orderID=?");
                        psm.setString(1, id);
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            
                    %>
                
                      <tr>
                          <td class="td">
                            
                          <p>Order Id</p>
                          <input type="text" value="<%= rs.getString("orderID")%>" name="orderID" id="orderID" readonly>
                          
                          </td>
                          
                          <td class="td">
                          <p>Customer Name</p>
                            <input type="text" value="<%= rs.getString("name")%>" name="name" id="name" readonly>
                            
                          </td>
                       
                      </tr>
                      <tr>
                          <td>
                            <p>Placed Date</p>
                            <input type="text" value="<%= rs.getString("placed")%>" name="placed" id="placed" readonly>
                          </td>
                          <td>
                            <p>Required Date</p>
                            <input type="text" value="<%= rs.getString("req")%>" name="req" id="req" readonly>
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                          <p>Total Price</p>
                          <input type="text" name="price" value="<%= rs.getString("amount")%>" id="price" readonly>
                          </td>
                          <td><p>Status</p>
                          <input type="text" name="status" value="<%= rs.getString("status")%>" id="status" readonly>
                          </td>
                      </tr>
                      <tr>
                        <td class="td">
                            <%
                            String tp1 = "normal";
                            String flag = rs.getString("flag");
                            if(tp1.compareTo(flag)==0){
                            %>
                            <a href="orderitems.jsp?id=<%= rs.getString("orderID")%>">Ordered Items</a></td>
                            <%}else{
                                PreparedStatement psm1 = conn.prepareStatement("select * from customizedorder where orderID=?");
                                psm1.setString(1, id);
                                ResultSet rs3 =  psm1.executeQuery();
                                while(rs3.next()){
                                    String path = rs3.getString("image");
                                    String quantity = rs3.getString("quantity");
                            %>
                            <a href="#" onclick="display2('<%=path%>','<%=quantity%>');">Sample Image</a>
                        </td>
                        <td class="td"></td>
                      </tr>
                    <%}}%>
                    <tr>
                        <td class="td">
                        <table>
                            <tr>
                                <td class="td">
                                <%
                                String ordr =  rs.getString("orderID");
                                String status = rs.getString("status");
                                String type = rs.getString("flag");
                                String s = "placed";
                                String tp = "normal";
                                String s2 = "Order Accepted";
                                if(status.compareTo(s2)==0){
                                    if(type.compareTo(tp)==0){
                                %>
                                    <form action="#">
                                        <button type="submit" id="btn1" name="btn3" value="<%= rs.getString("orderID")%>" onclick="form.action='application/order_sent.jsp'">Mark as sent</button>
                                    </form>
                                        
                                    
                                    <%}else{%>
                                        <button id="btn1" onclick="display1(<%=ordr%>);">Mark as Sent</button>
                                    <%}}%>
                                </td>
                                <td class="td">
                                    <%
                                    if(status.compareTo(s)!=0){
                                        if(type.compareTo(tp)==0){%>
                                        <form action="#">
                                        <button type="submit" id="btn1" name="btn" value="<%= rs.getString("orderID")%>" onclick="form.action='order_resipt.jsp'">Receipt</button>
                                    </form>
                                    <%}else{%>
                                    <form action="#">
                                        <button type="submit" id="btn1" name="btn" value="<%= rs.getString("orderID")%>" onclick="form.action='viewquotation.jsp'">Quotation</button>
                                    </form>
                                    <%}}else{%>
                                    <%if(type.compareTo(tp)==0){%>
                                        <form action="#">
                                        <button type="submit" id="btn1" name="btn" value="<%= rs.getString("orderID")%>" onclick="form.action='order_resipt.jsp'">Receipt</button>
                                    </form>
                                    <%}else{%>
                                    
                                    <form action="#">
                                        <button type="submit" id="btn1" name="btn" value="<%= rs.getString("orderID")%>" onclick="form.action='fillquotation.jsp                              '">Quotation</button>
                                    </form>
                                    <%}}%>
                                </td>
                            </tr>
                        </table>
                        </td>
                        <td class="td">
                        <table>
                            <tr>
                                <td class="td">
                                <%
                                    if(status.compareTo("Order Accepted")!=0 && status.compareTo("Order Sent")!=0 ){
                                %>
                                    <form action="#">
                                        <button type="submit" id="btn1" name="btn" value="<%= rs.getString("orderID")%>" onclick="form.action='application/deleteorder.jsp'">Delete</button>
                                    </form>
                                    <%}}%>
                                </td>
                                <td class="td">
                                    <form action="#">
                                        <button type="submit" id="btn1" name="btn" value="orderlist" onclick="form.action='application/salesredirect.jsp'">Back</button>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
  
            </div>
            
        </div>
    
        <div id="popup1" class="popup">
            <!-- Modal content -->
            <div class="popup-content">
                <span class="close" onclick="hide1();">&times;</span>
                <h3>Select Product</h3>
                <form>
                <select id="ptype" name="ptype" class="select">
                <%
                ResultSet rs3;
                PreparedStatement psm2 = conn.prepareStatement("select * from materials where type = ?");
                psm2.setString(1,"OUT");
                rs3 =  psm2.executeQuery();
                while(rs3.next()){
                %>
                        <option value="<%=rs3.getString("materialID")%>"><%=rs3.getString("name")%></option>
                <%}%>
                </select>
                <table style="float:right;">
                    <tr>
                        <td>
                            
                                <button type="submit" id="btn3" class="btn3" name="btn3"  onclick="form.action='application/custordersent.jsp'">Yes</button>
                            </form>
                        </td>
                        <td><button type="submit" id="btn4" class="btn5" onclick="hide1();">No</button></td>
                    </tr>
                </table>     
            </div>  
        </div>

        <!--popup for drawing-->
        <div id="popup2" class="popup">
            <div class="popup-content">
                <span class="close" onclick="hide2();">&times;</span>
                <h3>Drawing</h3>
                <img src="" class="drawing" id="drawing">
                <h3>Quantity: <label id="qty"></label></h3>
                
                <table style="float:right;">
                    <tr>
                        <td>
                        </td>
                        <td><button type="submit" id="btn4" class="btn4" onclick="hide2();">Ok</button></td>
                    </tr>
                </table>  
            </div>  
        </div>
               
                
    <script>
        // Get the modal
        var pop1 = document.getElementById("popup1");
        var pop2 = document.getElementById("popup2");
        //var materilal = document.getElementById("mid").value;
        // Get the button that opens the modal
        var btn1 = document.getElementById("btn1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];       

        // When the user clicks the button, open the modal 
        function display1(x){
        document.getElementById('btn3').value = x;
        pop1.style.display = "block";
        }
        function hide1(){
        pop1.style.display = "none";
        }

        //popup for drawing
        function display2(img,quantity){
        //document.getElementById('btn3').value = x;
        document.getElementById('drawing').src = "/project/customer/"+img;
        document.getElementById('qty').innerHTML = quantity;
        pop2.style.display = "block";
        }
        function hide2(){
        pop2.style.display = "none";
        }
        
        // When the user clicks on <span> (x), close the modal


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == pop1) {
            pop1.style.display = "none";
        }

        if (event.target == pop2) {
            pop2.style.display = "none";
        }
        }
    </script>
    <%}}%>
    </body>
</html>