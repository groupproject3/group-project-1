<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/resipt.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>View Order</title>
    </head>
    <body>
    <%String uid = "1";
        String ordr = request.getParameter("btn");
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#" id="a1"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" id="a1"><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="additem.jsp" id="a1">Add Items</a></li>
                            <li id="li"><a href="productlist.jsp">Item List</a></li>
                            <li id="li"><a href="orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" ><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../report/generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="../report/bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="../report/bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="report/productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box" style="height: auto;">
            <%
                ResultSet rs;
                PreparedStatement psm = conn.prepareStatement("select usertable.userID,usertable.name,usertable.email,allorders.req,allorders.amount,allorders.status from usertable inner join allorders on usertable.userID = allorders.userID where orderID = ?");
                psm.setString(1, ordr);
                rs =  psm.executeQuery();
                while(rs.next()){
            %>
                
                <table >
                      <tr>
                          <td><h1>Order receipt</h1></td>
                      </tr>
                      <tr>
                          <td class="td">
                            <p>Order ID</p>
                          </td>
                          <td class="td1 ">
                            <label><%=ordr%></label>
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                            <p>Customer Name</p>
                          </td>
                          <td class="td1">
                            <label><%= rs.getString("name")%></label>
                          </td>
                      </tr>
                      <tr>
                        <td class="td">
                          <p>Email Address</p>
                        </td>
                        <td class="td1">
                          <label><%= rs.getString("email")%></label>
                        </td>
                    </tr>
                   
                    <tr>
                        <td class="td">
                          <p>Items with Quantities</p>
                        </td>
                        <td class="td1">
                        
                            <select id="mtype" name="mtype" style="width: 325px;">
                            <%
                                ResultSet rs2;
                                PreparedStatement psm2 = conn.prepareStatement("select products.name,custnormalorder.quantity from products inner join custnormalorder on custnormalorder.itemID = products.productID where custnormalorder.orderID = ?");
                                psm2.setString(1, ordr);
                                rs2 =  psm2.executeQuery();
                                while(rs2.next()){
                            %>
                                <option><%= rs2.getString("name")%> &nbsp&nbsp <%= rs2.getString("quantity")%></option>
                                <%}%>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="td">
                          <p>Total Price</p>
                        </td>
                        <td class="td1">
                          <label>LKR.&nbsp<%= rs.getString("amount")%></label>
                        </td>
                    </tr>
                    <tr>
                        <td class="td">
                          <p>Required Date</p>
                        </td>
                        <td class="td1">
                          <label><input style="width: 200px;" type="date" value="<%= rs.getString("req")%>" readonly></label>
                        </td>
                    </tr>
                    <!-- <tr>
                      <td class="td">
                        <p>Required Date</p>
                      </td>
                      <td class="td1">
                        <label>00001</label>
                      </td>
                  </tr> -->
                    <tr>
                        <td class="td">
                          <p>Order Status</p>
                        </td>
                        <td class="td1">
                          <label><%= rs.getString("status")%></label>
                        </td>
                    </tr>
                      
                    <tr>
                        <td>
                             <table>
                                <tr>
                                    <td class="td"></td>
                                    <td class="td">
                                    <%
                                        String status = rs.getString("status");
                                        String s1 = "Order Accepted";
                                        String s2 = "Order Rejected";
                                        if(s1.compareTo(status)!=0 && s2.compareTo(status)!=0){
                                    %><form>
                                    <input type="text" value="<%= rs.getString("userID")%>" name="user" id="user" hidden>
                                    <button type="submit" id="btn1" name="id" value="<%=ordr%>" onclick="form.action='application/accept.jsp'">Accept</button></form> </td>
                                    <% 
                                        }
                                    %>
                                    
                                </tr>
                            </table>
                        
                        </td>
                        <td class="td1">
                            <!-- <a href="place-order.jsp?user=<%=uid%>">Submit</a> -->
                             <!-- <button type="submit" id="btn1" name="user" value="<%=uid%>" onclick="form.action='place-order.jsp'">Submit</button>  -->
                            <!-- <a href="cartnew.jsp?user=<%=uid%>">Cancel Order</a> -->
                            <table>
                                <tr>
                                    <td class="td">
                                    <%
                                        if(s1.compareTo(status)!=0 && s2.compareTo(status)!=0){
                                    %><form>
                                    <input type="text" value="<%= rs.getString("userID")%>" name="user" id="user" hidden>
                                    <button type="submit" id="btn1" name="id" value="<%=ordr%>" onclick="form.action='application/reject.jsp'">Reject</button></form> </td>
                                    <% 
                                        }
                                    %>
                                    
                                    <td class="td"><form><button type="submit" id="btn1" name="id" value="<%=ordr%>" onclick="form.action='vieworder.jsp'">Back</button></form> </td>
                                </tr>
                            </table>
                              
                        </td>
                    </tr>
                </table>
                
            </div>  
        </div>
    <%}}}%>
    
    </body>
    
  
</html>