<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.lang.Math"%>
<%@ page import = "java.time.*"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Best Selling Products</title>
        <link rel="stylesheet" type="text/css" href="css/bestselling.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    </head>

    <body>
        <%
            String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
                ResultSet rs5;
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
                psm5.setString(1,uid);
                rs5 = psm5.executeQuery();
                if(!rs5.next()){
                    response.sendRedirect("/project/index.jsp");
                }*/
        %>

        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../../logout.jsp?user=1" class="logout">Logout</a>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" ><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#" id="a1"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" id="a1"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="#" id="a1">Sales of Products</a></li>
                            <li id="li"><a href="productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>      
        </div>

        <div class="content" style="padding: 90px 80px; top: 57%; left: 50%;">
            <h3>Best Selling Products</h3> 

            <!--Date validation-->
            <%          
                LocalDate date = LocalDate.now();
                /*date = date.plusDays(1);*/
                String strDate = date.toString();
            %>
            
            <!--Form to filter by date-->
            <form>
                <table class="table" align="center">
                    <tr>
                        <td class="td2">
                            <label for="from">From</label>
                        </td>
                        <td class="td2">
                            <input id="fromDate" type="date" name="from" value="" max="<%=strDate%>" required>
                        </td>
                        <td class="td1">
                            <label for="to">To</label>
                        </td>
                        <td class="td2">
                            <input id="toDate" type="date" name="to" value="" max="<%=strDate%>" required>
                        </td>
                        <td class="td1">
                            <button type="submit" name="user" onclick="form.action='bestselling.jsp'">Filter</button>
                        </td>
                    </tr>
                </table>
            </form>

            <script>
                $("#fromDate").change(function(){
                    var selectedDate = $(this).val();
                    $("#toDate").attr("min", selectedDate);
                })
            </script>
        
            <%
                ResultSet rs6, rs7;
                String from = request.getParameter("from");
                String to = request.getParameter("to");
    
                String labels = "";
                String data = "";
                double sum=0.0;
                double percent=0.0;
    
                if(from!=null){
                    PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID inner join custorder cu on cu.orderID = c.orderID where cu.required_date between ? and ? group by p.name order by sum(c.quantity) desc limit 5");
                    psm6.setString(1,from);
                    psm6.setString(2,to);
                    rs6 = psm6.executeQuery();
    
                    while(rs6.next()){
                        sum = sum + Integer.parseInt(rs6.getString("sum(c.quantity)"));
                    }
                    rs7 = psm6.executeQuery();
                }
    
                else{
                    PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID group by p.name order by sum(c.quantity) desc limit 5");
                    rs6 = psm6.executeQuery();
    
                    while(rs6.next()){
                        sum = sum + Integer.parseInt(rs6.getString("sum(c.quantity)"));
                    }
                    rs7 = psm6.executeQuery();
                }
    
                while(rs7.next()){
                    percent = (double)Math.round(((Integer.parseInt(rs7.getString("sum(c.quantity)")) * 100) / sum) * 100) / 100;
                    labels = labels+","+rs7.getString("p.name").toString()+": "+String.valueOf(percent)+"%";
                    data = data+","+rs7.getString("sum(c.quantity)");
                }
            %>
        
            <hr>
            <%
                if(from!=null){
            %>
            <h4 align="center">From <%out.println(from);%>To <%out.println(to);%></h4>

            <%
                }
                else{
            %>
            <h4 align="center">Overall</h4>
            <%
                }
            %>
            <hr>

            <!--Pie Chart-->
            <div class="centerchart" style="width:600px; height:600px;">
                <canvas id="myChart"></canvas>
            </div>
                
            <script>
                var temp = '<%=labels%>';
                var labelArr = temp.split(',').slice(1,6);
                console.log(labelArr);
                var temp1 = '<%=data%>';
                var dataArr = temp1.split(',').slice(1,6);
                console.log(dataArr);
                console.log('<%=sum%>');
    
                new Chart(document.getElementById("myChart"), {
                    type: 'pie',
                    data: {
                    labels: labelArr,
                    datasets: [{
                        label: "Number of Products",
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                        data: dataArr
                    }]
                    },
                    options: {
                    // title: {
                    //     display: true,
                    //     text: 'Best Selling Products'
                    // },
                    legend:{
                        display:true,
                        position:'right',
                        labels:{
                            fontColor:'#000'
                        }
                    },
                    layout:{
                        padding:{
                            left:50,
                            right:0,
                            bottom:0,
                            top:0
                        }
                    }
                    }
                });
            </script>
        </div>
        <%
            }}
        %>
    </body>
</html>