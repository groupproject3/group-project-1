<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.lang.Math"%>
<%@ page import = "java.time.*"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Best Selling Products</title>
        <link rel="stylesheet" type="text/css" href="css/bestselling.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    </head>

    <body>
        <%
            String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
                ResultSet rs1;
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                /*PreparedStatement psm1 = conn.prepareStatement("select * from logged where userID=?");
                psm1.setString(1,uid);
                rs1 = psm1.executeQuery();
                if(!rs1.next()){
                    response.sendRedirect("/project/index.jsp");
                }*/
        %>

        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../../logout.jsp?user=1" class="logout">Logout</a>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" ><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#" id="a1"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" id="a1"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="#" id="a1">Sales of Products</a></li>
                            <li id="li"><a href="productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>      
        </div>

        <div class="content" style="padding: 90px 80px; top: 57%; left: 50%;">
            <h3>Sales of Products</h3> 

            <!--Date validation-->
            <%          
                LocalDate date = LocalDate.now();
                /*date = date.plusDays(1);*/
                String strDate = date.toString();
            %>
            
            <!--Form to filter by date and category-->
            <form>
                <table class="table" align="center">
                    <tr>
                        <td class="td2">
                            <label for="from">From * </label>
                            <input id="fromDate" type="date" name="from" value="" max="<%=strDate%>" required>
                        </td>
                        <td class="td2">
                            <label for="to">To * </label>
                            <input id="toDate" type="date" name="to" value="" max="<%=strDate%>" required>
                        </td>
                        <td class="td2">
                            <label for="cat">Category</label>
                            <select id="cat" name="cat">
                                <option value="" selected="selected">Select Category</option>
                                <%
                                    PreparedStatement psm2 = conn.prepareStatement("select distinct category from products");
                                    ResultSet rs2 = psm2.executeQuery();
                                    while(rs2.next()){
                                %>

                                <option value="<%=rs2.getString("category")%>"><%=rs2.getString("category")%></option>
                                
                                <%}%> 
                            </select>
                        </td>
                        <td class="td2">
                            <button type="submit" name="user" onclick="form.action='bestselling.jsp'">Filter</button>
                        </td>
                    </tr>
                </table>
            </form>

            <script>
                $("#fromDate").change(function(){
                    var selectedDate = $(this).val();
                    $("#toDate").attr("min", selectedDate);
                })
            </script>
        
            <%
                ResultSet rs3, rs4;
                String from = request.getParameter("from");
                String to = request.getParameter("to");
                String cat = request.getParameter("cat");
    
                String labels = "";
                String data = "";
                double sum = 0.0;
                double percent = 0.0;
    
                if(from!=null){
                    if(cat==""){
                        PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID inner join custorder cu on cu.orderID = c.orderID where cu.required_date between ? and ? group by p.name order by sum(c.quantity) desc");
                        psm6.setString(1,from);
                        psm6.setString(2,to);
                        rs3 = psm6.executeQuery();
        
                        while(rs3.next()){
                            sum = sum + Integer.parseInt(rs3.getString("sum(c.quantity)"));
                        }
                        rs4 = psm6.executeQuery();
                    }

                    else{
                        PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID inner join custorder cu on cu.orderID = c.orderID where p.category = ? and cu.required_date between ? and ? group by p.name order by sum(c.quantity) desc");
                        psm6.setString(1,cat);
                        psm6.setString(2,from);
                        psm6.setString(3,to);
                        rs3 = psm6.executeQuery();
    
                        while(rs3.next()){
                            sum = sum + Integer.parseInt(rs3.getString("sum(c.quantity)"));
                        }
                        rs4 = psm6.executeQuery();
                    }
                }
    
                else{
                    PreparedStatement psm6 = conn.prepareStatement("select sum(c.quantity), p.name from custnormalorder c inner join products p on c.itemID = p.productID group by p.name order by sum(c.quantity) desc");
                    rs3 = psm6.executeQuery();
    
                    while(rs3.next()){
                        sum = sum + Integer.parseInt(rs3.getString("sum(c.quantity)"));
                    }
                    rs4 = psm6.executeQuery();
                }
    
                while(rs4.next()){
                    percent = (double)Math.round(((Integer.parseInt(rs4.getString("sum(c.quantity)")) * 100) / sum) * 100) / 100;
                    labels = labels+","+rs4.getString("p.name").toString()+": "+String.valueOf(percent)+"%";
                    data = data+","+rs4.getString("sum(c.quantity)");
                }
            %>
        
            <hr>
            <%
                if(from!=null){
                    if(cat==""){
            %>
            <h4 align="center">From <%out.println(from);%> To <%out.println(to);%></h4>

            <%
                    }
                    else{
            %>
            <h4 align="center">From <%out.print(from);%> To <%out.print(to);%> and <%out.print(cat);%></h4>

            <%
                    }
                }
                else{
            %>
            <h4 align="center">Overall</h4>
            <%
                }
            %>
            <hr><br>

            <!--Redar Chart-->
            <div class="centerchart" style="width:700px; height:700px;">
                <canvas id="myChart"></canvas>
                <br><br>
                <h4>Top 5 Best Selling Products</h4>
                <ol>
                    <li id="0"></li>
                    <li id="1"></li>
                    <li id="2"></li>
                    <li id="3"></li>
                    <li id="4"></li>
                </ol>
                <br>
            </div>
                
            <script>
                var temp = '<%=labels%>';
                var labelArr = temp.split(',').slice(1,);
                console.log(labelArr);
                var temp1 = '<%=data%>';
                var dataArr = temp1.split(',').slice(1,);
                console.log(dataArr);
                console.log('<%=sum%>');
    
                new Chart(document.getElementById("myChart"), {
                    type: 'radar',
                    data:{
                    labels: labelArr,
                    datasets:[{
                        label: "Sales of Products",
                        backgroundColor: "rgba(200,0,0,0.2)",
                        borderColor: "rgba(200,0,0,0.6)",
                        fill: true,
                        data: dataArr
                    }]
                    },
                    options:{
                    // title: {
                    //     display: true,
                    //     text: 'Best Selling Products'
                    // },
                        legend:{
                            display:false,
                            position:'top',
                            labels:{
                                fontColor:'#000'
                            }
                        },
                        // layout:{
                        //     padding:{
                        //         left:0,
                        //         right:0,
                        //         bottom:0,
                        //         top:0
                        //     }
                        // },
                        scale:{
                            ticks:{
                                suggestedMin: 0,
                                stepSize: 10
                                // beginAtZero: true,
                                // min: 0,
                                // max: 100,
                            },
                            pointLabels:{
                                fontSize: 13
                            }
                        }
                    }
                });

                // Top 5
                var temp2 = '<%=labels%>';
                var labelArr1 = temp2.split(',').slice(1,6);
                console.log(labelArr1);
                var temp3 = '<%=data%>';
                var dataArr1 = temp3.split(',').slice(1,6);
                console.log(dataArr1);

                var i;
                for(i=0;i<5;i++){
                    document.getElementById(i).innerHTML = "None";
                } 

                if(labelArr1.length>5){
                    for(i=0;i<5;i++){
                    document.getElementById(i).innerHTML = labelArr1[i];
                    }  
                }
                else{
                    for(i=0;i<labelArr1.length;i++){
                        document.getElementById(i).innerHTML = labelArr1[i];
                    }  
                }
            </script>
        </div>
        <%
            }}
        %>
    </body>
</html>