<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/productinfo.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <head>
        <title>Product Stocks</title>
    </head>
    <body>
    <%String uid = "1";

        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        String tp = request.getParameter("mtype");
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
                    <tr>
                        <td>
                        <%
                        ResultSet rs6;
                        PreparedStatement psm6 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                        psm6.setString(1,uid);
                        psm6.setString(2,"not viewed");
                        rs6 = psm6.executeQuery();
                        if(rs6.next()){
                        %><a href="../notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                        }else{%>
                        <a href="../notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                        <%}%>
                        <td><a href="../../logout.jsp?user=1" class="logout">Logout</a></td>
                    </tr>
                </table>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" ><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#" id="a1"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" id="a1"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="generatereport.jsp">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="#"  id="a1">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a  href=../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
            <h4 style="font-size:20px; padding:0; margin-bottom: 10px;">Product Stocks</h4>
            <table>
                <tr>
                    <td class="td">
                        <%-- <form action="#">
                            <table style="float : right;">
                                
                                <tr>
                                    <td class="td">
                                        <select id="mtype" name="mtype">
                                            <option value="all">All</option>
                                            <option value="req">Shortage</option>
                                            <option value="other">Available</option>
                                        </select>
                                    </td>
                                    <td class="td"><button type="submit" id="btn1" name="btn" onclick="form.action='productinfo.jsp'">Filter</button></td>
                                </tr>
                            </table>
                        </form> --%>
                    </td>
                    <td class="td">
                        <table style="float : right;">
                            
                            <tr>
                                <td><h5 class="h5">Ordered Quantity</h5></td>
                                <td><div class="div1"></div></td>
                            </tr>
                            <tr>
                                <td><h5 class="h5">In Stocks</h5></td>
                                <td><div class="div2"></div></td>
                            </tr>
                            <tr>
                                <td><h5 class="h5">In Production</h5></td>
                                <td><div class="div3"></div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
                <%
                    ResultSet rs;
                    int ordr_qty;
                    int production_qty;
                    int npqty = 0;
                    String id = request.getParameter("id");
                    PreparedStatement psm = conn.prepareStatement("select * from materials where type=?");
                    psm.setString(1,"OUT");
                    rs =  psm.executeQuery();
                    while(rs.next()){
                            npqty = production_qty = ordr_qty = 0;
                            PreparedStatement ps2 = conn.prepareStatement("select custnormalorder.quantity from custnormalorder inner join products on custnormalorder.itemID = products.productID INNER join materials on materials.materialID = products.materialID where materials.materialID = ?");
                            ps2.setString(1,rs.getString("materialID"));
                            int stock = rs.getInt("stock");
                            ResultSet rs2 = ps2.executeQuery();
                            while(rs2.next()){
                                ordr_qty = rs2.getInt("quantity");
                            }
                            PreparedStatement ps3 = conn.prepareStatement("select * from production where outcome = ?");
                            ps3.setString(1,rs.getString("materialID"));
                            ResultSet rs3 = ps3.executeQuery();
                            while(rs3.next()){
                                production_qty = rs3.getInt("out_amt");
                                npqty = production_qty/3;
                            }
                            int x = stock + production_qty;
                           
                %>
                <table class="table1">
                <tr>
                <td class="td">
                <table>
                
                <%
                if(tp.compareTo("req")==0){
                    if(ordr_qty>x){
                        if(ordr_qty/3>=100){
                    %>
                    <tr>
                        <td class="td"><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td class="td"><span class="bar"><span class="material12a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=90){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=80){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=70){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=60){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=50){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=40){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=30){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=20){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>10){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3==0){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11a"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10a"></span></span></td>
                    </tr>
                    <%
                }
                    %>
                    <%
                if(stock/3>=100){
                    %>
                    
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12"></span></span></td>
                    </tr>
                    
                    <%
                }else if(stock/3>=90){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=80){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=70){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=60){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=50){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=40){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=30){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=20){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>10){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3==0){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10"></span></span></td>
                    </tr>
                    <%
                }
                    %>
                    <%
                if(npqty>=100){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=90){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=80){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=70){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=60){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=50){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=40){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=30){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=20){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>10){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty==0){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11b"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10b"></span></span></td>
                    </tr>
                    <%
                }
                  
                    %>
                    </table>
                    </td>
                    <td class="td">
                    
                    <%
                      
                        if(ordr_qty >x){
                            %>
                            
                            <img src="../../img/warning.png" class="del-img">
                            <%
                        }
                    %>
                        
                    </td>
                    </tr>
                    </table>
                <%}%>
                  <%  
                }else if(tp.compareTo("other")==0){
                    if(ordr_qty<x){
                        if(ordr_qty/3>=100){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=90){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=80){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=70){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=60){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=50){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=40){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=30){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=20){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>10){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3==0){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11a"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10a"></span></span></td>
                    </tr>
                    <%
                }
                    %>
                    <%
                if(stock/3>=100){
                    %>
                    
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12"></span></span></td>
                    </tr>
                    
                    <%
                }else if(stock/3>=90){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=80){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=70){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=60){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=50){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=40){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=30){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=20){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>10){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3==0){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10"></span></span></td>
                    </tr>
                    <%
                }
                    %>
                    <%
                if(npqty>=100){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=90){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=80){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=70){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=60){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=50){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=40){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=30){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=20){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>10){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty==0){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11b"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10b"></span></span></td>
                    </tr>
                    <%
                }
    
                    %>
                    </table>
                    </td>
                    <td class="td">
                    
                    <%
                      
                        if(ordr_qty >x){
                            %>
                            
                            <img src="../../img/warning.png" class="del-img">
                            <%
                        }
                    %>
                        
                    </td>
                    </tr>
                    </table>
                <%}%><%
                }else{
                if(ordr_qty/3>=100){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=90){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=80){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=70){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=60){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=50){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=40){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=30){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>=20){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3>10){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9a"></span></span></td>
                    </tr>
                    <%
                }else if(ordr_qty/3==0){
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11a"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td><%= rs.getString("name")%></td>
                        <td rowspan="2" class="td"><%=ordr_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10a"></span></span></td>
                    </tr>
                    <%
                }
                    %>
                    <%
                if(stock/3>=100){
                    %>
                    
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12"></span></span></td>
                    </tr>
                    
                    <%
                }else if(stock/3>=90){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=80){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=70){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=60){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=50){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=40){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=30){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>=20){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3>10){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9"></span></span></td>
                    </tr>
                    <%
                }else if(stock/3==0){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td><td>
                        <td rowspan="2" class="td"> <%= rs.getString("stock")%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10"></span></span></td>
                    </tr>
                    <%
                }
                    %>
                    <%
                if(npqty>=100){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material12b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=90){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material1b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=80){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material2b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=70){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material3b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=60){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material4b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=50){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material5b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=40){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material6b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=30){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material7b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>=20){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material8b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty>10){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material9b"></span></span></td>
                    </tr>
                    <%
                }else if(npqty==0){
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material11b"></span></span></td>
                    </tr>
                    <%
                }else{
                    %>
                    <tr>
                        <td></td>
                        <td rowspan="2" class="td"><%=production_qty%></td>
                    </tr>
                    <tr>
                        <td><span class="bar"><span class="material10b"></span></span></td>
                    </tr>
                    <%
                }
    
                    %>
                    </table>
                    </td>
                    <td class="td">
                    
                    <%
                      
                        if(ordr_qty >x){
                            %>
                            
                            <img src="../../img/warning.png" class="del-img">
                            <%
                        }
                    %>
                        
                    </td>
                    </tr>
                    </table>
                <%}}%>
                
            </div>
            
        </div>

    <%}}%>
    
    </body>
    
  
</html>