<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.lang.Math"%>
<%@ page import = "java.time.*"%>

<!DOCTYPE html>

<html>
    <head>
        <title>Mostly Used Raw Material</title>
        <link rel="stylesheet" type="text/css" href="css/bar.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    </head>

    <body>
        <%
            String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
                ResultSet rs1;
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                /*PreparedStatement psm1 = conn.prepareStatement("select * from logged where userID=?");
                psm1.setString(1,uid);
                rs1 = psm1.executeQuery();
                if(!rs1.next()){
                    response.sendRedirect("/project/index.jsp");
                }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../../logout.jsp?user=1" class="logout">Logout</a>
            </div>
        </header>

        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#" ><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a href="#" id="a1"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" id="a1"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="generatereport.jsp">Financial Analysis</a></li>
                            <li id="li"><a href="additional.jsp">Additional Data</a></li>
                            <li id="li"><a href="#" id="a1">Raw Materials</a></li>
                            <li id="li"><a href="productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>      
        </div>



        
        <div class="content" style="padding: 90px 80px; top: 57%; left: 50%;">
            <h3>Mostly Used Raw Materials</h3>
            <%          
                LocalDate date = LocalDate.now();
                /*date = date.plusDays(1);*/
                String strDate = date.toString();

                
            %>

            <!--Form to filter by date-->
            <form>
                <table class="table" align="center">
                    <tr>
                        <td class="td2">
                            <label for="from">From *</label>
                            <input id="fromDate" type="date" name="from" value="" max="<%=strDate%>" required>
                        </td>
                        <td class="td2">
                            <label for="to">To *</label>
                            <input id="toDate" type="date" name="to" value="" max="<%=strDate%>" required>
                        </td>
                        
                        <td class="td2">
                            <button type="submit" name="user" onclick="form.action='bar.jsp'">Filter</button>
                        </td>
                    </tr>
                </table>
            </form>
        
            <script>
                $("#fromDate").change(function(){
                    var selectedDate = $(this).val();
                    $("#toDate").attr("min", selectedDate);
                })
            </script>
        
            <%
                ResultSet rs3;
                String from = request.getParameter("from");
                String to = request.getParameter("to");
                /*String sup = request.getParameter("sup");*/

                String labels = "";
                String data = "";
                
                if(from!=null){
                    PreparedStatement psm3 = conn.prepareStatement("select sum(p.amount), m.name from materials m inner join productionmaterials p on m.materialID = p.materialID inner join production pr on pr.productionID = p.productionID where pr.edate between ? and ? group by m.name order by sum(p.amount) desc limit 5");  
                    psm3.setString(1,from);
                    psm3.setString(2,to);
                    rs3 = psm3.executeQuery();

                    /*else{
                        PreparedStatement psm3 = conn.prepareStatement("select sum(sm.quantity), m.name from materials m inner join supordermaterials sm inner join suporder so on so.orderID = sm.orderID inner join supplier s on s.supplierID = so.supplierID inner join allorders a on a.orderID = so.orderID where s.company = ? and a.req between ? and ? group by so.supplierID, sm.materialID order by sum(sm.quantity) desc limit 5");  

                        PreparedStatement psm3 = conn.prepareStatement("select sum(p.amount), m.name from materials m inner join productionmaterials p on m.materialID = p.materialID inner join production pr on pr.productionID = p.productionID inner join supplier_materials sm on sm.materialID = m.materialID inner join supplier s on s.supplierID = sm.supplierID where s.company = ? and pr.edate between ? and ? group by m.name order by sum(p.amount) desc limit 5"); 
                        psm3.setString(1,sup);
                        psm3.setString(2,from);
                        psm3.setString(3,to);
                        rs3 = psm3.executeQuery();
                    }*/
                }

                else{
                    PreparedStatement psm3 = conn.prepareStatement("select sum(p.amount), m.name from materials m inner join productionmaterials p on m.materialID = p.materialID group by m.name order by sum(p.amount) desc limit 5");
                    rs3 = psm3.executeQuery();
                }

                while(rs3.next()){
                    labels = labels+","+rs3.getString("m.name").toString();
                    data = data+","+rs3.getString("sum(p.amount)");
                }
            %>

            <hr>
            <%
                if(from!=null){
            %>
            <h4 align="center">From <%out.print(from);%> To <%out.print(to);%></h4>

            <%
                }
                else{
            %>
            <h4 align="center">Overall</h4>
            <%
                }
            %>
            <hr><br>

            <!--bar chart-->
            <div style=" position: relative; width: 75%; height: 75%;" class= "centerchart">
                <canvas id="bar-chart"></canvas>
            </div>

            <script>
                var temp = '<%=labels%>';
                var labelArr = temp.split(',').slice(1,6);
                console.log(labelArr);
                var temp1 = '<%=data%>';
                var dataArr = temp1.split(',').slice(1,6);
                console.log(dataArr);

                new Chart(document.getElementById("bar-chart"), {
                    type: 'bar',
                    data: {
                    labels: labelArr,
                    datasets: [{
                        label: "Amount",
                        backgroundColor: "#5779e9",
                        data: dataArr
                    }]
                    },
                    options: {
                        legend: { 
                            display: false 
                        },
                        title: {
                            display: false,
                            text: 'Mostly Used Raw Materials'
                        },
                        scales: {
                            yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                }  
                            }]
                        },
                        layout:{
                            padding:{
                                left:75,
                                right:0,
                                bottom:0,
                                top:0
                            }
                        } 
                    }
                });
            </script>
        </div>
        <%
            }}
        %>
    </body>
</html>