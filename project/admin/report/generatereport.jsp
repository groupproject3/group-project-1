<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>



<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/generatereport.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <head>
        <title>Report</title>
    </head>
    <body>
    <%String uid = "1";
        String user;
        if(session.getAttribute("user")==null){
            response.sendRedirect("/project/index.jsp");
        }else{
            user = (String)session.getAttribute("user");
        if(user.compareTo("admin")!=0){
            response.sendRedirect("/project/index.jsp");
        }else{
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="../../logout.jsp?user=1" class="logout">Logout</a>
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <img src="/project/img/avatar.svg" class="profileimg"  alt="">
                <h4 class="h4">Admin</h4>
            </div>
            <ul  class="ul">
                <li><a href="../adminhome.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <%-- <li><a href="#"><i class="fas fa-boxes"></i>Inventory</a> --%>
                <li><label for="btn-sub1" class="sub-head" ><i class="fas fa-boxes"></i>Inventory<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub1">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../inventory/inventoryitems.jsp">Items</a></li>
                            <li id="li"><a href="../inventory/inventorystocks.jsp">Stocks</a></li>
                            <li id="li"><a href="../inventory/inventoryproduction.jsp">Production</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-dollar-sign"></i>Sales</a> --%>
                <li><label for="btn-sub2" class="sub-head" ><i class="fas fa-dollar-sign"></i>Sales<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub2">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../sales/additem.jsp">Add Items</a></li>
                            <li id="li"><a href="../sales/productlist.jsp">Item List</a></li>
                            <li id="li"><a href="../sales/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                <%-- <li><a href="#"><i class="fas fa-truck"></i>Supply</a> --%>
                <li><label for="btn-sub3" class="sub-head" ><i class="fas fa-truck"></i>Supply<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub3">
                    <div class="submenu">
                        <ul>
                            <li id="li"><a href="../supply/supplierlist.jsp">Suppliers</a></li>
                            <li id="li"><a href="../supply/orderlist.jsp">Orders</a></li>
                        </ul>
                    </div>
                </li>
                
                <%-- <li><a id="a1" href="#"><i class="fas fa-file"></i>Report</a> --%>
                <li><label for="btn-sub4" class="sub-head" id="a1"><i class="fas fa-file"></i>Report<span><i class="fas fa-caret-down"></i></span></label>                   
                <input type="checkbox" id="btn-sub4">
                     <div class="submenu">
                        <ul>
                            <li id="li"><a href="#"  id="a1">Financial Analysis</a></li>
                            <%-- <li id="li"><a href="additional.jsp">Additional Data</a></li> --%>
                            <li id="li"><a href="bar.jsp">Raw Materials</a></li>
                            <li id="li"><a href="bestselling.jsp">Sales of Products</a></li>
                            <li id="li"><a href="productinfo.jsp?mtype=all">Products Info</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../settings/adminsettings.jsp"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
            <h3>Financial Analysis</h3>
    <div class="chart">
        <canvas id="myChart"></canvas>
    </div>

    

    <script>
        var i =0;
    </Script>


    <%
        double[] months1;
        double[] months2;
        double[] profit;
        months1 = new double[12];
        months2 = new double[12];
        profit = new double[12];

        ResultSet rs3;
        
        PreparedStatement psm3 = conn.prepareStatement("select sum(amount), MONTH(date) from income_expence where flag = ? AND date LIKE ? GROUP by MONTH(date)");
        psm3.setString(1,"income");
        psm3.setString(2,"2021%");
        
        rs3 =  psm3.executeQuery();

        while(rs3.next()){
        int month1 = rs3.getInt("MONTH(date)");
        Double sum1 = rs3.getDouble("sum(amount)");
            
        months1[month1-1] = sum1;
        
        }

        PreparedStatement psm5 = conn.prepareStatement("select sum(amount), MONTH(date) from income_expence where flag = ? AND date LIKE ? GROUP by MONTH(date)");
        psm5.setString(1,"expence");
        psm5.setString(2,"2021%");
        rs5 =  psm5.executeQuery();
        
        while(rs5.next()){
            int month2 = rs5.getInt("MONTH(date)");
            Double sum2 = rs5.getDouble("sum(amount)");

            months2[month2-1] = sum2;
        }

        for (int i = 0; i < 12; i++) {
           profit[i] = months1[i] - months2[i]; 
           
        }

    %>    

    <script>
        Chart.defaults.global.title.display = true;
        Chart.defaults.global.elements.point.pointStyle = "circle";

        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','Octomber','November','December'],
            datasets: [{
                label: 'Profit',
                //backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(56, 138, 232)',
               
                data: [<%=profit[0]%>,<%=profit[1]%>,<%=profit[2]%>,<%=profit[3]%>,<%=profit[4]%>,<%=profit[5]%>,<%=profit[6]%>,<%=profit[7]%>,<%=profit[8]%>,<%=profit[9]%>,<%=profit[10]%>,<%=profit[11]%>]
            },
            {
                label: 'Expense',
                fillColor: "rgb(255, 0, 0)",
                borderColor: 'rgb(255, 0, 0)',
                data: [<%=months2[0]%>,<%=months2[1]%>,<%=months2[2]%>,<%=months2[3]%>,<%=months2[4]%>,<%=months2[5]%>,<%=months2[6]%>,<%=months2[7]%>,<%=months2[8]%>,<%=months2[9]%>,<%=months2[10]%>,<%=months2[11]%>]
            },
            {
                label: 'Income',
                fillColor: "rgb(0, 201, 67)",
                borderColor: 'rgb(0, 201, 67)',
                data: [<%=months1[0]%>,<%=months1[1]%>,<%=months1[2]%>,<%=months1[3]%>,<%=months1[4]%>,<%=months1[5]%>,<%=months1[6]%>,<%=months1[7]%>,<%=months1[8]%>,<%=months1[9]%>,<%=months1[10]%>,<%=months1[11]%>]
            }
            ]
        },

        // Configuration options go here
        options: {
            elements: {
                line: {
                    tension: 0
                }
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                    display: true,
                    labelString: 'Amount (Rs.)',
                    fontStyle: 'bold',
                    fontColor: '#FF0000'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                    display: true,
                    labelString: 'Month',
                    fontStyle: 'bold',
                    fontColor: '#FF0000'
                    }
                }]
            }     
            }
        });
    </script>
                   
            </div>
                 </div>   
       

    <%}}%>
    </body>
    
  
</html>