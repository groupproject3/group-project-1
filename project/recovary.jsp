<%@ page import = "java.sql.*"%>
<%@ page import = "java.util.Random"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="recovary.css">
    <head>
        <title>Reset Password</title>
    </head>
    <body>
        <div class="box">
            <script type = "text/javascript">  
                    function pwdmatch(){  
                        var pwd1 = document.pwdform.password.value;  
                        var pwd2 = document.pwdform.repassword.value;  
                      
                        if(pwd1 == pwd2){  
                            // return true;  
                            document.getElementById("match").classList.add('hidden');
                        }  
                        else{  
                            // alert("Passwords must be the same!");  
                            document.getElementById("match").classList.remove('hidden');
                            return false; 
                        }  
                    }  
                </script> 

            <h1>Recovery</h1>
            <form action="recovaryconnect.jsp" name="pwdform" method="POST" onsubmit="return pwdmatch()">
            <%
            String user = request.getParameter("user");
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                PreparedStatement psm = conn.prepareStatement("select * from usertable where userID=?");
                psm.setString(1,user);
                ResultSet rs = psm.executeQuery();
                if(rs.next()){
                %>
                <p>User Name</p>
                <input type="text" name="name" value="<%=rs.getString("name")%>" readonly>
                <p>Password</p>
                <input type="password" name="password" placeholder="Enter a password">
                <p>Re-enter Password</p>
                <input type="password" name="repassword" placeholder="Enter a password">
                <p id="match" style="font-size:smaller;" class ="error hidden">Passwords must be same!</p>
                <button type="submit" name="user" value="<%=rs.getString("userID")%>">Submit</button>
                
                <%}%>
            </form>
            
            <form action="forgotpassword.jsp" method="GET">
                <button type="submit">Back</button>
            </form>
        </div>
    </body>
    
  
</html>