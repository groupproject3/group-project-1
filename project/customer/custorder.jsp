<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>



<!DOCTYPE html>
<html>
  <link rel="stylesheet" type="text/css" href="css/inventoryitems.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Customize Oder</title>
  </head>
  <body>
<%
  String uid = request.getParameter("user");
  String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

    ResultSet rs5;
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
      psm5.setString(1,uid);
      rs5 = psm5.executeQuery();
      if(!rs5.next()){
          response.sendRedirect("/project/index.jsp");
      }*/
       LocalDate date = LocalDate.now();
          date = date.plusDays(1);
          String strDate = date.toString();


%>
  <input type="checkbox" id="check">
  <!--header-->
  <header>
    <label for="check">
      <i class="fas fa-bars" id="sidebtn"></i>
    </label>
    <div class="leftarea">
      <h3>EMS<span></span></h3>
    </div>
    <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
    </div>
  </header>
  <!--sidebar-->
  <div class="sidebar">
   <div class="center">
                <%
                ResultSet rs6;
                PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                psm6.setString(1,uid);
                rs6 = psm6.executeQuery();
                if(rs6.next()){
                    
                %>
                    <img src="img/avatar.svg" class="profileimg" alt="">
                    <h4><%=rs6.getString("name")%></h4>
                
                <%}%>
            </div>
    <ul class="ul">
                <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
                <li><label for="btn-sub" class="sub-head" ><i class="fas fa-boxes"></i>Show Items<span><i class="fas fa-caret-down"></i></span></label>                   
                    <input type="checkbox" id="btn-sub">
                    <div class="submenu">
                        <ul>    
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Items</a></li>
                    <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                    <li id="li3"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                    <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass Based</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="custorder.jsp" id="a1"><i class="fas fa-shopping-bag"></i>Customized Order</a></li>
                <li><a href="place-order.jsp?user=<%=uid%>"><i class="fas fa-truck"></i>Order</a></li>
                <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
                
            </ul>
            
  </div>

  <div class="content">
    <div class="box">  
          <form action = "#">   
      <table>
        <tr>
            <td class="td"><h2>Order Form</h2></td>
          
        </tr>
        <tr>
          <td class="td" style="height: 30px;">
            <p> <b>Required Date</b></p>
            <input type="date"  value="" name="reqdate" min="<%=strDate%>" id="name" Required>
          </td>
          <td class="td"> 
            <p style="margin-left: 60px;"><b>Quantity</b></p>
            
            <div class="qty" id="incdec1">
              <input type="number" value="0" name="quantity"  id="quantity" min="1"   required/>
              <%-- <input type="number" name = "quantity" value="0" min="1" />
              <div class="updwn">
              <a><i class="fa fa-angle-up" style="font-size:36px" id="up1"></i></a>
              <a><i class="fa fa-angle-down" style="font-size:36px" id="down1"></i></a>
              </div> --%>
            </div>  
          </td>
        </tr>
        <tr>
          <td class="td">
            <p><b>Drawing</b></p>
            <div class = "container">
              <div class = "wrapper">
                <div class = "image">
                  <img src = " " alt="" onerror='this.src="img/img cart/zip.png"'>
                </div>
                <div class = "contenter">
                  <div class="icon"><i class = "fas fa-cloud-upload-alt"></i></div>
                  <div class="icon">Required file chosen, No file yet!</div>
                </div>
                <div id="cancel-btn"><i class = "fas fa-times"></i></div>
                <div class="file-name">File name here</div>
              </div>
              <div>
              <input id= "default-btn" name = "path" type = "file" onchange="return fileValidation()" hidden required>
              <label for="default-btn">Choose a file</label>
             <%-- <button onclick = "defaultBtnActive()" id="custom-btn">Choose a file</button> --%>
            </div>  
          </td>  
        </tr>
        <tr>
          <th colspan="2"><textarea id="description" name="description" rows="3" cols="50" placeholder="Enter any Description"></textarea></th>
        </tr> 
        <tr>
          <th></th>
          <th><button  type="submit" id="btn1" class = "btn2" name="user" value="<%=uid%>" onclick="form.action='application/insert-custorder.jsp'">Submit</button></th>
        </tr>    
      
<script>
   
    const wrapper = document.querySelector(".wrapper");
    const fileName = document.querySelector(".file-name");
    const cancelBtn = document.querySelector("#cancel-btn");
    const defaultBtn = document.querySelector("#default-btn");
    const costomtBtn = document.querySelector("#costom-btn");
    const img = document.querySelector(".image img");
    let regExp = /[0-9a-zA-Z\^\&\@\{\}\[\]\,\$\=\~\!\-\#\(\)\.%\+\_ ]+$/;
    function defaultBtnActive(){
      defaultBtn.click();
    }
  
    var fileInput = document.getElementById('default-btn'); 
    var filePath = fileInput.value;   
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.JPG)$/i; 
        
      defaultBtn.addEventListener("change",function(){
        const file = this.files[0];
        if(file){
          
          const reader = new FileReader();
          reader.onload = function(){
          const result = reader.result;
          
          img.src = result;
          wrapper.classList.add("active");
        }
        cancelBtn.addEventListener("click", function(){
        img.src = "";
        wrapper.classList.remove("active");
      });
        reader.readAsDataURL(file)
      }

      if(this.value){
        let valueStore = this.value.match(regExp);
        fileName.textContent = valueStore;  
        }
      });
    
  
</script>
                  
<script>
  $(document).ready(function(){
    $("#up1").on('click',function(){
      if($("#incdec1 input").val()<20){
        $("#incdec1 input").val(parseInt($("#incdec1 input").val())+1);
      }  
    });

    $("#down1").on('click',function(){
      if($("#incdec1 input").val()!=0){
        $("#incdec1 input").val(parseInt($("#incdec1 input").val())-1);
      }
    });

  });
</script>
</table>
    </form>
    </div>    
  </div>
  <%}}%>

</body>
</html>