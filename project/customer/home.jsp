<!-- <%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%> -->


<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/hom.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <title>Home</title>
    </head>
    <body>
    
        <%String uid ; 
         String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->

        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
                
            
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <div class="center">
                <%
                ResultSet rs6;
                PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                psm6.setString(1,uid);
                rs6 = psm6.executeQuery();
                if(rs6.next()){
                    
                %>
                    <img src="img/avatar.svg" class="profileimg" alt="">
                    <h4><%=rs6.getString("name")%></h4>
                
                <%}%>
            </div>
            <ul class="ul">
                <li><a href="#" id="a1"><i class="fas fa-house-user"></i>Home</a></li>
                <li><label for="btn-sub" class="sub-head" ><i class="fas fa-boxes"></i>Show Items<span><i class="fas fa-caret-down"></i></span></label>                   
                    <input type="checkbox" id="btn-sub">
                    <div class="submenu">
                        <ul>    
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Items</a></li>
                    <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                    <li id="li3"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                    <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass Based</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="custorder.jsp"><i class="fas fa-shopping-bag"></i>Customized Order</a></li>
                <li><a href="place-order.jsp?user=<%=uid%>"><i class="fas fa-truck"></i>Order</a></li>
                <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
                
            </ul>
            
        </div>

        <div class="content" style="height: 100%;">
        
                <div class="slid">
                    
                    <div class="slideshow-container">

                       
                        <div class="mySlides fade">
                            <div class="numbertext">1 / 4</div>
                            <img src="img/logo2.png" >
                      
                        </div>

                        <div class="mySlides fade">
                        <div class="numbertext">2 / 4</div>
                        <img src="img/item1.jpg" >
                  
                        </div>
                    
                        <div class="mySlides fade">
                        <div class="numbertext">3 / 4</div>
                        <img src="img/item2.jpg" >
                       
                        </div>
                    
                        <div class="mySlides fade">
                        <div class="numbertext">4 / 4</div>
                        <img src="img/item3.jpg" >
                    
                        </div>
                    
                        <!-- Next and previous buttons -->
                        <!-- <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)">&#10095;</a> -->
                    </div>
                    <br>
                    
                    <!-- The dots/circles -->
                    <div style="text-align:center">
                        <span class="dot" onclick="currentSlide(1)"></span>
                        <span class="dot" onclick="currentSlide(2)"></span>
                        <span class="dot" onclick="currentSlide(3)"></span>
                        <span class="dot" onclick="currentSlide(4)"></span>
                    </div>
                </div>

                 <!-- Site footer -->
            <footer class="site-footer">
                <table style="table-layout: auto; width: 100%;">
                    
                    <tr>
                        <div class="container"></div>
                        <div class="row">
                            <td style="width: 960px; ">
                                <div style="width: 500px;margin-left: 20px;">
                                <h6>About</h6>
                                <p class="text-justify" style="width: 630px;text-align: justify;"><b>Asiri Industries (Pvt) Ltd.</b>
                                    Cast Iron, Bronze, Aluminium and S.G. Iron Founders.<br><br>
                                    <b>Asiri Industries (Pvt) Ltd.</b> is a cast iron industry that was established in 1985. The industry is manufacturing the finest machinery parts since then. Among the many machinery parts 
                                    are concrete mixture parts, manhole, dumbell parts, toggle plates, wheels. We even manufacture machinery parts according to individual needs and we provide the best service and 
                                    products to meet the customer satisfaction.</p>
                                </div>
                            </td>
                            <td style="width: 300px;">
                                <div class="col-xs-6 col-md-3">
                                <h6>Certified</h6>
                                <ul class="footer-links">
                                    <li><a href="#">Goverment</a></li>
                                    <li><a href="#">Municiple</a></li>
                                    <li><a href="#">Business Ministry</a></li>
                                </ul>
                                </div>
                            </td>
                            <td style="width: 460px;">
                                <div class="col-xs-6 col-md-3">
                                <h6>Quick Links</h6>
                                <ul class="footer-links">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul>
                                </div>
                            </td>
                        </div>
                    </div>
                    </tr> 
                      
                    <tr>
                    <td>
                    <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="social-icons">
                            <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></i></a></li>
                            <li><a class="dribbble" href="#"><i class="fas fa-envelope"></i></a></li>
                            <li><a class="linkedin" href="#"><i class="fas fa-map-marker-alt"></i></a></li>   
                        </ul>
                        </div>

                    </div>
                    </td>
                    </div>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                
                                <p class="copyright-text">Copyright &copy; 2017 All Rights Reserved by 
                                <a href="#">Scanfcode</a>.
                                </p>
                                
                            </div>
                        </td>
                    </tr>      
                </table>
        </footer>
            
        </div>

        <script>
            var slideIndex = 0;
            showSlides();
            
            function showSlides() {
              var i;
              var slides = document.getElementsByClassName("mySlides");
              var dots = document.getElementsByClassName("dot");
              for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";  
              }
              slideIndex++;
              if (slideIndex > slides.length) {slideIndex = 1}    
              for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
              }
              slides[slideIndex-1].style.display = "block";  
              dots[slideIndex-1].className += " active";
              setTimeout(showSlides, 4000); // Change image every 2 seconds
            }

            </script>  
        <%}}%>    
    </body> 
  
</html>