<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/place-order.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Place Order</title>
    </head>
    <body>
   <%String uid = request.getParameter("user");
   String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
        <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
    </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
           <div class="center">
                <%
                ResultSet rs6;
                PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                psm6.setString(1,uid);
                rs6 = psm6.executeQuery();
                if(rs6.next()){
                    
                %>
                    <img src="img/avatar.svg" class="profileimg" alt="">
                    <h4><%=rs6.getString("name")%></h4>
                
                <%}%>
            </div>
            <ul class="ul">
                <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
                <li><label for="btn-sub" class="sub-head" ><i class="fas fa-boxes"></i>Show Items<span><i class="fas fa-caret-down"></i></span></label>                   
                    <input type="checkbox" id="btn-sub">
                    <div class="submenu">
                        <ul>    
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Items</a></li>
                    <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                    <li id="li3"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                    <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass Based</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="custorder.jsp"><i class="fas fa-shopping-bag"></i>Customized Order</a></li>
                <li><a href="#" id="a1"><i class="fas fa-truck"></i>Order</a></li>
                <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
                
            </ul>
         
            
        </div>
        <div class="content">
            <div class="box">

            

                <table cellpadding ="0" width="100%">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Order Type</th>
                            <th>Status</th>
                            <th>Placed Date</th>
                            <th>Items</th>
                            <th>Quotation</th>
                        </tr>
                    </thead>
                     <%
                        ResultSet rs;
                        PreparedStatement psm = conn.prepareStatement("select allorders.orderID,allorders.status,allorders.placed,custorder.flag from allorders inner join custorder on allorders.orderID = custorder.orderID where allorders.userID = ?");
                        psm.setString(1,uid);
                        rs =  psm.executeQuery();

                        while(rs.next()){   
                        String id2 = rs.getString("orderID");
                        String flag1 = rs.getString("flag");
                        String sts1 = rs.getString("status");   
                    %>   
                                  
                        <tr>
                        <form>
                            <td><%=rs.getString("orderID") %></td>
                            <td><%=rs.getString("flag") %></td>
                            <td><%=rs.getString("status") %></td>
                            <td><%=rs.getString("placed") %></td>

                            <%if(flag1.compareTo("normal")==0){%>
                                 <td><button  type="submit" class = "btn2" name="id" value="<%=id2%>" onclick="form.action='order-list.jsp'">Ordered Items</button></td>
                                <%-- <td><a href="order-list.jsp?id=<%=id2%>">Ordered Items</a></td> --%>
                            <%}else{%>
                                <td><button  type="submit" class = "btn2" name="id" value="<%=id2%>" onclick="form.action='custorder-item.jsp'">Order Details</button></td>
                                <%-- <td><a href="custorder-item.jsp?id=<%=id2%>">Order Details</a></td> --%>
                            <%}%>    
                            </form>
                            <!-- <td><a href="viewquotation.jsp?user=<%=uid%>">Quotation</a></td> -->
                            <%if(flag1.compareTo("normal")==0){%>
                            <form>
                                <td><button  type="submit" class = "btn2" name="id" value="<%=id2%>" onclick="form.action='conform-resipt.jsp'">Order Receipt</button></td>
                                <%-- <td><a href="conform-resipt.jsp?id=<%=id2%>">Order receipt</a></td> --%>
                            </form>    
                            <%}else{%>
                                <%if(sts1.compareTo("placed")==0){%>
                                    <td><button  type="text" class = "btn2" value="<%=id2%>" onclick="display1();">Quotation</button></td>
                                    <%-- <td "><a href="#" onclick="display1();">Quotation</a></td> --%>
                                <%}else{%>
                                    <form>
                                    <td><button  type="submit" class = "btn2" name="btn" value="<%=id2%>" onclick="form.action='custorder-quotation.jsp'">Quotation</button></td>
                                    <input type="text" name="stat" value="<%=sts1%>" hidden>
                                    <%-- <td ><a href="custorder-quotation.jsp?btn=<%=id2%>&stat=<%=sts1%> ">Quotation</a></td> --%>
                                    </form>
                                <%}%>         
                            <%}%>
                        </tr>
                     
                     <%}%>                       
                </table>
           
                   
            </div>
            
        </div>

     <div id="popup1" class="popup">

    <!-- Modal content -->
    <div class="popup-content">
        <span class="close" onclick="hide1();">&times;</span>
        <h3>Warning</h3>
        <p id="mid2"></p>
        <table style="float:right;">
            <tr>
               
                    <%-- <form>
                        <button type="submit" id="btn3" class="btn3" name="pid"  onclick="form.action='application/deleteitem.jsp'">Yes</button>
                    </form> --%>
                
                <th style="background-color:white;"><button type="submit" id="btn4" class="btn4" onclick="hide1();">OK</button></th>
            </tr>
        </table>
        
        </div>  
    </div>
               
                
    <script>
        // Get the modal
        var pop1 = document.getElementById("popup1");
        //var materilal = document.getElementById("mid").value;
        // Get the button that opens the modal
        //var btn1 = document.getElementById("btn1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];       

        // When the user clicks the button, open the modal 
        function display1( ){
        document.getElementById('mid2').innerHTML = "Not received Quotation";
        pop1.style.display = "block";
        }
        function hide1(){
        pop1.style.display = "none";
        }
        
        // When the user clicks on <span> (x), close the modal


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == pop1) {
            pop1.style.display = "none";
        }
        }
    </script>

    <%}}%>
    </body>
    
  
</html>
        
    
  