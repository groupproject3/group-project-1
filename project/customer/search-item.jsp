<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
 
<!DOCTYPE html>
<html>
   
<link rel="stylesheet" type="text/css" href="css/new09 30.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Search Item</title>
</head>
<body>
<%String uid = request.getParameter("user");
 // String search = request.getParameter("search");

    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	   /* PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
<input type="checkbox" id="check">
<!--header-->
<header>
    <label for="check">
        <i class="fas fa-bars" id="sidebtn"></i>
    </label>
    <div class="leftarea">
        <h3>Menu<span></span></h3>
    </div>
    <div class="rightarea">
        <a href="../logout.jsp" class="logout">Logout</a>
        <%
        ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a>
                <%}%>

        <div class="searchbar">
            <form>
            <input type="text" name="Search" value = "" placeholder="Search..">
            <button class="search-bttn" type="submit"  value=""  onclick="form.action='search-item.jsp'"><i class="fa fa-search" aria-hidden="true"></i></button>
            <%-- <a href="search-item.jsp" class="searchicon"><i class="fa fa-search" aria-hidden="true"></i></a> --%>
            </form>
        </div>
        <div class="cart-btn">
            <a href="cartnew.jsp?user=<%=uid%>" class="c-btn"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
    </div>
    </div>
</header>
<!--sidebar-->
<div class="cart-btn">
            <button style="background:transparent;" class="ct-btn" disabled></button>
    </div>
    
<div class="sidebar">
    <center>
        <%
        ResultSet rs6;
        PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
        psm6.setString(1,uid);
        rs6 = psm6.executeQuery();
        if(rs6.next()){
            
        %>
            <img src="img/avatar.svg" class="profileimg" alt="">
            <h4><%=rs6.getString("name")%></h4>
        <%}%>
    </center>
    <ul class="ul">
        <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
        <li><label for="btn-sub" class="sub-head" id="a1" ><i class="fa fa-shopping-bag" aria-hidden="true"></i>Search Results<span><i class="fas fa-caret-down"></i></span></label>                   
            <input type="checkbox" id="btn-sub">
            <div class="submenu">
                <ul>
                    <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                    <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                    <li id="li3"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                    <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass Based</a></li>
                </ul>
            </div>
        </li>
        <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-shopping-bag"></i>Customized Order</a></li>
        <li><a href="place-order.jsp?user=<%=uid%>"><i class="fas fa-truck"></i>Order</a></li>
        <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
        <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>

    </ul>
    
</div>

<div class="content">
    <table style="width:90%" class="table1">
    <%
        String Search = request.getParameter("Search");
        ResultSet rs;
        PreparedStatement psm = conn.prepareStatement("select * from products where name like ?");
        psm.setString(1, "%" + Search + "%");
        rs =  psm.executeQuery();
        


        while(rs.next()){
        String id = rs.getString("productID");
        String item_img = rs.getString("path"); 
        String cat = rs.getString("category");
        String item_name = rs.getString("name");
        String item_size = rs.getString("productID")+"size";
        String item_price = rs.getString("uprice");
        String item_min = rs.getString("min");
         
    %>
    
    <tr class="tr1">
    <div class="product-item 1">
        <th>            
            <img class="item-img1" src="<%=rs.getString("path") %>" alt="Denim Jeans" style="width:15%">

            <div class="detail1">
                <table style=" table-layout: fixed;width: 700px;margin-left: 20px; margin-right: 30px; table-layout: auto;">
                    <tr class="tr2">
                        <td>
                            <ul>
                                <li class="li2"><%=rs.getString("category") %></li>
                                <li class="li1"><h3>Product ID :&nbsp<%=rs.getString("productID")%></h3></li>
                                <li class="li1"><h3><input type="text" style="height:28px;margin-top:-5px" value="<%=item_name%>" name="product_name" id="<%=rs.getString("productID")%>" readonly></h3></li>
                                <li class="li1"><h3 id="<%=item_size%>"><%=rs.getString("size")%></h3></li>
                                <li class="li1"><h3>Minimun Order Quantity&nbsp-&nbsp<%=rs.getString("min")%></h3></li>
                            </ul>
                        </td>
                        <td>
                            <input class="unit-price" style="color: rgb(141, 42, 42);font-size: 35px;width: 250px; height: 70px;" type="text" value="LKR <%=rs.getString("uprice") %>" name="unit_price" id="unit_price" readonly>
                        </td>     
                    </tr>
                </table>
                <div class="submit-btn">
                <a href="#" class="addtocart"   onclick="display1('<%=id%>','<%=cat%>','<%=item_img%>',<%=item_price%>,'<%=item_min%>');"><i class="fas fa-shopping-cart"></i> Add to Cart</a>
                <a href="item-detail.jsp?user=<%=uid%>&id=<%=id%>"  class="writereview"><i class="fas fa-pen"></i> More Details or Buy Now</a>
                </div>
            </div>    
        </th>    
    </div>          
    </tr>
    
    <%}%>
    </table> 
</div>

        <div id="popup1" class="popup">
            <!-- Modal content -->
            <div class="popup-content">
            <form>
                <span class="close" onclick="hide1();">&times;</span>
                <%-- <h2 name = "product_name" id="popup_name" value=""></h2> --%>
                <input class="topic"  type="text"  name = "product_name" id="popup_name" readonly>
                <table style="float:left;">
                    <tr>
                        <th><img class="item-img1" id="popup_img" src="#" alt="Denim Jeans"></th>
                        <th>
                            <ul class = "popupul">
                                <li><h3 id="popup_cat"></h3></li>
                                <%-- <li ><h3 name="id" id="popup_id"></h3></li> --%>
                                <li><input class="unit-price"  type="text"  name = "id" id="popup_id" readonly></li>
                            </ul>
                            <ul class="subul">
                                <li><h3>Minimun Order Quantity :</h3></li>
                                <li><h3 id ="popup_min"></h3></li>
                            </ul>
                            <ul class="subul">
                                <li><p style="color: rgb(141, 42, 42);font-size: 42px; font-weight:5px; width: 90px; height: 40px;border:none;">LKR </p></li>
                                <li><input class="unit-price" style="color: rgb(141, 42, 42);font-size: 42px;font-weight:bold; width: 250px; height: 40px;border:none;" type="text"  name="unit_price" id="popup_price" readonly></li>
                            </ul>    
                            <ul class = "popupul" style ="padding-top: 40px">
                                <li>
                                    <div class="qty" id="incdec">
                                    <b>Quantity</b>
                                        <input oninput="displayDate()" name="quantity" id="qty_id" type="number" value="0" min=" " required/>
                                    
                                        <%-- <div class="updwn">
                                        <a onclick="up();"><i class="fa fa-angle-up" style="font-size:36px" id="up"></i></a>
                                        <a><i class="fa fa-angle-down" style="font-size:36px" id="down"></i></a>
                                        </div> --%>
                                    </div>
                                </li>
                                <li><b>Total Price</b> <input name = "total_price" id="total_id" type="text" required/></li>
                            </ul>
                        </th>
                    </tr>

                    <tr>
                        <th><button type="submit"id="btn3"  value="" onclick="form.action='application/insert.jsp'">Add Cart</button></form></th>
                    </tr>
                    <%-- <tr>
                        <td>
                            <form>
                                <button type="submit" id="btn3" class="btn3" name="pid"  onclick="form.action='application/deleteitem.jsp'">Yes</button>
                            </form>
                        </td>
                        <td><button type="submit" id="btn4" class="btn4" onclick="hide1();">No</button></td>
                    </tr> --%>
                </table>
                
            </div>       
        </div>
          
                
    <script>
        // Get the modal
        var pop1 = document.getElementById("popup1");
        //var materilal = document.getElementById("mid").value;
        // Get the button that opens the modal
        var btn1 = document.getElementById("btn1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];       

        // When the user clicks the button, open the modal 
        function display1(id,cat,item_img,item_price,item_min){
        var name = document.getElementById(id).value;
        var min_sum = Number(item_min)*Number(item_price);
        // var price = document.getElementById(item_price).value;
      
         document.getElementById('popup_id').value = id ; 
         document.getElementById('popup_cat').innerHTML = cat;  
         document.getElementById('popup_name').value = name ;
         document.getElementById('popup_img').src = item_img ;

         document.getElementById('popup_price').value = item_price.toFixed(2) ;
         document.getElementById('popup_min').innerHTML = item_min ;
         document.getElementById('qty_id').min = item_min;
         document.getElementById('qty_id').value = item_min ;
         document.getElementById('total_id').value = min_sum.toFixed(2) ;
          
        pop1.style.display = "block";
        }
        function hide1(){
        pop1.style.display = "none";
        }

        // document.getElementById("qty_id").addEventListener("click", displayDate);
        // var qty =  document.getElementById(qty_id).value;

        function displayDate() {
            var total,qty,price;
            price = document.getElementById('popup_price').value; 
            qty = document.getElementById('qty_id').value;
            total =Number(qty)*Number(price);
            document.getElementById('total_id').value = total.toFixed(2);
        }
        
        // When the user clicks on <span> (x), close the modal


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == pop1) {
            pop1.style.display = "none";
        }
        }
    </script>
    <%}%>
    
</body>
</html>