<!-- <%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%> -->


<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/customer-front.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
      <title>Welcome</title>
    </head>
    <body>

        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
               
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <a href="http://localhost:8080/project/" class="logout">login</a>
            </div>
        </header>

        
        <div class="content" style="height: 100%;">

            <!-- <div>
                <img class="item-img1" src="img/welcome 1.JPG" alt="Denim Jeans" style="width:15%">
            </div> -->
                <div class="slid">
                    
                    <div class="slideshow-container">

                        <!-- Full-width images with number and caption text -->
                        <div class="mySlides fade">
                            <div class="numbertext">1 / 4</div>
                            <img src="img/logo2.png" >
                      
                        </div>

                        <div class="mySlides fade">
                        <div class="numbertext">2 / 4</div>
                        <img src="img/item1.jpg" >
                  
                        </div>
                    
                        <div class="mySlides fade">
                        <div class="numbertext">3 / 4</div>
                        <img src="img/item2.jpg" >
                       
                        </div>
                    
                        <div class="mySlides fade">
                        <div class="numbertext">4 / 4</div>
                        <img src="img/item3.jpg" >
                    
                        </div>
                    
                        <!-- Next and previous buttons -->
                        <!-- <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                        <a class="next" onclick="plusSlides(1)">&#10095;</a> -->
                    </div>
                    <br>
                    
                    <!-- The dots/circles -->
                    <div style="text-align:center">
                        <span class="dot" onclick="currentSlide(1)"></span>
                        <span class="dot" onclick="currentSlide(2)"></span>
                        <span class="dot" onclick="currentSlide(3)"></span>
                        <span class="dot" onclick="currentSlide(4)"></span>
                    </div>
                </div>

                 <!-- Site footer -->
            <footer class="site-footer">
                <table style="table-layout: auto; width: 100%;">
                    
                    <tr>
                        <div class="container"></div>
                        <div class="row">
                            <td style="width: 960px; ">
                                <div style="width: 500px;margin-left: 20px;">
                                <h6>About</h6>
                                <p class="text-justify" style="width: 630px;text-align: justify;"><b>Asiri Industries (Pvt) Ltd.</b>
                                    Cast Iron, Bronze, Aluminium and S.G. Iron Founders.<br><br>
                                    <b>Asiri Industries (Pvt) Ltd.</b> is a cast iron industry that was established in 1985. The industry is manufacturing the finest machinery parts since then. Among the many machinery parts 
                                    are concrete mixture parts, manhole, dumbell parts, toggle plates, wheels. We even manufacture machinery parts according to individual needs and we provide the best service and 
                                    products to meet the customer satisfaction.</p>
                                </div>
                            </td>
                            <td style="width: 300px;">
                                <div class="col-xs-6 col-md-3">
                                <h6>Certified</h6>
                                <ul class="footer-links">
                                    <li><a href="http://scanfcode.com/category/c-language/">Goverment</a></li>
                                    <li><a href="http://scanfcode.com/category/front-end-development/">Municiple</a></li>
                                    <li><a href="http://scanfcode.com/category/back-end-development/">Business Ministry</a></li>
                                </ul>
                                </div>
                            </td>
                            <td style="width: 460px;">
                                <div class="col-xs-6 col-md-3">
                                <h6>Quick Links</h6>
                                <ul class="footer-links">
                                    <li><a href="http://scanfcode.com/about/">About Us</a></li>
                                    <li><a href="http://scanfcode.com/contact/">Contact Us</a></li>
                                    <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
                                </ul>
                                </div>
                            </td>
                        </div>
                    </div>
                    </tr> 
                      
                    <tr>
                    <td>
                    <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="social-icons">
                            <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></i></a></li>
                            <li><a class="dribbble" href="#"><i class="fas fa-envelope"></i></a></li>
                            <li><a class="linkedin" href="#"><i class="fas fa-map-marker-alt"></i></a></li>   
                        </ul>
                        </div>

                    </div>
                    </td>
                    </div>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                
                                <p class="copyright-text">Copyright &copy; 2017 All Rights Reserved by 
                                <a href="#">Scanfcode</a>.
                                </p>
                                
                            </div>
                        </td>
                    </tr>      
                </table>
        </footer>
            
        </div>

        <script>
            var slideIndex = 0;
            showSlides();
            
            function showSlides() {
              var i;
              var slides = document.getElementsByClassName("mySlides");
              var dots = document.getElementsByClassName("dot");
              for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";  
              }
              slideIndex++;
              if (slideIndex > slides.length) {slideIndex = 1}    
              for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
              }
              slides[slideIndex-1].style.display = "block";  
              dots[slideIndex-1].className += " active";
              setTimeout(showSlides, 4000); // Change image every 2 seconds
            }

            </script>

           
    
    </body>
    
  
</html>