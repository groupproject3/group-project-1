<!-- <%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%> -->


<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/hom.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <title>notification</title>
    </head>
    <body>
    
        <%String uid ; 
         String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");
        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.jsp");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->

        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
                <table style="float:right;">
            <tr>
                
                <td><a href="customer-front.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
                
            </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
           <div class="center">
                <%
                ResultSet rs6;
                PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                psm6.setString(1,uid);
                rs6 = psm6.executeQuery();
                if(rs6.next()){
                    
                %>
                    <img src="avatar.svg" class="profileimg" alt="">
                    <h4><%=rs6.getString("name")%></h4>
                
                <%}%>
            </div>
            <ul class="ul">
                <li><a href="../home.jsp"><i class="fas fa-house-user"></i>Home</a></li>
                <li><label for="btn-sub" class="sub-head" ><i class="fas fa-boxes"></i>Show Items<span><i class="fas fa-caret-down"></i></span></label>                   
                    <input type="checkbox" id="btn-sub">
                    <div class="submenu">
                        <ul>    
                            <li id="li1"><a href="../products1.jsp?user=<%=uid%>">All Items</a></li>
                    <li id="li2"><a href="../products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                    <li id="li3"><a href="../products3.jsp?user=<%=uid%>">Iron Based</a></li>
                    <li id="li4"><a href="../products4.jsp?user=<%=uid%>">Brass Based</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="../custorder.jsp"><i class="fas fa-shopping-bag"></i>Customized Order</a></li>
                <li><a href="../place-order.jsp?user=<%=uid%>"><i class="fas fa-truck"></i>Order</a></li>
                <li><a href="../cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
                <li><a href="../customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
                
            </ul>
            
        </div>


        <div class="content">
            <div class="box">
                <table cellpadding ="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                  <%
                        ResultSet rs;
                        
                        PreparedStatement psm = conn.prepareStatement("select notification.id,usertable.name,notification.type,notification.description,notification.link,notification.date from notification inner join usertable on usertable.userID=notification.from_user where notification.to_user=? and notification.status=?");
                        psm.setString(1,uid);
                        psm.setString(2,"not viewed");
                        rs =  psm.executeQuery();
                        while(rs.next()){
                            %>
                                <tr class="tr1">
                                    <td><i class="fas fa-exclamation-circle"></i> <%=rs.getString("name") %></td>
                                    <td><%=rs.getString("type") %></td>
                                    <td><%=rs.getString("description") %></td>
                                    <td><%=rs.getString("date") %></td>
                                    <td><a href="application/notifyredirect.jsp?id=<%=rs.getString("id") %>">Link</a></td>
                                </tr>
                            <%
                        }

                    %>
                    <%
                        ResultSet rs2;
                        
                        PreparedStatement psm2 = conn.prepareStatement("select notification.id,usertable.name,notification.type,notification.description,notification.link,notification.date from notification inner join usertable on usertable.userID=notification.from_user where notification.to_user=? and notification.status=?");
                        psm2.setString(1,uid);
                        psm2.setString(2,"viewed");
                        rs2 =  psm2.executeQuery();
                        while(rs2.next()){
                            %>
                                <tr class="tr1">
                                    <td><%=rs2.getString("name") %></td>
                                    <td><%=rs2.getString("type") %></td>
                                    <td><%=rs2.getString("description") %></td>
                                    <td><%=rs2.getString("date") %></td>
                                    <td><a href="application/notifyredirect.jsp?id=<%=rs2.getString("id") %>">Link</a></td>
                                </tr>
                            <%
                        }

                    %>
                
                </table>
                <form action="#">
                    <button type="submit" id="btn1" name="btn" value="" onclick="form.action='customer.jsp'">Back</button>
                </form>
            </div>
        </div>

    <%}}%>
    </body>
    
  
</html>S