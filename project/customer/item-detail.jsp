<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="css/item-detail.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<head>
    <title>item Detail</title>
</head>
<body>

<%String uid = request.getParameter("user");

    String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

    ResultSet rs5;
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
    psm5.setString(1,uid);
    rs5 = psm5.executeQuery();
    if(!rs5.next()){
        response.sendRedirect("/project/index.html");
    }*/


    %>
    <input type="checkbox" id="check">
    <!--header-->
    <header>
        <label for="check">
            <i class="fas fa-bars" id="sidebtn"></i>
        </label>
        <div class="leftarea">
            <h3>EMS<span></span></h3>
        </div>
    <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
    </div>

    </header>
    <!--sidebar-->
    <div class="sidebar">
     <div class="center">
                <%
                ResultSet rs6;
                PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                psm6.setString(1,uid);
                rs6 = psm6.executeQuery();
                if(rs6.next()){
                    
                %>
                    <img src="img/avatar.svg" class="profileimg" alt="">
                    <h4><%=rs6.getString("name")%></h4>
                
                <%}%>
            </div>
    
        <%
            ResultSet rs;
            String id = request.getParameter("id");
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
            PreparedStatement psm = conn2.prepareStatement("select * from products where productID=?");
            psm.setString(1, id);
            rs =  psm.executeQuery();
            while(rs.next()){
            String path = rs.getString("category");
        %>

        <ul class="ul">
        <li><a href="home.jsp?user=<%=uid%>"><i class="fas fa-house-user"></i>Home</a></li>
                <li><label for="btn-sub" class="sub-head" id="a1"><i class="fas fa-boxes"></i><%=rs.getString("category") %><span><i class="fas fa-caret-down"></i></span></label>                   
                    <input type="checkbox" id="btn-sub">
            <div class="submenu">
            <ul>
                <!-- <li id="li1"><a href="products1.jsp">All Item</a></li> -->
                <%if(path.compareTo("Concrete Mixture Parts")==0){%>
                <li id="li2"><a href="products1.jsp?user=<%=uid%>">All Items</a></li>
                <li id="li3"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass Based</a></li>
                <%}else if(path.compareTo("Vehical Engine Parts")==0){%>
                <li id="li2"><a href="products1.jsp?user=<%=uid%>">All Items</a></li>
                <li id="li3"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass Based</a></li>
                <%}else{%>
                <li id="li2"><a href="products1.jsp?user=<%=uid%>">All Items</a></li>
                <li id="li3"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                <li id="li4"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                <%}%>
            </ul>
            </div>
        </li>
        <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-shopping-bag"></i>Customer Order</a></li>
            <li><a href="place-order.jsp?user=<%=uid%>"><i class="fas fa-truck"></i>Order</a></li>
            <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
            <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
        </ul>
        <%}%>  
    </div>

    <div class="content">
        <div class="box">
        <table class="table">    
        <%
        ResultSet rs1;
        String id1 = request.getParameter("id");
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn1 = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
        PreparedStatement psm1 = conn1.prepareStatement("select * from products where productID=?");
        psm1.setString(1, id1);
        rs1 =  psm1.executeQuery();
        while(rs1.next()){
            int min1 = rs1.getInt("min");
            String id2 = rs1.getString("productID");


        %>
        <form  action="#">
            <tr class="tr1">
            <div class="product-item1">
                <th>
                    <div class="topic">
                        <h3><input type="text" value="<%=rs1.getString("name")%>" name="product_name" id="product_name" readonly></h3>
                    </div>
                    <img class="item-img1" src="<%=rs1.getString("path") %>" alt="Denim Jeans">
                </th>
                <th>
                    <div class="detail1">
                        <ul>
                            <li class="li2"><%=rs1.getString("category") %></li>
                            <li><h3><%=rs1.getString("size")%></h3></li>
                            <li><h3>Minimun Order Quantity&nbsp-&nbsp<%=min1%></h3></li>
                               
                            <li style="color: rgb(141, 42, 42);font-size: 42px;width: 450px; height: 70px;">LKR <input class="unit-price" name="unit_price" style="color: rgb(141, 42, 42);font-size: 42px;font-weight:bold;width: 250px; height: 70px;" type="text" value="<%=rs1.getString("uprice") %>"  id="unit_price" readonly></li>
                            <li>
                            
                                <p><b>Quantity</b></p>
                    <div class="qty" id="incdec">                 
                        <input type="number" value="<%=min1%>" oninput="multiplyBy()" name="quantity"  id="quantity" min="<%=min1%>"   required/>
                        
                    </div>
                            <div><input type="text" value = "0" name = "total_price" id ="total_price2" hidden></div>
                   <script>
              $(document).ready(function(){
                multiplyBy();

              $("#up").on('click',function(){
                if($("#incdec input").val()==0){
                  $("#incdec input").val(parseInt($("#incdec input").val())+'<%=rs1.getString("min")%>'-1);
                }
               
                  $("#incdec input").val(parseInt($("#incdec input").val())+1);
                    multiplyBy();
                 
              });
    
              $("#down").on('click',function(){
                  if($("#incdec input").val()>'<%=rs1.getString("min")%>'){
                  $("#incdec input").val(parseInt($("#incdec input").val())-1);
                    multiplyBy();
                  }
              });
    
              });
            </script>
                            </li>
                        </ul>                        
                    </div>    
                </th>
            </div>     
            </tr>
            <tr>
                <th colspan="2" class="p">
                    
                    <h2>More Details</h2>
                    <p><%=rs1.getString("description") %></p>
                </th>
            </tr>
            <tr class="tr1">
                <th></th>
                <th>
                <div class="submit-btn">
                    <!-- <button type="submit" id="btn1" name="id" value="<%=id%>" onclick="form.action='insert.jsp'">Add to Cart</button> -->
                     <button type="submit"id="btn1" name= "id" value="<%=id2%>" onclick="form.action='application/insert.jsp'">Add to Cart</button>
                <%-- <a href="#"  onclick="form.action='application/insert.jsp'" class= "addtocart" value="<%=id2%> " name="id"><i class="fas fa-shopping-cart"></i> Add to Cart</a> --%>
                <%-- <a href="order-resipt-one.jsp?user=<%=uid%>&id=<%=id%>&qty=<%=id%>" class="writereview"><i class="fas fa-pen"></i>Buy Now</a> --%>
                <button type="submit"id="btn1" name= "single" value="<%=id2%>" onclick="form.action='order-resipt-one.jsp'">Buy Now</button>
                </div>
                </th>
            </tr>
            </form>

            <script>
                function multiplyBy()
                {
                    num2 = '<%=rs1.getString("uprice") %>';
                    num3 = $("#incdec input").val();
                 

                    document.getElementById("total_price2").value = (Number(num3)*num2).toFixed(2);
                     
                }
            </script>

            <%       
              }  
            %>
            </table>
       </div>
    </div>
    <%}}%>
</body>
</html>