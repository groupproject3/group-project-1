<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="css/cart.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <title>Shopping Cart</title>
</head>
<body>
<%
    String uid = request.getParameter("user");
    String id1 = null;
   // id1 = request.getParameter("id");
    String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");
    ResultSet rs5;
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
   /* PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
    psm5.setString(1,uid);
    rs5 = psm5.executeQuery();
    if(!rs5.next()){
        response.sendRedirect("/project/index.html");
    }*/
    %>
  <input type="checkbox" id="check">
  <!--header-->
  <header>
    <label for="check">
        <i class="fas fa-bars" id="sidebtn"></i>
    </label>
    <div class="leftarea">
        <h3>EMS<span></span></h3>
    </div>
    <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
    </div>
  </header>
    <!--sidebar-->
    <div class="sidebar">
      <div class="center">
        <%
        ResultSet rs6;
        PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
        psm6.setString(1,uid);
        rs6 = psm6.executeQuery();
        if(rs6.next()){
            
        %>
            <img src="img/avatar.svg" class="profileimg" alt="">
            <h4><%=rs6.getString("name")%></h4>
        <%}%>
      </div>
      <ul class="ul">
        <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
        <li><label for="btn-sub" class="sub-head" ><i class="fas fa-boxes"></i>Show Items<span><i class="fas fa-caret-down"></i></span></label>                   
          <input type="checkbox" id="btn-sub">
            <div class="submenu">
                <ul>
                    <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                    <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                    <li id="li3"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                    <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass BAsed</a></li>
                </ul>
            </div>
        </li>
        <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-shopping-bag"></i>Customized Order</a></li>
          <li><a href="place-order.jsp?user=<%=uid%>"><i class="fas fa-truck"></i>Order</a></li>
          <li><a href="#"id="a1"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
          <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
    </ul>
      
  </div>

  <div class="content">
    <div class="box">
      <h1><b>Shopping Cart</b></h1>
      <form action="#">
      <!-- <form name="myform" onsubmit="return validateForm()" method="post">   -->
      <table id="table" class="table" cellpadding ="0" style="width:1050px">
        <tr class="tr1">
          <th><b>Item Name</b></th>
          <th><b>Unit Price</b></th>
          <th><b>Quantity</b></th>
          <th><b>Item Price</b></th>
          <th><b>Image</b></th>
        </tr>

        <%
          ResultSet rs;
          PreparedStatement psm = conn.prepareStatement("select cart.product_id,cart.product_name,cart.unit_price,cart.quantity,cart.total_price,products.path,products.min from cart inner join products on cart.product_id = products.productID where cart.customer_id = ?");
          psm.setString(1,uid);
          rs =  psm.executeQuery();

          while(rs.next()){   
            String id2 = rs.getString("product_id");
            String pname = rs.getString("product_name");
            String quantity = rs.getString("quantity");
            String total = rs.getString("total_price");
            String price = rs.getString("unit_price");
            String min = rs.getString("min");
        %>
          
        <tr class="tr2">
          <th><input type="text" value="<%=rs.getString("product_name") %>" disabled></th>
          <th><input type="text" value="<%=rs.getString("unit_price") %>" disabled></th>
          <th>
            <div class="qty" id="incdec1">
              <input type="text" value="<%=rs.getString("quantity") %>" disabled />                
            </div>          
          </th>
          <th><input type="text" style=" width: 120px;margin-left: 15px;" id="" value="<%=rs.getString("total_price") %>" name="total_price" disabled></th>
          <th><img src="<%=rs.getString("path") %>" class ="del-img" alt=""></th>
          
          <th>
            <form>
             
              <button type="button" class="del-icon" name="id"  onclick="Delete('<%=id2%>','<%=pname%>','<%=uid%>')"><i class="fas fa-trash-alt"></i></button>
            </form>
          </th>
          <th><form><input type="text" value="<%=uid%>" id="user" hidden name="user"><button type="button" class="edit-icon" onclick="display1('<%=quantity%>','<%=price%>','<%=min%>','<%=id2%>')" value="<%=id2%>" name="id" id="<%=id2%>"  ><i class="fas fa-edit"></i></button></form></th>
          <!-- <th ><a class="edit-icon" href="#"><i class="fas fa-edit"></i></a></th> -->
        </tr>     
        <%}%>       
        <tr>
          <th ><hr class="hr"></th>
          <th ><hr class="hr"></th>
          <th ><hr class="hr"></th>
          <th ><hr class="hr"></th>
        </tr>
        <%
          ResultSet rs3;
         
          PreparedStatement psm3 = conn.prepareStatement("select sum(total_price) from cart where customer_id=?");
          psm3.setString(1,uid);
          rs3 =  psm3.executeQuery();

          while(rs3.next()){
             String sum1 = rs3.getString("sum(total_price)");
            if(sum1==null){%>
            <tr>
              <th><h1>Total</h1></th>
              <th></th>
              <th></th>
              <th>LKR.<input class="sum" type="text" id = "sum_total" value = "0.00" readonly></th>
              <th></th>
            </tr>
          <%}else{   
        %>
        <tr>
          <th><h1>Total</h1></th>
          <th></th>
          <th></th>
          <th>LKR.<input class="sum" type="text" id = "sum_total" value = "<%=rs3.getString("sum(total_price)") %>" readonly></th>
          <th></th>
        </tr>
        
      <%}%>
     
      </table>
      <table id="table" class="table" cellpadding ="0" style="width:1050px; background-color:rgb(220, 220, 233);">
        
        <tr>
          <th></th>
          <th></th>
          <th>
            <div class="button">
             <input type="text" value="<%=uid%>" id="user" hidden name="user">
             <button type="submit"id="btn1" name="id" value="<%=id1%>" onclick="form.action='products1.jsp'">Back to Shopping</button>
             </div>
          </th>
          <%if(sum1!=null){%>
          <th>
            <div class="button1">
             <input type="text" value="<%=uid%>" id="user" hidden name="user1">
              <button type="submit"id="btn1" name="id" value="<%=id1%>" onclick="form.action='order-resipt.jsp'">Place Order</button>
            </div>
            </th> 
            <%}%>
        </tr>
     
      </table>
      <%}%>
    </form>  
    </form>
   
    </div>
  </div>
  

  <div id="popup1" class="popup">
    <!-- Modal content -->
    <div class="popup-content">
    <form>
      <h2 style="margin-top:-10px">Update</h2>
        <span class="close" onclick="hide1();">&times;</span>
        <ul>
        <li><b>Quantity &nbsp:</b><input class="pop-qty" style="width:100px" oninput="displayDate()" name="pop_qty" id="qty_id" type="number" value=" " min = " " required/></li>
        <li><input class="unit-price" type="text" value = " "  name="unit_price" id="popup_price" hidden></li>
        <li><input class="unit-price" type="text" value = " "  name="min" id="popup_min" hidden></li>
      
          <%-- <div class="updwn">
          <a onclick="up();"><i class="fa fa-angle-up" style="font-size:36px" id="up"></i></a>
          <a><i class="fa fa-angle-down" style="font-size:36px" id="down"></i></a>
          </div> --%>
           <li><label><b>Toatal &nbsp&nbsp&nbsp&nbsp&nbsp:</b></label><input style="border: none;" name = "pop_total" id="total_id" value = " " type="text" required/></li>
        </ul>
           
              <button type="submit" id="btn3" class="btn3" name="btn" value=""  onclick="form.action='application/update-cart.jsp'">Update</button>
          </form>
       
      
        <%-- <table style="float:right;">
            <tr>
                <td> --%>
                    
                <%-- </td>
                <td><button type="submit" id="btn4" class="btn4" onclick="hide1();">No</button></td>
            </tr>
        </table> --%>
        
      </div>  
    </div>


    <script>
    // Get the modal
    var pop1 = document.getElementById("popup1");
    //var materilal = document.getElementById("mid").value;
    // Get the button that opens the modal
    // var btn1 = document.getElementById("btn1");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];       

    // When the user clicks the button, open the modal 
    function display1(qty,item_price,min,id){
      var sum = Number(qty)*Number(item_price);
      document.getElementById('btn3').value = id ;
      document.getElementById('qty_id').min = min;
      document.getElementById('popup_price').value = item_price;
      document.getElementById('qty_id').value = qty ;
      document.getElementById('total_id').value = sum.toFixed(2) ;

    pop1.style.display = "block";
    }
    function hide1(){
    pop1.style.display = "none";
    }
    
    function displayDate() {
            var total,qty,price;
            price = document.getElementById('popup_price').value; 
            qty = document.getElementById('qty_id').value;
            total =Number(qty)*Number(price);
            document.getElementById('total_id').value = total.toFixed(2);
        }
    // When the user clicks on <span> (x), close the modal


    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
    if (event.target == pop1) {
        pop1.style.display = "none";
    }
    if (event.target == pop) {
        pop.style.display = "none";
    }
    
    }
</script>

<div id="popup" class="popup">

<!-- Modal content -->
<div class="popup-content">
    <span style="margin-top:-14px;" class="close" onclick="hide();">&times;</span>
    <h3>Deleting...</h3>
    <p id="mid2"></p>
    <table style="float:right;">
        <tr>
            <td>
                <form>
                    <%-- <input type="text" value="" id="item" hidden name="item"> --%>
                    <input type="text" value="" id="user3" hidden name="user3">
                    <button type="submit" id="btn2"  value="" class="btn3" name="pid"  onclick="form.action='application/del-cart-item.jsp'">Yes</button>
                </form>
            </td>
            <td><button type="submit" id="btn4" class="btn4" onclick="hide();">No</button></td>
        </tr>
    </table>
    
  </div>  
</div>

<script>
    // Get the modal
    var pop = document.getElementById("popup");
    //var materilal = document.getElementById("mid").value;
    // Get the button that opens the modal
    // var btn1 = document.getElementById("btn1");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    function Delete(pid , name, user){
   // document.getElementById('item').value =item;
    document.getElementById('user3').value = user;
    document.getElementById('btn2').value = pid;
    document.getElementById('mid2').innerHTML = "Are you sure to delete the " + name;
    pop.style.display = "block";
    }
    function hide(){
    pop.style.display = "none";
    }
    
    // When the user clicks on <span> (x), close the modal


    // When the user clicks anywhere outside of the modal, close it
   /* window.onclick = function(event) {
    if (event.target == pop) {
        pop.style.display = "none";
    }
    }*/
</script>


<%}}%>
  
</body> 
</html>