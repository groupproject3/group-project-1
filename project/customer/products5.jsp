<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>

<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/new09 30.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Concrete Mixture Parts</title>
    </head>
    <body>
    <%String uid = request.getParameter("user");
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>Menu<span></span></h3>
            </div>
            <div class="searchbar">
                <input type="text" placeholder="Search..">
                <a href="product.html" class="searchicon"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
            <div class="rightarea">
                <a href="../logout.jsp?user=<%=uid%>" class="logout">Logout</a>
            </div>
        </header>
        <!--sidebar-->
        <div class="cart-btn">
            <a href="cartnew.jsp?user=<%=uid%>" class="c-btn">Your Cart <i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
            </div>
        <div class="sidebar">
            <center>
                <%
        ResultSet rs6;
        PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
        psm6.setString(1,uid);
        rs6 = psm6.executeQuery();
        if(rs6.next()){
            
        %>
            <img src="img/avatar.svg" class="profileimg" alt="">
            <h4><%=rs6.getString("name")%></h4>
        <%}%>
            </center>
            <ul class="ul">
                <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
                <li><label for="btn-sub" class="sub-head" id="a1" ><i class="fa fa-shopping-bag" aria-hidden="true"></i>Concrete Mixture Parts<span><i class="fas fa-caret-down"></i></span></label></li>                   
                    <input type="checkbox" id="btn-sub">
                    <div class="submenu">
                        <ul>
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                            <!-- <li id="li2"><a href="products2.jsp">Concrete Mixture Parts</a></li> -->
                            <li id="li3"><a href="products3.jsp?user=<%=uid%>">Vehicle Engine Wheel</a></li>
                            <li id="li4"><a href="products4.jsp?user=<%=uid%>">JCB Parts</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-shopping-bag"></i>Customer Order</a></li>
                <li><a href="place-order.jsp?user=<%=uid%>"><i class="fas fa-truck"></i>Order</a></li>
                <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <table style="width:90%" class="table1">
                <%
                    ResultSet rs;
                    PreparedStatement psm = conn.prepareStatement("select * from products");
                    rs =  psm.executeQuery();

                    while(rs.next()){
                    String path = rs.getString("category"); 

                    if(path.compareTo("others")==0){
                    String id = rs.getString("productID");
                    %>
                    <form method="POST" action="insert.jsp">
                        <tr class="tr1">
                        <div class="product-item 1">
                            <th>            
                                <img class="item-img1" src="<%=rs.getString("path") %>" alt="Denim Jeans" style="width:15%">
                    
                                <div class="detail1">
                                    <table style=" table-layout: fixed;width: 700px; margin-right: 30px; table-layout: auto;">
                                        <tr class="tr2">
                                            <td>
                                                <ul>
                                                    <li class="li2"><%=rs.getString("category") %></li>
                                                    <br>
                                                    <li class="li1"><%=rs.getString("productID")%>&nbsp&nbsp-&nbsp&nbsp<input type="text" value="<%=rs.getString("name")%>" name="product_name" id="product_name" readonly></li>
                                                    <li><h3><%=rs.getString("size")%></h3></li>
                                                    <i1><h3>Minimun Order Quantity&nbsp-&nbsp5</h3></i1>
                                                </ul>
                                            </td>
                                            <td>
                                                <input class="unit-price" style="color: rgb(141, 42, 42);font-size: 42px;width: 250px; height: 70px;" type="text" value="LKR <%=rs.getString("uprice") %>" name="unit_price" id="unit_price"readonly >
                                            </td>     
                                        </tr>
                                    </table>
                                    <div class="submit-btn">
                                        <!-- <button type="submit" id="btn1" name="id" value="<%=id%>" onclick="form.action='insert.jsp'">Add to Cart</button> --> 
                                    <a href="cartnew.jsp?user=<%=uid%>&id=<%=id%>" class="addtocart" value="submit" name="submit"  ><i class="fas fa-shopping-cart"></i> Add to Cart</a>
                                    <a href="item-detail.jsp?user=<%=uid%>&id=<%=id%>" class="writereview"><i class="fas fa-pen"></i> More Details or Buy Now</a>
                                    <!-- <a href="order-resipt.jsp?id=<%=id%>" class="buynow"><i class="fas fa-hand-holding-usd"></i> Buy Now</a> -->
                                    </div>
                                </div>    
                            </th>    
                        </div>     
                        </tr>
                        </form>
                    <%       
                     } }
                %>

            </table>
        </div>
        
        <%}%>
    
    </body>
    
  
</html>