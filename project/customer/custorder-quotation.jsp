<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/quotation.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>order-receipt</title>
    </head>
    <body>
    <%String uid = request.getParameter("user");

     String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	      /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
        <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
        </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <center>
              <%
              ResultSet rs6;
              PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
              psm6.setString(1,uid);
              rs6 = psm6.executeQuery();
              if(rs6.next()){
                  
              %>
                  <img src="img/avatar.svg" class="profileimg" alt="">
                  <h4><%=rs6.getString("name")%></h4>
              <%}%>
            </center>
            <ul>
                <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
                <!-- <li><a href="#" id="a1" ><i class="fas fa-boxes">Concrete Mixture Parts</i></a>
                    <div class="submenu">
                        <ul>
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                             <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                             <li id="li3"><a href="products3.jsp?user=<%=uid%>">Vehicle Engine Wheel</a></li>
                            <li id="li4"><a href="products4.jsp?user=<%=uid%>">JCB Parts</a></li>
                        </ul>
                    </div>
                </li>  
                 <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-dollar-sign"></i>Custome Order</a></li> -->
                 <li><a href="#" id="a1"><i class="fas fa-truck"></i>Quotation</a></li>
                <%-- <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li> --%>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box">
                <h3>Quotation</h3>
                <%   
                    ResultSet rs;
                    String ordrid = request.getParameter("btn");
                    String status = request.getParameter("stat");
                    Class.forName("com.mysql.jdbc.Driver");
                   // Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                    PreparedStatement psm = conn.prepareStatement("select * from quotation where orderID = ?");
                    psm.setString(1, ordrid);
                    rs =  psm.executeQuery();
                       
                    while(rs.next()){ 
                    String advance = rs.getString("discount");       
                %>
            
                <form action="application/quotationsend.jsp" method="POST">
                    <table>
                        <tr>
                            <td class="td1"><label for="date">Date</label></td>
                            <td><input type="date" id="date" name="date" value="<%=rs.getString("date")%>" disabled></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td class="td1"><label for="id">Quotation ID</label></td>
                            <td><input type="text" id="id" name="qid" value="<%=rs.getString("qid")%>" disabled></td>   
                            <td class="td1"><label for="orderid">Order ID</label></td>
                            <td><input type="text" id="orderid" value="<%=rs.getString("orderid")%>" disabled name="orderid"></td>
                        </tr>
                        
                        <tr>
                            <td class="td1"><label for="body">Quotation Body</label></td>
                            <td colspan=3><textarea name="body" rows="9" cols="84"  disabled><%=rs.getString("body")%></textarea></td>
                        </tr>

                        <tr>
                            <td class="td1"><label for="subtotal">Total</label></td>
                            <td><input type="text" id="subtotal" name="subtotal" value="<%=rs.getString("subtotal")%>" disabled style="text-align: right;"></td>
                            <td></td>
                            <td rowspan="3">
                                <p style="font-size:smaller;">**Please deposit the advanced payment to this account.</p>
                                <textarea id="account" name="account" rows="5" cols="38" readonly>Bank & Branch: BOC, Malabe
                                 Account No.: 12345678</textarea>
                            </td>  
                        </tr>

                        <tr>
                            <td class="td1"><label for="discount">Advance</label></td>
                            <td><input type="text" id="discount" name="discount" value="<%=rs.getString("discount")%>" disabled style="text-align: right;"></td>  
                        </tr>

                        <tr>
                            <td class="td1"><label for="total">Balance</label></td>
                            <td><input type="text" id="total" name="total" value="<%=rs.getString("total")%>" disabled style="text-align: right;"></td>  
                        </tr>
                    </table>
                    
                    <%
                         ResultSet rs2;
                        PreparedStatement psm2 = conn.prepareStatement("select * from allorders where orderID = ?");
                        psm2.setString(1, ordrid);
                        rs2 =  psm2.executeQuery();
                        
                        while(rs2.next()){ 
                        String status1 = rs2.getString("status");       
                    %>

                    <form action="#">
                        
                        <button type="submit" id="btn1"  name="id" value="<%=ordrid%>" onclick="form.action='place-order.jsp'">Back</button>
                        <%if(status1.compareTo("Quotation Sent")==0){%>
                        <button type="button" id="btn1"   value="" onclick="display1('<%=ordrid%>',<%=advance%>);">Accept Quotation</button>
                        <%}%>
                    </form> 
                   <%}%>
                <%}%>
                </form>
            </div> 
        </div>
   <div id="popup1" class="popup">
        <!-- Modal content -->
        <div class="popup-content">
            <span class="close" onclick="hide1();">&times;</span>
            <h3>Advance</h3>
            <p id="mid2"></p>
            <table style="float:right;">
                <tr>
                <form>
                <td>
                    <p>Payment</p>
                    <input id="adv" type = "text" value =" " name = "advan" readonly >
                    <input id= "default-btn" name = "path" type = "file"  hidden Required>
                    <label for="default-btn" >Put Advance Slip</label>
                </td>
                
                </tr>
                <tr>
               
                    <td>
                        <%-- <input id="adv" type = "text" value =" " name = "advan" hidden > --%>
                        <button type="submit" id="btn3" class="btn3" name="pid" onclick="form.action='application/slip-insert.jsp'">Ok</button>
                        
                    </td>
                    </form>
                    
                    <td><button type="submit" id="btn4" class="btn4" onclick="hide1();">Cancel</button></td>
                   
                </tr>
            </table>
            
            </div>  
        </div>

        <script>
        // Get the modal
        var pop1 = document.getElementById("popup1");      
        var btn1 = document.getElementById("btn1");
        var span = document.getElementsByClassName("close")[0];       

       
        function display1(orderID,discountvl){
            document.getElementById('btn3').value = orderID ;
            document.getElementById('adv').value = discountvl;
            pop1.style.display = "block";
        }
        function hide1(){
        pop1.style.display = "none";
        }
        
        // When the user clicks on <span> (x), close the modal


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == pop1) {
            pop1.style.display = "none";
        }
        }
    </script>



        <%}}%>
    </body>
</html>