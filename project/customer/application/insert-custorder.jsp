<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@ page import = "java.time.*"%>
<%

String uid = request.getParameter("user");


try{
    String req_date = request.getParameter("reqdate");
    String qty = request.getParameter("quantity");
    String status = "placed";
    String description = request.getParameter("description");
    String total = "0.00";
    String flagall = "customer";
    String flagcust = "customized";
    String payment = "no";
    String image = request.getParameter("path");
    LocalDate date = LocalDate.now();
    String strDate = date.toString();

    Class.forName("com.mysql.jdbc.Driver");

    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
    ResultSet rs;
    PreparedStatement ps = conn.prepareStatement("insert into allorders(userID,status,placed,req,amount,flag) values(?,?,?,?,?,?)");
    ps.setString(1,uid);
    ps.setString(2,status);
    ps.setString(3,strDate);
    ps.setString(4,req_date);
    ps.setString(5,total);
    ps.setString(6,flagall);
    ps.executeUpdate();

    PreparedStatement ps1 = conn.prepareStatement("select max(orderID) from allorders where userID = ? ");
    ps1.setString(1,uid);
    ResultSet rs1 = ps1.executeQuery();
    while(rs1.next()){
        String order_id = rs1.getString("max(orderID)");
        PreparedStatement ps2 = conn.prepareStatement("insert into custorder(orderID,customerID,flag,required_date,order_status,payment) values(?,?,?,?,?,?)");
        ps2.setString(1,order_id);
        ps2.setString(2,uid);
        ps2.setString(3,flagcust);
        ps2.setString(4,req_date);
        ps2.setString(5,description);
        ps2.setString(6,payment);
        ps2.executeUpdate();

        
            PreparedStatement ps5 = conn.prepareStatement("insert into customizedorder(orderID,image,quantity) values(?,?,?)");
            ps5.setString(1,rs1.getString("max(orderID)"));
            ps5.setString(2,"img/img cart/"+image);
            ps5.setString(3,qty);
            ps5.executeUpdate();
        
    //PreparedStatement ps6 = conn.prepareStatement("delete from supshoppingcart where supplierID = ?");
    // ps6.setString(1,sid);
    //  ps6.executeUpdate();
        PreparedStatement ps7 = conn.prepareStatement("insert into notification(oip,from_user,to_user,type,description,link,status,date) values(?,?,?,?,?,?,?,?)");
            ps7.setString(1,rs1.getString("max(orderID)"));
            ps7.setString(2,uid);
             ps7.setString(3,"1");
            ps7.setString(4,"Customized Order");
            ps7.setString(5,"Placed");
            ps7.setString(6,"/project/admin/sales/vieworder.jsp?id="+rs1.getString("max(orderID)"));
            ps7.setString(7,"not viewed");
            ps7.setString(8,strDate);
            ps7.executeUpdate();

    }
        
    
    //   PreparedStatement ps1 = conn1.prepareStatement("select * from products where productID=?")
    response.sendRedirect("/project/customer/place-order.jsp?user="+uid);


}catch(Exception e){
    out.println(e);
}

%>