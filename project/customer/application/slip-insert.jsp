<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,java.util.*"%>
<%@ page import = "java.time.*"%>
<%

try{
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        String uid =(String)session.getAttribute("user");
        String orderid = request.getParameter("pid");
        String advancevl = request.getParameter("advan");
        String path_img = request.getParameter("path");
        String status = "Advance Paid";
        String flag = "income";
        String type = "Customized";
        LocalDate date = LocalDate.now();
        String strDate = date.toString();

        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
        PreparedStatement ps = conn.prepareStatement("update allorders set status=? where orderID=? ");
        ps.setString(1,status);
        ps.setString(2,orderid);
        ps.executeUpdate();

        PreparedStatement ps1 = conn.prepareStatement("update customizedorder set slip=? where orderID=?");
        ps1.setString(1,"img/img cart/"+path_img);
        ps1.setString(2,orderid);
        ps1.executeUpdate();

        PreparedStatement ps2 = conn.prepareStatement("insert into income_expence(flag,description,amount,oip,date) values(?,?,?,?,?)");
        ps2.setString(1,flag);
        ps2.setString(2,status);
        ps2.setString(3,advancevl);
        ps2.setString(4,orderid);
        ps2.setString(5,strDate);
        ps2.executeUpdate();

        PreparedStatement ps4 = conn.prepareStatement("select * from allorders where orderID =?");
        ps4.setString(1,orderid);
        ResultSet rs =  ps4.executeQuery();
        while(rs.next()){
            PreparedStatement ps3 = conn.prepareStatement("insert into notification(oip,from_user,to_user,type,description,link,status,date) values(?,?,?,?,?,?,?,?)");
            ps3.setString(1,orderid);
            ps3.setString(2,uid);
            ps3.setString(3,"1");
            ps3.setString(4,"Customized order");
            ps3.setString(5,"Advance Paid");
            ps3.setString(6,"/project/admin/sales/viewquotation.jsp?btn="+orderid);
            ps3.setString(7,"not viewed");
            ps3.setString(8,strDate);
            ps3.executeUpdate();
        }
        //out.println(uid+" " + orderid+" "+ advancevl+" " + strDate + path_img);
         response.sendRedirect("/project/customer/place-order.jsp?user="+uid);


    }
}catch(Exception e){
    out.println(e);
}

%>