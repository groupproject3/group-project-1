<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<%@ page import = "java.time.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/order-resipt.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>order-receipt</title>
    </head>
    <body>
    <%
    String uid = request.getParameter("user");
    String item_id = request.getParameter("single");
    String qty1 = request.getParameter("quantity");
    String amount = request.getParameter("total_price");

    String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	      /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/

         LocalDate date = LocalDate.now();
          date = date.plusDays(1);
          String strDate = date.toString();
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
    </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <center>
              <%
              ResultSet rs6;
              PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
              psm6.setString(1,uid);
              rs6 = psm6.executeQuery();
              if(rs6.next()){
                  
              %>
                  <img src="img/avatar.svg" class="profileimg" alt="">
                  <h4><%=rs6.getString("name")%></h4>
              <%}%>
            </center>
            <ul>
               <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
                <!-- <li><a href="#" id="a1" ><i class="fas fa-boxes">Concrete Mixture Parts</i></a>
                    <div class="submenu">
                        <ul>
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                             <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                             <li id="li3"><a href="products3.jsp?user=<%=uid%>">Vehicle Engine Wheel</a></li>
                            <li id="li4"><a href="products4.jsp?user=<%=uid%>">JCB Parts</a></li>
                        </ul>
                    </div>
                </li>  
                 <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-dollar-sign"></i>Custome Order</a></li> -->
                 <li><a href="#" id="a1"><i class="fas fa-truck"></i>Order receipt</a></li>
                <%-- <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li> --%>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>
        <%
          ResultSet rs4;
          PreparedStatement psm4 = conn.prepareStatement("select * from products where productID=?");
          psm4.setString(1,item_id);
          rs4 = psm4.executeQuery();
        %>

        <div class="content">
            <div class="box" style="height: auto;">
             <h1>Order receipt</h1>
                <form action="#">
               
                <table >
                      
                      <tr>
                          <td class="td">
                            <p>Customer Name</p>
                          </td>
                          <td class="td1">
                            <p><%=rs6.getString("name")%></p>
                          </td>
                      </tr>
                      <tr>
                        <td class="td">
                          <p>Email Address</p>
                        </td>
                        <td class="td1">
                          <p><%=rs6.getString("email")%></p>
                        </td>
                    </tr>
                   
                     <% while(rs4.next()){%>
                    <tr>
                       
                        <td class="td">
                          <p>Items with Quantities</p>
                        </td>
                        <td class="td">
                           <p><%=rs4.getString("name")%> &nbsp&nbsp&nbsp <b>Quantity:</b> <%=qty1%></p>
                        </td>
                       
                    </tr>
                    <tr>

                   
                        <td class="td">
                          <p>Total Price</p>
                        </td>
                        <td class="td1">
                          <p>LKR.&nbsp<input class="sum" name="total-sum" value ="<%=amount%>" readonly></p>
                        </td>
                   
                    </tr>
                       <%}%>

                   
                       
                    <tr>
                        <td class="td">
                          <p>Required Date</p>
                        </td>
                        <td class="td1">
                          <p><input style="width: 210px;" type="date" name="date" min="<%=strDate%>" Required></p>
                        </td>
                    </tr>
                    <!-- <tr>
                      <td class="td">
                        <p>Required Date</p>
                      </td>
                      <td class="td1">
                        <label>00001</label>
                      </td>
                  </tr> -->
                  
                    <tr>
                        <td class="td">
                          <p>Order Status</p>
                        </td>
                        <td class="td1">
                          <%-- <label><input class="sum" style="width: 200px;" type="text" id="description" name="description" value = "Placed" ></label> --%>
                          <%-- <label id="description" name="description" value = "Placed">Placed</label> --%>
                          <p><input class="sum" name="description" value ="placed" readonly></p>
                          <%-- <p id="description" name="description" value="Placed">Placed</p> --%>
                          <%-- <label><p id="description" name="description" value ="Placed">Placed</p></label> --%>
                        </td>
                    </tr>
                      
                    <tr>
                        <td>
                          
                        </td>
                        <td class="td1">
                            <!-- <a href="place-order.jsp?user=<%=uid%>">Submit</a> -->
                            <form>
                              <button type="submit" id="btn1" name="user" value="<%=uid%>" onclick="form.action='application/insert-nomorder.jsp'">Submit</button> 
                            <!-- <a href="cartnew.jsp?user=<%=uid%>">Cancel Order</a> -->
                             <button type="button" id="btn2" name="user" value="<%=uid%>" onclick="Cancel(<%=uid%>)">Cancel Order</button> 
                             </form>
                        </td>
                    </tr>
                </table>
                </form>
            </div>  
        </div>

  <div id="popup1" class="popup">
    <!-- Modal content -->
    <div class="popup-content">
        <span class="close" onclick="hide1();">&times;</span>
        <h3>Warning</h3>
        <p id="mid2"></p>
        <table style="float:right;">
            <tr>
                <td>
                    <form>
                        <button type="submit" id="btn3" class="btn3" name="user"  value="" onclick="form.action='application/cancel-order.jsp'">Yes</button>
                    </form>
                </td>
                <td><button type="submit" id="btn4" class="btn4" onclick="hide1();">No</button></td>
            </tr>
        </table>
        
        </div>  
    </div>
               
                
    <script>
        // Get the modal
        var pop1 = document.getElementById("popup1");
        //var materilal = document.getElementById("mid").value;
        // Get the button that opens the modal
        var btn1 = document.getElementById("btn1");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];       

        // When the user clicks the button, open the modal 
        function Cancel(user){
          document.getElementById('btn3').value = user;
          document.getElementById('mid2').innerHTML = "Are you sure to cancel this order";
          pop1.style.display = "block";
        }
        function hide1(){
        pop1.style.display = "none";
        }
        
        // When the user clicks on <span> (x), close the modal


        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
        if (event.target == pop1) {
            pop1.style.display = "none";
        }
        }
    </script>
       
  <%}}%>
    
    </body>
    
  
</html>
    
