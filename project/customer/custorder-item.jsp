<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/order-resipt.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>order Details</title>
    </head>
    <body>
    <%
    String orderid =  request.getParameter("id");
    String uid = request.getParameter("user");
    String item_id = request.getParameter("single");
    String qty1 = request.getParameter("quantity");
    String amount = request.getParameter("total_price");

    String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	      /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
          <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs8;
                PreparedStatement psm8 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm8.setString(1,uid);
                psm8.setString(2,"not viewed");
                rs8 = psm8.executeQuery();
                if(rs8.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
        </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <center>
              <%
              ResultSet rs6;
              PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
              psm6.setString(1,uid);
              rs6 = psm6.executeQuery();
              if(rs6.next()){
                  
              %>
                  <img src="img/avatar.svg" class="profileimg" alt="">
                  <h4><%=rs6.getString("name")%></h4>
              <%}%>
            </center>
            <ul>
                <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li>
                <!-- <li><a href="#" id="a1" ><i class="fas fa-boxes">Concrete Mixture Parts</i></a>
                    <div class="submenu">
                        <ul>
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                             <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                             <li id="li3"><a href="products3.jsp?user=<%=uid%>">Vehicle Engine Wheel</a></li>
                            <li id="li4"><a href="products4.jsp?user=<%=uid%>">JCB Parts</a></li>
                        </ul>
                    </div>
                </li>  
                 <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-dollar-sign"></i>Custome Order</a></li> -->
                 <li><a href="#" id="a1"><i class="fas fa-truck"></i>Order receipt</a></li>
                <%-- <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li> --%>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>
        <%
          ResultSet rs4;
          PreparedStatement psm4 = conn.prepareStatement("select * from customizedorder where orderID=?");
          
          psm4.setString(1,orderid);
          rs4 = psm4.executeQuery();
        %>

        <div class="content">
            <div class="box" style="height: auto;width:700px">
                <form action="#">
                <h1>Order Details</h1>
                <table >
                      
                      <tr>
                          <td class="td">
                            <p>Customer Name</p>
                          </td>
                          <td class="td1">
                            <p><%=rs6.getString("name")%></p>
                          </td>
                      </tr>
                      <tr>
                        <td class="td">
                          <p>Email Address</p>
                        </td>
                        <td class="td1">
                          <p><%=rs6.getString("email")%></p>
                        </td>
                    </tr>
                  <% while(rs4.next()){%>
                    <tr>
                       
                        <td class="td">
                          <p>Quantity</p>
                        </td>
                        <td class="td1">
                           <p><%=rs4.getString("quantity")%></p>
                        </td>
                       
                    </tr>
                    <%}
                   
                    ResultSet rs7;
                    PreparedStatement psm7 = conn.prepareStatement("select * from allorders where orderID=?");
                    
                    psm7.setString(1,orderid);
                    rs7 = psm7.executeQuery();
                    while(rs7.next()){
                    %>  
                    <tr>
                        <td class="td">
                          <p>Required Date</p>
                        </td>
                        <td class="td1">
                         <p><input style="width: 200px;" type="date" value="<%= rs7.getString("req")%>" readonly></p>
                        </td>
                    </tr>
                    
                    <tr>

                        <td class="td">
                          <p>Draft</p>
                        </td>
                        <td class="td1">
                          <label>Saved</label>
                        </td>
                   
                    </tr>
                      
                    
                    <!-- <tr>
                      <td class="td">
                        <p>Required Date</p>
                      </td>
                      <td class="td1">
                        <label>00001</label>
                      </td>
                  </tr> -->
                  
                    <tr>
                        <td class="td">
                          <p>Order Status</p>
                        </td>
                        <td class="td1">
                          <p><%= rs7.getString("status")%></p>
                        </td>
                    </tr>
                      
                    <%}%>
                    <tr>
                        <td>
                        
                        </td>
                        <td class="td1">
                            <!-- <a href="place-order.jsp?user=<%=uid%>">Submit</a> -->
                            <%-- <input  type="text" name="qty" value ="<%=qty1%>"hidden> --%>
                             <button type="submit" id="btn3" name="product_id" value="<%=item_id%>" onclick="form.action='place-order.jsp'">Back</button> 
                            <!-- <a href="cartnew.jsp?user=<%=uid%>">Cancel Order</a> -->
                             <%-- <button type="submit" id="btn1" name="user" value="<%=uid%>" onclick="form.action='application/cancel-order.jsp'">Cancel Order</button>  --%>
                        </td>
                    </tr>
                </table>
                </form>
            </div>  
        </div>
       
  <%}}%>
    
    </body>
    
  
</html>