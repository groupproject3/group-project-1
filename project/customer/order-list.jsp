<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/place-order.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>Place Order</title>
    </head>
    <body>
   <%String uid = request.getParameter("user");

  String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/

        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
            <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
    </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <center>
                <%
                ResultSet rs6;
                PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
                psm6.setString(1,uid);
                rs6 = psm6.executeQuery();
                if(rs6.next()){
                    
                %>
                    <img src="img/avatar.svg" class="profileimg" alt="">
                    <h4><%=rs6.getString("name")%></h4>
                <%}%>
            </center>
            <ul>
                <li><a href="home.jsp?user=<%=uid%>"><i class="fas fa-house-user"></i>Home</a></li>
                <li><a href="#" ><i class="fas fa-boxes"></i>Show Items</a>
                    <div class="submenu">
                        <ul>
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                    <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                    <li id="li3"><a href="products3.jsp?user=<%=uid%>">Iron Based</a></li>
                    <li id="li4"><a href="products4.jsp?user=<%=uid%>">Brass Based</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-shopping-bag"></i>Customer Order</a></li>
                <li><a href="#" id="a1"><i class="fas fa-truck"></i>Order</a></li>
                <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>

            </ul>
            
            
        </div>
        <div class="content">
            <div class="box">
                <table cellpadding ="0" width="100%">
                    <thead>
                        <tr>
                            <th>Product ID</th>
                            <th>Product Name</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                     <%
                        String oid = request.getParameter("id");
                        //out.println(oid);
                        ResultSet rs,rs2,rs3;
                        PreparedStatement psm = conn.prepareStatement("select custnormalorder.itemID,custnormalorder.quantity,products.name from custnormalorder inner join products on custnormalorder.itemID =products.productID where custnormalorder.orderID=?");
                        psm.setString(1,oid);
                        rs =  psm.executeQuery();
                        while(rs.next()){      
                    %> 
                            <tr>
                                <td><%=rs.getString("itemID") %></td>
                                <td><%=rs.getString("name") %></td>
                                <td><%=rs.getString("quantity") %></td> 
                                
                            </tr>
                        <%}%>
                </table>
                <form action="#">
                    <button style="margin-top:20px" type="submit" name="user" value="<%=uid%>" id="btn1" onclick="form.action='place-order.jsp'">Back</button>
                </form>
            </div>
        </div>
        <%}}%>
    </body>
</html>
            