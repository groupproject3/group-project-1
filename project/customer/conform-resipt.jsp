<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="css/order-resipt.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <head>
        <title>order-receipt</title>
    </head>
    <body>
    <%String uid = request.getParameter("user");

    String stats = "null";
    stats = request.getParameter("notify");
    String ordr = request.getParameter("id");
    String user;
    if(session.getAttribute("user")==null){
        response.sendRedirect("/project/index.jsp");
    }else{
        user = (String)session.getAttribute("type");
    if(user.compareTo("customer")!=0){
        response.sendRedirect("/project/index.jsp");
    }else{
        uid =(String)session.getAttribute("user");

        ResultSet rs5;
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");

        if(stats != null){
           PreparedStatement psm5 = conn.prepareStatement("update notification set status=? where to_user=? and oip=?");
           psm5.setString(1,"viewed");
           psm5.setString(2,uid);
           psm5.setString(3,ordr);
           psm5.executeUpdate();

        }
	    /*PreparedStatement psm5 = conn.prepareStatement("select * from logged where userID=?");
        psm5.setString(1,uid);
        rs5 = psm5.executeQuery();
        if(!rs5.next()){
            response.sendRedirect("/project/index.html");
        }*/
        %>
        <input type="checkbox" id="check">
        <!--header-->
        <header>
            <label for="check">
                <i class="fas fa-bars" id="sidebtn"></i>
            </label>
            <div class="leftarea">
                <h3>EMS<span></span></h3>
            </div>
          <div class="rightarea">
        <table style="float:right;">
            <tr>
                <td>
                <%
                ResultSet rs7;
                PreparedStatement psm7 = conn.prepareStatement("select * from notification where to_user=? and status=?");
                psm7.setString(1,uid);
                psm7.setString(2,"not viewed");
                rs7 = psm7.executeQuery();
                if(rs7.next()){
                    %><a href="notification/customer.jsp"><img src="/project/img/notificationred.svg"  class="notifyimgr" alt=""></a></td><%
                }else{%>
                    <a href="notification/customer.jsp"><img src="/project/img/notificationblack.svg"  class="notifyimgb" alt=""></a></td>
                <%}%>
                <td><a href="../logout.jsp" class="logout">Logout</a></td>
                </tr>
            </table>
        </div>
        </header>
        <!--sidebar-->
        <div class="sidebar">
            <center>
              <%
              ResultSet rs6;
              PreparedStatement psm6 = conn.prepareStatement("select * from usertable where userID=?");
              psm6.setString(1,uid);
              rs6 = psm6.executeQuery();
              if(rs6.next()){
                  
              %>
                  <img src="img/avatar.svg" class="profileimg" alt="">
                  <h4><%=rs6.getString("name")%></h4>
              <%}%>
            </center>
            <ul>
                <!-- <li><a href="home.jsp?user=<%=uid%>" ><i class="fas fa-house-user"></i>Home</a></li> -->
                <!-- <li><a href="#" id="a1" ><i class="fas fa-boxes">Concrete Mixture Parts</i></a>
                    <div class="submenu">
                        <ul>
                            <li id="li1"><a href="products1.jsp?user=<%=uid%>">All Item</a></li>
                             <li id="li2"><a href="products2.jsp?user=<%=uid%>">Concrete Mixture Parts</a></li>
                             <li id="li3"><a href="products3.jsp?user=<%=uid%>">Vehicle Engine Wheel</a></li>
                            <li id="li4"><a href="products4.jsp?user=<%=uid%>">JCB Parts</a></li>
                        </ul>
                    </div>
                </li>  
                 <li><a href="custorder.jsp?user=<%=uid%>"><i class="fas fa-dollar-sign"></i>Custome Order</a></li> -->
                 <li><a href="#" id="a1"><i class="fas fa-truck"></i>Order receipt</a></li>
                <li><a href="cartnew.jsp?user=<%=uid%>"><i class="fa fa-shopping-cart"></i>Shopping Cart</a></li>
                <li><a href="customersettings.jsp?user=<%=uid%>"><i class="fas fa-cog"></i>Settings</a></li>
            </ul>
            
        </div>

        <div class="content">
            <div class="box" style="height: auto;">
            <%
                ResultSet rs;
                PreparedStatement psm = conn.prepareStatement("select usertable.userID,usertable.name,usertable.email,allorders.req,allorders.amount,allorders.status from usertable inner join allorders on usertable.userID = allorders.userID where orderID = ?");
                psm.setString(1, ordr);
                rs =  psm.executeQuery();
                while(rs.next()){
            %>
                <h1>Order receipt</h1>
                <table >
                      
                      <tr>
                          <td class="td">
                            <p>Order ID</p>
                          </td>
                          <td class="td1 ">
                            <p><%=ordr%></p>
                          </td>
                      </tr>
                      <tr>
                          <td class="td">
                            <p>Customer Name</p>
                          </td>
                          <td class="td1">
                            <p><%= rs.getString("name")%></p>
                          </td>
                      </tr>
                      <tr>
                        <td class="td">
                          <p>Email Address</p>
                        </td>
                        <td class="td1">
                          <p><%= rs.getString("email")%></p>
                        </td>
                    </tr>
                    
                   
                   <tr>
                        <td class="td" style="vertical-align: top;">
                          <p>Items with Quantities</p>
                        </td>
                        <td class="td1" style="vertical-align: top;">
                        
                            <%-- <select id="mtype" name="mtype" style="width: 325px;"> --%>
                            <table class="list">
                            <tr>
                              <th>Product Name</th>
                              <th>Quantity</th>
                            </tr>
                            <%
                                ResultSet rs2;
                                PreparedStatement psm2 = conn.prepareStatement("select products.name,custnormalorder.quantity from products inner join custnormalorder on custnormalorder.itemID = products.productID where custnormalorder.orderID = ?");
                                psm2.setString(1, ordr);
                                rs2 =  psm2.executeQuery();
                                while(rs2.next()){
                            %>
                                <tr>
                                  <td><%=rs2.getString("name")%></td> 
                                  <td><%=rs2.getString("quantity")%></td>
                                </tr>  
                                <%}%>
                           </table> 
                        </td>
                    </tr>
                    <tr>
                        <td class="td">
                          <p>Total Price</p>
                        </td>
                        <td class="td1">
                          <p>LKR.&nbsp<%= rs.getString("amount")%></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="td">
                          <p>Required Date</p>
                        </td>
                        <td class="td1">
                          <p><input style="width: 200px;" type="date" value="<%= rs.getString("req")%>" readonly></p>
                        </td>
                    </tr>
                    <!-- <tr>
                      <td class="td">
                        <p>Required Date</p>
                      </td>
                      <td class="td1">
                        <label>00001</label>
                      </td>
                  </tr> -->
                    <tr>
                        <td class="td">
                          <p>Order Status</p>
                        </td>
                        <td class="td1">
                          <p><%= rs.getString("status")%></p>
                        </td>
                    </tr>
                      
                    <tr>
                        <td>
                             <table>
                                <tr>
                                    <td class="td"></td>
                                    <td class="td">
                                   </td>
                                    
                                </tr>
                            </table>
                        
                        </td>
                        <td class="td1">
                            <!-- <a href="place-order.jsp?user=<%=uid%>">Submit</a> -->
                             <!-- <button type="submit" id="btn1" name="user" value="<%=uid%>" onclick="form.action='place-order.jsp'">Submit</button>  -->
                            <!-- <a href="cartnew.jsp?user=<%=uid%>">Cancel Order</a> -->
                            <table>
                                <tr>
                                    <td class="td">
                                    
                                    
                                    <td class="td"><form><button type="submit" id="btn1" name="id" value="<%=ordr%>" onclick="form.action='place-order.jsp'">Back</button></form> </td>
                                </tr>
                            </table>
                              
                        </td>
                    </tr>
                </table>
                
            </div>  
        </div>
    <%}}}%>
    
    </body>
    
  
</html>