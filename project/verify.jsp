<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="verify.css">
    <script src="https://smtpjs.com/v3/smtp.js"></script>
    <head>
        <title>Sign Up</title>
    </head>
    <body>
        <div class="box">
            
            <h1>Verify</h1>
            <form action="verifyconnect.jsp" method="GET">
            <%
                String result = null;
                result = request.getParameter("result");
                String str = "Invalide";
                String user = request.getParameter("user");
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ems","root","");
                PreparedStatement psm = conn.prepareStatement("select * from usertable where userID=?");
                psm.setString(1,user);
                ResultSet rs = psm.executeQuery();
                if(rs.next()){
                %>
                <p class="p">User</p>
                <input type="text" name="name" value="<%=rs.getString("name")%>" readonly>
                <p class="p">Email</p>
                <input type="text" name="email" value="<%=rs.getString("email")%>" readonly>
                <p class="p">Verification Code</p>
                <input type="text" name="code" placeholder="Enter a Verification Code">
                <%
                    if(result!=null){
                %><h4>Incorrect Code!</h4><% 
                    }
                    result = null;
                %>
                <a href="forgotpasswordconnect.jsp?email=<%=rs.getString("email")%>&uname=<%=rs.getString("uname")%>">Resend Email</a><br>
                <button type="submit" name="user" value="<%=rs.getString("userID")%>">Submit</button>
            </form>
            <%}%>
            <form action="forgotpassword.jsp" method="GET">
                <button  type="submit">Back</button>
            </form>
        </div>
    </body>
    
  
</html>
