-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2020 at 07:36 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `allorders`
--

CREATE TABLE `allorders` (
  `orderID` int(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `placed` varchar(255) NOT NULL,
  `req` varchar(255) DEFAULT NULL,
  `amount` decimal(9,2) NOT NULL,
  `flag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `allorders`
--

INSERT INTO `allorders` (`orderID`, `status`, `placed`, `req`, `amount`, `flag`) VALUES
(2, 'Quotation sent', '10/11/2020', '11/11/2020', '98000.00', 'customer'),
(18, 'Quotation accepted', '2020-11-22', '2020-11-29', '190.00', 'supplier');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `customer_id` int(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `unit_price` decimal(9,2) NOT NULL,
  `quantity` int(255) NOT NULL,
  `total_price` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`customer_id`, `product_id`, `product_name`, `unit_price`, `quantity`, `total_price`) VALUES
(2, 'CK003', '4\' inch Wheel', '3000.00', 4, '12000.00'),
(2, 'CK005', '5mm Treed Wheel', '900.00', 5, '4500.00');

-- --------------------------------------------------------

--
-- Table structure for table `custnormalorder`
--

CREATE TABLE `custnormalorder` (
  `orderID` int(255) NOT NULL,
  `itemID` varchar(255) NOT NULL,
  `quantity` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `custnormalorder`
--

INSERT INTO `custnormalorder` (`orderID`, `itemID`, `quantity`) VALUES
(2, 'CK001', 4),
(2, 'CK002', 5);

-- --------------------------------------------------------

--
-- Table structure for table `customizedorder`
--

CREATE TABLE `customizedorder` (
  `orderID` int(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `custorder`
--

CREATE TABLE `custorder` (
  `orderID` int(255) NOT NULL,
  `customerID` int(255) NOT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `payment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `custorder`
--

INSERT INTO `custorder` (`orderID`, `customerID`, `flag`, `payment`) VALUES
(2, 2, 'normal', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `logged`
--

CREATE TABLE `logged` (
  `logID` int(255) NOT NULL,
  `userID` int(255) NOT NULL,
  `start` time(6) NOT NULL,
  `end` time(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logged`
--

INSERT INTO `logged` (`logID`, `userID`, `start`, `end`) VALUES
(59, 1, '11:14:17.094000', '00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE `materials` (
  `materialID` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `prelvl` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `mtype` varchar(255) NOT NULL,
  `stock` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`materialID`, `name`, `prelvl`, `type`, `description`, `mtype`, `stock`) VALUES
('RAW001', 'Grey Cast Iron', 500, 'raw', '', 'kg', 2500),
('RAW002', 'Ductile Cast Iron', 200, 'raw', '', 'kg', 0),
('RAW003', 'Dolomite', 250, 'raw', '', 'kg', 0),
('RAW004', 'Brass', 250, 'raw', '', 'kg', 0),
('RAW005', 'Zinc', 50, 'raw', '', 'kg', 0),
('RAW006', 'Clay', 50, 'raw', '', 'kg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `production`
--

CREATE TABLE `production` (
  `productionID` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `sdate` varchar(255) NOT NULL,
  `edate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `productionmaterials`
--

CREATE TABLE `productionmaterials` (
  `productionID` int(255) NOT NULL,
  `materialID` int(255) NOT NULL,
  `amount` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productID` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `min` int(255) NOT NULL,
  `uprice` int(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `materialID` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productID`, `category`, `path`, `name`, `min`, `uprice`, `size`, `materialID`, `description`) VALUES
('CM001', 'Concrete Mixture Parts', 'img/img cart/DSC_0045.JPG', 'Concrete Mixer Wheel 15\"', 5, 3500, '15\"', 'CM001', 'Can be used for manufacturing of concrete mixer machines. Suitable for operating the concrete mixer.'),
('CM002', 'Concrete Mixture Parts', 'img/img cart/DSC_0117.JPG', 'Concrete Mixer Wheel 7\"', 10, 2000, '7\"', 'CM002', 'Can be used for manufacturing of concrete mixer machines. Suitable for operating the concrete mixer.'),
('CW001', 'Iron', 'img/img cart/IMG_0651.JPG', 'Cog Wheel 4\"', 50, 400, '4\"', 'CW001', 'An Iron cog wheel with a width of 2\", a diameter of 4\" and a circumferences of 6\".<br>Suitable for machines to withstand a weight of 100kg and capable of 300 rpm.'),
('GW001', 'Brass', 'img/img cart/31Sdr6-PYdL.jpg', 'Gear Wheel 15\"', 10, 3000, '15\"', 'GW001', 'A Brass Gear Wheel with a width of 3\", a diameter of 15\" and a circumferences of 45\".<br>Able to transmit torque between parallel shafts. Brass for corrosion resistance, ductility at high temperatures, and low magnetic permeability.'),
('WH001', 'Iron', 'img/img cart/IMG_0658.JPG', 'Belt Wheel 15\"', 2, 5000, '15\"', 'WH001', 'An Iron belt wheel with a width of 5\", a diameter of 15\" and a circumferences of 45\".<br>Suitable for machine belts with a weight of 200kg and capable of 200 rpm.'),
('WW001', 'Brass', 'img/img cart/WormWheel.jpg', 'Worm Wheel 6\"', 20, 1000, '6\"', 'WW001', 'A Brass Worm Wheel with a width of 3\", a diameter of 6\" and a circumferences of 9\".<br>Able to change the rotational movement by 90 degrees.  Has a high reduction ratio. Not capable of reversing the direction of power.');

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `date` date NOT NULL,
  `qid` int(255) NOT NULL,
  `orderid` int(255) NOT NULL,
  `body` varchar(500) NOT NULL,
  `subtotal` decimal(9,2) NOT NULL,
  `discount` decimal(9,2) NOT NULL,
  `total` decimal(9,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`date`, `qid`, `orderid`, `body`, `subtotal`, `discount`, `total`) VALUES
('2020-11-19', 2, 2, 'xyz', '100000.00', '2000.00', '98000.00'),
('2020-11-22', 4, 18, '0', '200.00', '10.00', '190.00');

-- --------------------------------------------------------

--
-- Table structure for table `suporder`
--

CREATE TABLE `suporder` (
  `supplierID` int(255) NOT NULL,
  `orderID` int(255) NOT NULL,
  `payment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `suporder`
--

INSERT INTO `suporder` (`supplierID`, `orderID`, `payment`) VALUES
(3, 15, 'none'),
(3, 16, 'none'),
(3, 17, 'none'),
(3, 18, 'none');

-- --------------------------------------------------------

--
-- Table structure for table `supordermaterials`
--

CREATE TABLE `supordermaterials` (
  `orderID` int(255) NOT NULL,
  `materialID` varchar(255) NOT NULL,
  `quantity` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supordermaterials`
--

INSERT INTO `supordermaterials` (`orderID`, `materialID`, `quantity`) VALUES
(15, 'sj0001', 200),
(15, 'sj0002', 200),
(16, 'sj0003', 50),
(17, 'sj0001', 3),
(17, 'sj0003', 2),
(17, 'sj0004', 2),
(18, 'RAW001', 4),
(18, 'RAW004', 7);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplierID` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplierID`, `company`, `name`, `contact`, `email`, `address`) VALUES
(3, 'Nimal & Sons (PVT) LTD.', 'Nimal Soysa', '0777777777', 'nimal@gmail.com', '45/b, Kaduwela Road, Biyagama'),
(4, 'Kulathunga Enterprises', 'Saman Kulathunga', '0762222558', 'saman@gmail.com', '52/1, Galle Road, Wallawatta');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_materials`
--

CREATE TABLE `supplier_materials` (
  `supplierID` int(255) NOT NULL,
  `materialID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier_materials`
--

INSERT INTO `supplier_materials` (`supplierID`, `materialID`) VALUES
(3, 'RAW001'),
(3, 'RAW002'),
(3, 'RAW003'),
(3, 'RAW004');

-- --------------------------------------------------------

--
-- Table structure for table `supshoppingcart`
--

CREATE TABLE `supshoppingcart` (
  `supplierID` int(255) NOT NULL,
  `materialID` varchar(255) NOT NULL,
  `quantity` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `usertable`
--

CREATE TABLE `usertable` (
  `userID` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usertable`
--

INSERT INTO `usertable` (`userID`, `title`, `name`, `company`, `type`, `email`, `phone`, `uname`, `password`) VALUES
(1, 'mr', 'hiruna', 'P&A ltd', 'admin', 'hirugmail.com', '0788888888', 'hiruna', 'hiruna@969580'),
(2, 'mr', 'sajith', 'abc', 'cus', 'sajith@gmail.com', '07777777788', 'sajith', 'sajith123'),
(3, 'mr', 'perera', 'abc', 'sup', 'abc@gmail.com', '0777777788', 'perera', 'perera123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allorders`
--
ALTER TABLE `allorders`
  ADD PRIMARY KEY (`orderID`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `custnormalorder`
--
ALTER TABLE `custnormalorder`
  ADD PRIMARY KEY (`orderID`,`itemID`);

--
-- Indexes for table `customizedorder`
--
ALTER TABLE `customizedorder`
  ADD PRIMARY KEY (`orderID`,`image`);

--
-- Indexes for table `custorder`
--
ALTER TABLE `custorder`
  ADD PRIMARY KEY (`orderID`,`customerID`);

--
-- Indexes for table `logged`
--
ALTER TABLE `logged`
  ADD PRIMARY KEY (`logID`);

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`materialID`);

--
-- Indexes for table `production`
--
ALTER TABLE `production`
  ADD PRIMARY KEY (`productionID`);

--
-- Indexes for table `productionmaterials`
--
ALTER TABLE `productionmaterials`
  ADD PRIMARY KEY (`productionID`,`materialID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productID`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`qid`,`orderid`),
  ADD KEY `order_key` (`orderid`);

--
-- Indexes for table `suporder`
--
ALTER TABLE `suporder`
  ADD PRIMARY KEY (`supplierID`,`orderID`);

--
-- Indexes for table `supordermaterials`
--
ALTER TABLE `supordermaterials`
  ADD PRIMARY KEY (`orderID`,`materialID`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplierID`);

--
-- Indexes for table `supplier_materials`
--
ALTER TABLE `supplier_materials`
  ADD PRIMARY KEY (`supplierID`,`materialID`);

--
-- Indexes for table `supshoppingcart`
--
ALTER TABLE `supshoppingcart`
  ADD PRIMARY KEY (`supplierID`,`materialID`);

--
-- Indexes for table `usertable`
--
ALTER TABLE `usertable`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allorders`
--
ALTER TABLE `allorders`
  MODIFY `orderID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `logged`
--
ALTER TABLE `logged`
  MODIFY `logID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `production`
--
ALTER TABLE `production`
  MODIFY `productionID` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplierID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usertable`
--
ALTER TABLE `usertable`
  MODIFY `userID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `productionmaterials`
--
ALTER TABLE `productionmaterials`
  ADD CONSTRAINT `productionmaterials_ibfk_1` FOREIGN KEY (`productionID`) REFERENCES `production` (`productionID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `quotation`
--
ALTER TABLE `quotation`
  ADD CONSTRAINT `order_key` FOREIGN KEY (`orderid`) REFERENCES `allorders` (`orderID`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
