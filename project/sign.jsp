<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html>
<html>
    <link rel="stylesheet" type="text/css" href="sign.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <head>
        <title>Sign Up</title>
    </head>
    <body>
    
        <div class="box">
            <h1>Sign Up</h1>
            <form action="signconnect.jsp" method="POST">
                <table>
                    <tr>
                        <td class="td">
                        <p>Title</p>
                        <select name="title">
                            <option value="mr">Mr</option>
                            <option value="mrs">Mrs</option>
                            <option value="miss">Miss</option>
                        </select>
                        </td>
                        <td class="td">
                        <p>Name</p>
                        <input type="text" placeholder="Enter Name" name="name" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="td">
                        <p>Company Name</p>
                        <input type="text" placeholder="Enter Company Name" name="company" required>
                        </td>
                        <%-- <td class="td">
                        <p>User Type</p>
                        <select name="type">
                            <option value="sup">Supplier</option>
                            <option value="cus">Customer</option>
                        </select>
                        </td> --%>
                        <td class="td">
                        <p>Contact Number</p>
                        <input type="text" name="phone" placeholder="Enter Contact Number" pattern="0[0-9]{9}" required>
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="td">
                        <p>Email</p>
                        <input type="email" placeholder="Enter Email" name="email" required>
                        </td>
                        <td></td>
                    </tr>   
                    </table>
                    
                    <table>
                    <tr>
                        <td class="td">
                        <p>User Name</p>
                        <input type="text" name="uname" placeholder="Enter User Name"  required>
                        </td>
                        <td class="td">
                        <script>
                        function repwd(){
                            var passwordInput = document.getElementById('password');
                            var passStatus = document.getElementById('icon3');

                            if(passwordInput.type == 'password'){
                                passwordInput.type = 'text';
                                passStatus.className = 'fa fa-eye-slash';
                            }
                            else{
                                passwordInput.type = 'password';
                                passStatus.className = 'fa fa-eye';
                            }
                            }
                            function repwd1(){
                            var passwordInput = document.getElementById('re_password');
                            var passStatus = document.getElementById('icon4');

                            if(passwordInput.type == 'password'){
                                passwordInput.type = 'text';
                                passStatus.className = 'fa fa-eye-slash';
                            }
                            else{
                                passwordInput.type = 'password';
                                passStatus.className = 'fa fa-eye';
                            }
                            }
                            </script>
                        <p>Password</p>
                        <input type="password" name="password" id="password" placeholder="Enter Password"  oninvalid="this.setCustomValidity('Minimum 8 characters, atleast one letter & number')" oninput="this.setCustomValidity('')" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" required>
                        <i id="icon3" class="fa fa-eye" onClick="repwd()"></i>
                        </td>
                        <td class="td">
                        <p>Re- Enter Password</p>
                        <input type="password" name="rpassword" id="re_password" placeholder="Re-enter Password"  oninvalid="this.setCustomValidity('Minimum 8 characters, atleast one letter & number')" oninput="this.setCustomValidity('')" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" required>
                        <i id="icon4" class="fa fa-eye" onClick="repwd1()"></i>
                        </td>
                    </tr>
              </table>
              <button type="submit" id="btn">Register</button>
            </form>
            <form action="login.jsp"><button type="submit" id="btn">Back</button></form>
        </div>
    </body>
    
  
</html>